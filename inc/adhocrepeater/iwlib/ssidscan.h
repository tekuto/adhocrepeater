﻿#ifndef ADHOCREPEATER_IWLIB_SSIDSCAN_H
#define ADHOCREPEATER_IWLIB_SSIDSCAN_H

#include "adhocrepeater/def/iwlib/ssidscan.h"
#include "adhocrepeater/def/iwlib/socket.h"
#include "adhocrepeater/def/common/time.h"

#include <iwlib.h>
#include <array>
#include <string>

namespace adhocrepeater {
    struct SsidScanData
    {
        typedef unsigned short Size;

        typedef std::array<
            char
            , static_cast< Size >( ~0 )
        > Buffer;

        Buffer  buffer;
        Size    size;
    };

    struct ScannedSsid
    {
        __u32       mode;
        std::string ssid;
        iw_freq     iwFreq;
    };

    bool initSsidScan(
        IwSocket &
    );

    bool getSsidScanData(
        SsidScanData &
        , IwSocket &
        , const Duration &
        , const Duration &
    );

    void initEventStream(
        stream_descr &
        , SsidScanData &
    );

    bool getScannedSsid(
        ScannedSsid &
        , stream_descr &
        , IwSocket &
    );
}

#endif  // ADHOCREPEATER_IWLIB_SSIDSCAN_H
