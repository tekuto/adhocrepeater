﻿#ifndef ADHOCREPEATER_IWLIB_SOCKET_H
#define ADHOCREPEATER_IWLIB_SOCKET_H

#include "adhocrepeater/def/iwlib/socket.h"

#include <iwlib.h>
#include <memory>
#include <string>

namespace adhocrepeater {
    struct IwSocket
    {
        struct CloseSocket
        {
            void operator()(
                int *
            ) const;
        };

        typedef std::unique_ptr<
            int
            , CloseSocket
        > SocketCloser;

        std::string     interface;
        int             socket;
        SocketCloser    socketCloser;
        iw_range        iwRange;
    };

    IwSocket * newIwSocket(
        const std::string &
    );

    void free(
        IwSocket &
    );

    bool iwGetExt(
        IwSocket &
        , int
        , const SetIwReq &
        , const GetIwReq &
    );

    bool iwSetExt(
        IwSocket &
        , int
        , const SetIwReq &
    );

    bool toChannel(
        int &
        , IwSocket &
        , const iw_freq &
    );

    bool toIwFreq(
        iw_freq &
        , IwSocket &
        , int
    );
}

#endif  // ADHOCREPEATER_IWLIB_SOCKET_H
