﻿#ifndef ADHOCREPEATER_SSID_SENDER_H
#define ADHOCREPEATER_SSID_SENDER_H

#include "adhocrepeater/def/ssid/sender.h"
#include "adhocrepeater/def/packet/manager.h"
#include "adhocrepeater/def/config/config.h"
#include "adhocrepeater/def/common/thread.h"
#include "adhocrepeater/def/common/unique.h"

#include <string>

namespace adhocrepeater {
    struct SsidSender
    {
        Unique< PacketManager > packetManagerUnique;

        Unique< Thread >    sendThreadUnique;
    };

    SsidSender * newSsidSender(
        const Config &
    );

    void free(
        SsidSender &
    );

    bool send(
        SsidSender &
        , const std::string &
    );
}

#endif  // ADHOCREPEATER_SSID_SENDER_H
