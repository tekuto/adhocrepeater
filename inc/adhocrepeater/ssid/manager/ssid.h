﻿#ifndef ADHOCREPEATER_SSID_MANAGER_SSID_H
#define ADHOCREPEATER_SSID_MANAGER_SSID_H

#include "adhocrepeater/def/ssid/manager/ssid.h"
#include "adhocrepeater/def/common/time.h"

#include <string>

namespace adhocrepeater {
    struct Ssid
    {
        std::string ssid;
        TimePoint   updateTime;

        bool operator==(
            const std::string &
        ) const;

        bool operator<(
            const Ssid &
        ) const;
    };

    void update(
        Ssid &
        , const TimePoint &
    );
}

#endif  // ADHOCREPEATER_SSID_MANAGER_SSID_H
