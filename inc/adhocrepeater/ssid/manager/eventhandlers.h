﻿#ifndef ADHOCREPEATER_SSID_MANAGER_EVENTHANDLERS_H
#define ADHOCREPEATER_SSID_MANAGER_EVENTHANDLERS_H

#include "adhocrepeater/def/ssid/manager/eventhandlers.h"
#include "adhocrepeater/def/ssid/manager/inserteventhandler.h"
#include "adhocrepeater/def/ssid/manager/insertevent.h"

#include <set>

namespace adhocrepeater {
    struct SsidManagerEventHandlers
    {
        typedef std::set< SsidManagerInsertEventHandler * > InsertEventHandlerPtrSet;

        InsertEventHandlerPtrSet    insertEventHandlerPtrSet;
    };

    SsidManagerEventHandlers * newSsidManagerEventHandlers(
    );

    SsidManagerEventHandlers * clone(
        const SsidManagerEventHandlers &
    );

    void free(
        SsidManagerEventHandlers &
    );

    bool add(
        SsidManagerEventHandlers &
        , SsidManagerInsertEventHandler &
    );

    void remove(
        SsidManagerEventHandlers &
        , SsidManagerInsertEventHandler &
    );

    void call(
        SsidManagerEventHandlers &
        , SsidManagerInsertEvent &
    );
}

#endif  // ADHOCREPEATER_SSID_MANAGER_EVENTHANDLERS_H
