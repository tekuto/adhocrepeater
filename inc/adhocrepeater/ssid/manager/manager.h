﻿#ifndef ADHOCREPEATER_SSID_MANAGER_MANAGER_H
#define ADHOCREPEATER_SSID_MANAGER_MANAGER_H

#include "adhocrepeater/def/ssid/manager/manager.h"
#include "adhocrepeater/def/ssid/manager/eventhandlers.h"
#include "adhocrepeater/def/ssid/manager/ssid.h"
#include "adhocrepeater/def/ssid/list.h"
#include "adhocrepeater/def/common/time.h"
#include "adhocrepeater/def/common/unique.h"

#include <set>
#include <mutex>
#include <string>

namespace adhocrepeater {
    struct SsidManager
    {
        typedef std::set< Ssid > SsidSet;

        Unique< SsidManagerEventHandlers >  eventHandlersUnique;
        const MicroSeconds &                KEEP_TIME;

        std::mutex  mutex;
        SsidSet     ssidSet;
    };

    SsidManager * newSsidManager(
        const SsidManagerEventHandlers &
        , const MicroSeconds &
    );

    void free(
        SsidManager &
    );

    bool update(
        SsidManager &
        , const std::string &
    );

    bool getSsidList(
        SsidManager &
        , SsidList &
    );
}

#endif  // ADHOCREPEATER_SSID_MANAGER_MANAGER_H
