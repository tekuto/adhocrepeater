﻿#ifndef ADHOCREPEATER_SSID_MANAGER_INSERTEVENT_H
#define ADHOCREPEATER_SSID_MANAGER_INSERTEVENT_H

#include "adhocrepeater/def/ssid/manager/insertevent.h"
#include "adhocrepeater/def/ssid/manager/manager.h"

#include <string>

namespace adhocrepeater {
    struct SsidManagerInsertEvent
    {
        SsidManager &   manager;
        std::string     ssid;
    };

    SsidManagerInsertEvent * newSsidManagerInsertEvent(
        SsidManager &
        , const std::string &
    );

    void free(
        SsidManagerInsertEvent &
    );

    SsidManager & getManager(
        SsidManagerInsertEvent &
    );

    const std::string & getSsid(
        const SsidManagerInsertEvent &
    );
}

#endif  // ADHOCREPEATER_SSID_MANAGER_INSERTEVENT_H
