﻿#ifndef ADHOCREPEATER_SSID_MANAGER_INSERTEVENTHANDLER_H
#define ADHOCREPEATER_SSID_MANAGER_INSERTEVENTHANDLER_H

#include "adhocrepeater/def/ssid/manager/inserteventhandler.h"
#include "adhocrepeater/def/ssid/manager/insertevent.h"

namespace adhocrepeater {
    struct SsidManagerInsertEventHandler
    {
        SsidManagerInsertEventHandlerProc   proc;
    };

    SsidManagerInsertEventHandler * newSsidManagerInsertEventHandler(
        const SsidManagerInsertEventHandlerProc &
    );

    void free(
        SsidManagerInsertEventHandler &
    );

    void call(
        SsidManagerInsertEventHandler &
        , SsidManagerInsertEvent &
    );
}

#endif  // ADHOCREPEATER_SSID_MANAGER_INSERTEVENTHANDLER_H
