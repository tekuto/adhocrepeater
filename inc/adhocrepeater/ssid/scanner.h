﻿#ifndef ADHOCREPEATER_SSID_SCANNER_H
#define ADHOCREPEATER_SSID_SCANNER_H

#include "adhocrepeater/def/ssid/scanner.h"
#include "adhocrepeater/def/ssid/manager/manager.h"
#include "adhocrepeater/def/config/config.h"
#include "adhocrepeater/def/common/thread.h"
#include "adhocrepeater/def/common/unique.h"

namespace adhocrepeater {
    struct SsidScanner
    {
        Unique< Thread >    threadUnique;
    };

    SsidScanner * newSsidScanner(
        const Config &
        , SsidManager &
    );

    void free(
        SsidScanner &
    );
}

#endif  // ADHOCREPEATER_SSID_SCANNER_H
