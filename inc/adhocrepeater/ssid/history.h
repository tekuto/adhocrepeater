﻿#ifndef ADHOCREPEATER_SSID_HISTORY_H
#define ADHOCREPEATER_SSID_HISTORY_H

#include "adhocrepeater/def/ssid/history.h"
#include "adhocrepeater/def/ssid/list.h"
#include "adhocrepeater/def/common/time.h"

#include <vector>
#include <string>

namespace adhocrepeater {
    struct SsidHistory
    {
        const size_t    MAX_SIZE;

        SsidList    history;
    };

    SsidHistory * newSsidHistory(
        size_t
    );

    void free(
        SsidHistory &
    );

    bool getNextSsid(
        std::string &
        , const SsidHistory &
        , const SsidList &
    );

    bool add(
        SsidHistory &
        , const std::string &
    );
}

#endif  // ADHOCREPEATER_SSID_HISTORY_H
