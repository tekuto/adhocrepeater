﻿#ifndef ADHOCREPEATER_SSID_CHANGER_H
#define ADHOCREPEATER_SSID_CHANGER_H

#include "adhocrepeater/def/ssid/changer.h"
#include "adhocrepeater/def/ssid/sender.h"
#include "adhocrepeater/def/ssid/manager/inserteventhandler.h"
#include "adhocrepeater/def/ssid/manager/manager.h"
#include "adhocrepeater/def/config/config.h"
#include "adhocrepeater/def/common/thread.h"
#include "adhocrepeater/def/common/time.h"
#include "adhocrepeater/def/common/unique.h"

#include <mutex>
#include <condition_variable>
#include <string>

namespace adhocrepeater {
    struct SsidChanger
    {
        const Config &  CONFIG;

        std::mutex              mutex;
        std::condition_variable cond;

        bool        existsNextSsid;
        std::string nextSsid;

        bool        connected;
        std::string currentSsid;
        TimePoint   connectTime;

        Unique< SsidManagerInsertEventHandler > insertEventHandlerUnique;
        Unique< SsidManager >                   ssidManagerUnique;

        Unique< Thread >    threadUnique;
    };

    SsidChanger * newSsidChanger(
        const Config &
        , SsidSender &
    );

    void free(
        SsidChanger &
    );

    void change(
        SsidChanger &
        , const std::string &
    );

    void disconnect(
        SsidChanger &
    );

    SsidManager & getSsidManager(
        SsidChanger &
    );
}

#endif  // ADHOCREPEATER_SSID_CHANGER_H
