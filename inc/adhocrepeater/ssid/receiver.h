﻿#ifndef ADHOCREPEATER_SSID_RECEIVER_H
#define ADHOCREPEATER_SSID_RECEIVER_H

#include "adhocrepeater/def/ssid/receiver.h"
#include "adhocrepeater/def/ssid/manager/manager.h"
#include "adhocrepeater/def/packet/manager.h"
#include "adhocrepeater/def/config/config.h"
#include "adhocrepeater/def/common/thread.h"
#include "adhocrepeater/def/common/unique.h"

namespace adhocrepeater {
    struct SsidReceiver
    {
        Unique< PacketManager > packetManagerUnique;

        Unique< Thread >    receiveThreadUnique;
        Unique< Thread >    updateThreadUnique;
    };

    SsidReceiver * newSsidReceiver(
        const Config &
        , SsidManager &
    );

    void free(
        SsidReceiver &
    );
}

#endif  // ADHOCREPEATER_SSID_RECEIVER_H
