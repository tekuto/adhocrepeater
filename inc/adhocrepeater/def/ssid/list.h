﻿#ifndef ADHOCREPEATER_DEF_SSID_LIST_H
#define ADHOCREPEATER_DEF_SSID_LIST_H

#include <vector>
#include <string>

namespace adhocrepeater {
    typedef std::vector< std::string > SsidList;
}

#endif  // ADHOCREPEATER_DEF_SSID_LIST_H
