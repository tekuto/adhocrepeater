﻿#ifndef ADHOCREPEATER_DEF_SSID_MANAGER_INSERTEVENTHANDLER_H
#define ADHOCREPEATER_DEF_SSID_MANAGER_INSERTEVENTHANDLER_H

#include "adhocrepeater/def/ssid/manager/insertevent.h"

#include <functional>

namespace adhocrepeater {
    struct SsidManagerInsertEventHandler;

    typedef std::function<
        void (
            SsidManagerInsertEvent &
        )
    > SsidManagerInsertEventHandlerProc;
}

#endif  // ADHOCREPEATER_DEF_SSID_MANAGER_INSERTEVENTHANDLER_H
