﻿#ifndef ADHOCREPEATER_DEF_PACKET_BUFFER_H
#define ADHOCREPEATER_DEF_PACKET_BUFFER_H

#include "adhocrepeater/def/packet/packet.h"

namespace adhocrepeater {
    typedef std::vector< Packet > PacketBuffer;
}

#endif  // ADHOCREPEATER_DEF_PACKET_BUFFER_H
