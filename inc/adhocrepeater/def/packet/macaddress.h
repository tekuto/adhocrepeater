﻿#ifndef ADHOCREPEATER_DEF_PACKET_MACADDRESS_H
#define ADHOCREPEATER_DEF_PACKET_MACADDRESS_H

#include <array>

namespace adhocrepeater {
    const auto  MAC_ADDRESS_LENGTH = 6;

    typedef std::array<
        unsigned char
        , MAC_ADDRESS_LENGTH
    > MacAddress;
}

#endif  // ADHOCREPEATER_DEF_PACKET_MACADDRESS_H
