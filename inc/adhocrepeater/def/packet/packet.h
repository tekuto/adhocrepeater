﻿#ifndef ADHOCREPEATER_DEF_PACKET_PACKET_H
#define ADHOCREPEATER_DEF_PACKET_PACKET_H

#include <vector>

namespace adhocrepeater {
    typedef std::vector< char > Packet;
}

#endif  // ADHOCREPEATER_DEF_PACKET_PACKET_H
