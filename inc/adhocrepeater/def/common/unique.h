﻿#ifndef ADHOCREPEATER_DEF_COMMON_UNIQUE_H
#define ADHOCREPEATER_DEF_COMMON_UNIQUE_H

#include "adhocrepeater/def/common/free.h"

#include <memory>
#include <type_traits>

namespace adhocrepeater {
    template< typename T >
    using Unique = std::unique_ptr<
        T
        , Free< T >
    >;

    template< typename T >
    Unique< typename std::remove_pointer< T >::type > unique(
        T   _ptr
    )
    {
        return Unique< typename std::remove_pointer< T >::type >(
            _ptr
        );
    }
}

#endif  // ADHOCREPEATER_DEF_COMMON_UNIQUE_H
