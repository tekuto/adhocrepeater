﻿#ifndef ADHOCREPEATER_DEF_COMMON_THREAD_H
#define ADHOCREPEATER_DEF_COMMON_THREAD_H

#include <functional>

namespace adhocrepeater {
    struct Thread;

    typedef std::function< void( Thread & ) > ThreadProc;
}

#endif  // ADHOCREPEATER_DEF_COMMON_THREAD_H
