﻿#ifndef ADHOCREPEATER_DEF_COMMON_TIME_H
#define ADHOCREPEATER_DEF_COMMON_TIME_H

#include <chrono>
#include <functional>

namespace adhocrepeater {
    typedef std::chrono::steady_clock Time;

    typedef Time::time_point TimePoint;

    typedef Time::duration Duration;

    typedef std::chrono::microseconds MicroSeconds;

    typedef std::function< bool( bool & ) > LoopProc;
}

#endif  // ADHOCREPEATER_DEF_COMMON_TIME_H
