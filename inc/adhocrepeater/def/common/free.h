﻿#ifndef ADHOCREPEATER_DEF_COMMON_FREE_H
#define ADHOCREPEATER_DEF_COMMON_FREE_H

namespace adhocrepeater {
    template< typename T >
    struct Free
    {
        void operator()(
            T * _this
        ) const
        {
            free( *_this );
        }
    };
}

#endif  // ADHOCREPEATER_DEF_COMMON_FREE_H
