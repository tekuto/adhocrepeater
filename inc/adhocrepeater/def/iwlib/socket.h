﻿#ifndef ADHOCREPEATER_DEF_IWLIB_SOCKET_H
#define ADHOCREPEATER_DEF_IWLIB_SOCKET_H

#include <functional>
#include <iwlib.h>

namespace adhocrepeater {
    struct IwSocket;

    typedef std::function< void( iwreq & ) > SetIwReq;

    typedef std::function< void( const iwreq & ) > GetIwReq;
}

#endif  // ADHOCREPEATER_DEF_IWLIB_SOCKET_H
