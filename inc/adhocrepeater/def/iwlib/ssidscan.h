﻿#ifndef ADHOCREPEATER_DEF_IWLIB_SSIDSCAN_H
#define ADHOCREPEATER_DEF_IWLIB_SSIDSCAN_H

namespace adhocrepeater {
    struct SsidScanData;

    struct ScannedSsid;
}

#endif  // ADHOCREPEATER_DEF_IWLIB_SSIDSCAN_H
