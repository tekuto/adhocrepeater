﻿#ifndef ADHOCREPEATER_CONFIG_ELEMENT_MAP_H
#define ADHOCREPEATER_CONFIG_ELEMENT_MAP_H

#include "adhocrepeater/def/config/element/map.h"
#include "adhocrepeater/def/config/element/string.h"
#include "adhocrepeater/def/config/element/element.h"
#include "adhocrepeater/def/common/unique.h"

#include <map>
#include <string>

namespace adhocrepeater {
    struct ConfigElementMap
    {
        typedef std::map<
            std::string
            , Unique< ConfigElement >
        > Map;

        Map data;
    };

    void free(
        ConfigElementMap &
    );

    const char * init(
        Unique< ConfigElementMap > &
        , const char *
        , const char *
    );

    const ConfigElementMap::Map & getMap(
        const ConfigElementMap &
    );

    ConfigElementMap::Map & getMap(
        ConfigElementMap &
    );

    const ConfigElementString * getString(
        const ConfigElementMap &
        , const char *
    );
}

#endif  // ADHOCREPEATER_CONFIG_ELEMENT_MAP_H
