﻿#ifndef ADHOCREPEATER_CONFIG_ELEMENT_LIST_H
#define ADHOCREPEATER_CONFIG_ELEMENT_LIST_H

#include "adhocrepeater/def/config/element/list.h"
#include "adhocrepeater/def/config/element/element.h"
#include "adhocrepeater/def/common/unique.h"

#include <vector>

namespace adhocrepeater {
    struct ConfigElementList
    {
        typedef std::vector< Unique< ConfigElement > > List;

        List    data;
    };

    void free(
        ConfigElementList &
    );

    const char * init(
        Unique< ConfigElementList > &
        , const char *
        , const char *
    );

    const ConfigElementList::List & getList(
        const ConfigElementList &
    );

    ConfigElementList::List & getList(
        ConfigElementList &
    );
}

#endif  // ADHOCREPEATER_CONFIG_ELEMENT_LIST_H
