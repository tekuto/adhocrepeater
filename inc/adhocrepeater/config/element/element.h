﻿#ifndef ADHOCREPEATER_CONFIG_ELEMENT_ELEMENT_H
#define ADHOCREPEATER_CONFIG_ELEMENT_ELEMENT_H

#include "adhocrepeater/def/config/element/element.h"
#include "adhocrepeater/def/config/element/string.h"
#include "adhocrepeater/def/config/element/list.h"
#include "adhocrepeater/def/config/element/map.h"
#include "adhocrepeater/def/common/unique.h"

#include <string>

namespace adhocrepeater {
    struct ConfigElement
    {
        Unique< ConfigElementString >   stringUnique;
        Unique< ConfigElementList >     listUnique;
        Unique< ConfigElementMap >      mapUnique;
    };

    ConfigElement * newConfigElement(
        const std::string &
    );

    void free(
        ConfigElement &
    );

    const char * getElement(
        const char *
        , const char *
    );

    const char * init(
        Unique< ConfigElement > &
        , const char *
        , const char *
    );

    const ConfigElementString * getString(
        const ConfigElement &
    );

    const ConfigElementList * getList(
        const ConfigElement &
    );

    const ConfigElementMap * getMap(
        const ConfigElement &
    );
}

#endif  // ADHOCREPEATER_CONFIG_ELEMENT_ELEMENT_H
