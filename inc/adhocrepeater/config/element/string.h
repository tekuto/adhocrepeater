﻿#ifndef ADHOCREPEATER_CONFIG_ELEMENT_STRING_H
#define ADHOCREPEATER_CONFIG_ELEMENT_STRING_H

#include "adhocrepeater/def/config/element/string.h"
#include "adhocrepeater/def/common/unique.h"

#include <string>

namespace adhocrepeater {
    struct ConfigElementString
    {
        typedef std::string String;

        String  data;
    };

    const char * init(
        Unique< ConfigElementString > &
        , const char *
        , const char *
    );

    void free(
        ConfigElementString &
    );

    const std::string & getString(
        const ConfigElementString &
    );

    std::string & getString(
        ConfigElementString &
    );
}

#endif  // ADHOCREPEATER_CONFIG_ELEMENT_STRING_H
