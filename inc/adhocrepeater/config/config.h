﻿#ifndef ADHOCREPEATER_CONFIG_CONFIG_H
#define ADHOCREPEATER_CONFIG_CONFIG_H

#include "adhocrepeater/def/config/config.h"
#include "adhocrepeater/def/packet/macaddress.h"
#include "adhocrepeater/def/common/time.h"

#include <string>

namespace adhocrepeater {
    struct Config
    {
        struct Interfaces
        {
            std::string scanner;
            std::string repeater;
        };

        struct TargetSsid
        {
            std::string prefix;
            int         prefixSize;
            int         channel;
        };

        struct Ports
        {
            unsigned short  data;
            unsigned short  ssid;
        };

        struct Peer
        {
            std::string     address;    //TODO
            unsigned short  dataPort;   //TODO
            unsigned short  ssidPort;   //TODO
        };

        struct Scanner
        {
            MicroSeconds    scanInterval;
            MicroSeconds    getScanDataTimeout;
            MicroSeconds    getScanDataInterval;
        };

        struct Changer
        {
            MicroSeconds    getSsidWaitTime;
            int             ssidHistorySize;
            MicroSeconds    disconnectableTime;
            MicroSeconds    ssidManagerKeepTime;
            MicroSeconds    findSsidTimeout;
            MicroSeconds    findSsidInterval;
            MicroSeconds    getScanDataTimeout;
            MicroSeconds    getScanDataInterval;
        };

        struct Sender
        {
            MacAddress      deviceAddress;  //TODO
            MicroSeconds    receiveRawTimeout;
            int             pollingTimeoutMSeconds;
        };

        struct Receiver
        {
            MicroSeconds    receiveUdpTimeout;
            int             pollingTimeoutMSeconds;
        };

        Interfaces  interfaces;
        TargetSsid  targetSsid;
        Ports       ports;
        Peer        peer;
        Scanner     scanner;
        Changer     changer;
        Sender      sender;
        Receiver    receiver;
    };

    Config * newConfig(
        const std::string &
    );

    void free(
        Config &
    );
}

#endif  // ADHOCREPEATER_CONFIG_CONFIG_H
