﻿#ifndef ADHOCREPEATER_COMMON_THREAD_H
#define ADHOCREPEATER_COMMON_THREAD_H

#include "adhocrepeater/def/common/thread.h"

#include <mutex>
#include <thread>

namespace adhocrepeater {
    struct Thread
    {
        struct JoinThread
        {
            void operator()(
                Thread *
            ) const;
        };

        typedef std::unique_ptr<
            Thread
            , JoinThread
        > ThreadJoiner;

        std::mutex  mutex;
        bool        running;

        std::thread     thread;
        ThreadJoiner    threadJoiner;
    };

    Thread * newThread(
        const ThreadProc &
    );

    void free(
        Thread &
    );

    void end(
        Thread &
    );

    bool isRunning(
        Thread &
    );
}

#endif  // ADHOCREPEATER_COMMON_THREAD_H
