﻿#ifndef ADHOCREPEATER_COMMON_FILE_H
#define ADHOCREPEATER_COMMON_FILE_H

#include <string>

namespace adhocrepeater {
    bool readFile(
        std::string &
        , const std::string &
    );
}

#endif  // ADHOCREPEATER_COMMON_FILE_H
