﻿#ifndef ADHOCREPEATER_COMMON_TIME_H
#define ADHOCREPEATER_COMMON_TIME_H

#include "adhocrepeater/def/common/time.h"

namespace adhocrepeater {
    bool intervalProc(
        const Duration &
        , const LoopProc &
    );

    bool intervalProc(
        const LoopProc &
    );

    bool timeoutProc(
        const Duration &
        , const Duration &
        , const LoopProc &
    );

    bool timeoutProc(
        const Duration &
        , const LoopProc &
    );

    bool stringToMicroSeconds(
        MicroSeconds &
        , const std::string &
    );
}

#endif  // ADHOCREPEATER_COMMON_TIME_H
