﻿#ifndef ADHOCREPEATER_DATA_RECEIVER_H
#define ADHOCREPEATER_DATA_RECEIVER_H

#include "adhocrepeater/def/data/receiver.h"
#include "adhocrepeater/def/packet/manager.h"
#include "adhocrepeater/def/config/config.h"
#include "adhocrepeater/def/common/thread.h"
#include "adhocrepeater/def/common/unique.h"

namespace adhocrepeater {
    struct DataReceiver
    {
        Unique< PacketManager > packetManagerUnique;

        Unique< Thread >    receiveThreadUnique;
        Unique< Thread >    sendThreadUnique;
    };

    DataReceiver * newDataReceiver(
        const Config &
    );

    void free(
        DataReceiver &
    );
}

#endif  // ADHOCREPEATER_DATA_RECEIVER_H
