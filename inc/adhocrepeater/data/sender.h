﻿#ifndef ADHOCREPEATER_DATA_SENDER_H
#define ADHOCREPEATER_DATA_SENDER_H

#include "adhocrepeater/def/data/sender.h"
#include "adhocrepeater/def/packet/manager.h"
#include "adhocrepeater/def/ssid/changer.h"
#include "adhocrepeater/def/config/config.h"
#include "adhocrepeater/def/common/thread.h"
#include "adhocrepeater/def/common/unique.h"

namespace adhocrepeater {
    struct DataSender
    {
        Unique< PacketManager > packetManagerUnique;

        Unique< Thread >    receiveThreadUnique;
        Unique< Thread >    sendThreadUnique;
    };

    DataSender * newDataSender(
        const Config &
        , SsidChanger &
    );

    void free(
        DataSender &
    );
}

#endif  // ADHOCREPEATER_DATA_SENDER_H
