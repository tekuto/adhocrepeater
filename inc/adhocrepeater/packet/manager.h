﻿#ifndef ADHOCREPEATER_PACKET_MANAGER_H
#define ADHOCREPEATER_PACKET_MANAGER_H

#include "adhocrepeater/def/packet/manager.h"
#include "adhocrepeater/def/packet/buffer.h"
#include "adhocrepeater/def/packet/packet.h"

#include <mutex>
#include <condition_variable>

namespace adhocrepeater {
    struct PacketManager
    {
        std::mutex              mutex;
        std::condition_variable cond;
        PacketBuffer            buffer;
    };

    PacketManager * newPacketManager(
    );

    void free(
        PacketManager &
    );

    bool add(
        PacketManager &
        , Packet &&
    );

    void getBuffer(
        PacketManager &
        , PacketBuffer &
    );
}

#endif  // ADHOCREPEATER_PACKET_MANAGER_H
