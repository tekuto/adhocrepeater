﻿#ifndef ADHOCREPEATER_PACKET_RAW_RECEIVER_H
#define ADHOCREPEATER_PACKET_RAW_RECEIVER_H

#include "adhocrepeater/def/packet/raw/receiver.h"
#include "adhocrepeater/def/packet/packet.h"
#include "adhocrepeater/def/common/time.h"

#include <string>

namespace adhocrepeater {
    RawReceiver * newRawReceiver(
        const std::string &
    );

    void free(
        RawReceiver &
    );

    bool receive(
        RawReceiver &
        , Packet &
    );

    bool poll(
        RawReceiver &
        , int
    );
}

#endif  // ADHOCREPEATER_PACKET_RAW_RECEIVER_H
