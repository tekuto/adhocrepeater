﻿#ifndef ADHOCREPEATER_PACKET_RAW_SENDER_H
#define ADHOCREPEATER_PACKET_RAW_SENDER_H

#include "adhocrepeater/def/packet/raw/sender.h"
#include "adhocrepeater/def/packet/packet.h"

#include <string>

namespace adhocrepeater {
    RawSender * newRawSender(
        const std::string &
    );

    void free(
        RawSender &
    );

    bool send(
        RawSender &
        , const Packet &
    );
}

#endif  // ADHOCREPEATER_PACKET_RAW_SENDER_H
