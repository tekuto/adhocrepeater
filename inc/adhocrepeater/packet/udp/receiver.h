﻿#ifndef ADHOCREPEATER_PACKET_UDP_RECEIVER_H
#define ADHOCREPEATER_PACKET_UDP_RECEIVER_H

#include "adhocrepeater/def/packet/udp/receiver.h"
#include "adhocrepeater/def/packet/packet.h"

namespace adhocrepeater {
    UdpReceiver * newUdpReceiver(
        unsigned short
    );

    void free(
        UdpReceiver &
    );

    bool receive(
        UdpReceiver &
        , Packet &
    );

    bool poll(
        UdpReceiver &
        , int
    );
}

#endif  // ADHOCREPEATER_PACKET_UDP_RECEIVER_H
