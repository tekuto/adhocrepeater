﻿#ifndef ADHOCREPEATER_PACKET_UDP_SENDER_H
#define ADHOCREPEATER_PACKET_UDP_SENDER_H

#include "adhocrepeater/def/packet/udp/sender.h"
#include "adhocrepeater/def/packet/packet.h"

#include <string>

namespace adhocrepeater {
    UdpSender * newUdpSender(
        const std::string &
        , unsigned short
    );

    void free(
        UdpSender &
    );

    bool send(
        UdpSender &
        , const Packet &
    );
}

#endif  // ADHOCREPEATER_PACKET_UDP_SENDER_H
