﻿#ifndef ADHOCREPEATER_PACKET_MACADDRESS_H
#define ADHOCREPEATER_PACKET_MACADDRESS_H

#include "adhocrepeater/def/packet/macaddress.h"

#include <string>

namespace adhocrepeater {
    bool stringToMacAddress(
        MacAddress &
        , const std::string &
    );
}

#endif  // ADHOCREPEATER_PACKET_MACADDRESS_H
