﻿#ifndef ADHOCREPEATER_PACKET_PACKET_H
#define ADHOCREPEATER_PACKET_PACKET_H

#include "adhocrepeater/def/packet/packet.h"

#include <string>

namespace adhocrepeater {
    bool setData(
        Packet &
        , const std::string &
    );

    bool setData(
        Packet &
        , const void *
        , size_t
    );
}

#endif  // ADHOCREPEATER_PACKET_PACKET_H
