﻿#ifndef ADHOCREPEATER_PACKET_SOCKET_H
#define ADHOCREPEATER_PACKET_SOCKET_H

#include "adhocrepeater/def/packet/socket.h"
#include "adhocrepeater/def/packet/packet.h"

#include <string>
#include <memory>

namespace adhocrepeater {
    struct Socket
    {
        struct CloseSocket
        {
            void operator()(
                int *
            ) const;
        };

        typedef std::unique_ptr<
            int
            , CloseSocket
        > SocketCloser;

        int             socket;
        SocketCloser    socketCloser;
    };

    void free(
        Socket &
    );

    int socketUdp(
    );

    int socketRaw(
    );

    bool connectUdp(
        Socket &
        , const std::string &
        , unsigned short
    );

    bool bindUdp(
        Socket &
        , unsigned short
    );

    bool bindRaw(
        Socket &
        , const std::string &
    );

    bool send(
        Socket &
        , const Packet &
    );

    bool receive(
        Socket &
        , Packet &
    );

    bool poll(
        Socket &
        , int
    );
}

#endif  // ADHOCREPEATER_PACKET_SOCKET_H
