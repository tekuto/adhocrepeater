# -*- coding: utf-8 -*-

from .builder import cpp

def getDependModules(
):
    return [
    ]

def build(
    _context,
):
    cpp.gtest(
        _context,
        'adhocrepeater-iwlib-ssidscantest',
        {
            'iwlib' : [
                'ssidscantest.cpp',
                'ssidscan.cpp',
                'socket.cpp',
            ],
            'common' : [
                'time.cpp',
            ],
        },
        lib = [
            'iw',
        ],
    )
