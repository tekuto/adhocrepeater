# -*- coding: utf-8 -*-

from .builder import testdata

def getDependModules(
):
    return [
    ]

def build(
    _context,
):
    testdata.cp(
        _context,
        'testconf',
        [
            'testconf.conf',
        ],
    )
