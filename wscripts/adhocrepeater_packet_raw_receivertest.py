# -*- coding: utf-8 -*-

from .builder import cpp

def getDependModules(
):
    return [
    ]

def build(
    _context,
):
    cpp.gtest(
        _context,
        'adhocrepeater-packet-raw-receivertest',
        {
            'packet' : [
                {
                    'raw' : [
                        'receivertest.cpp',
                        'receiver.cpp',
                        'sender.cpp',
                    ],
                },
                'socket.cpp',
                'packet.cpp',
            ],
        },
    )
