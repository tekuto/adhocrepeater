# -*- coding: utf-8 -*-

from .builder import cpp

def getDependModules(
):
    return [
    ]

def build(
    _context,
):
    cpp.gtest(
        _context,
        'adhocrepeater-packet-managertest',
        {
            'packet' : [
                'managertest.cpp',
                'manager.cpp',
                'packet.cpp',
            ],
        },
    )
