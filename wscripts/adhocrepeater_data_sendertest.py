# -*- coding: utf-8 -*-

from .builder import cpp

def getDependModules(
):
    return [
    ]

def build(
    _context,
):
    cpp.gtest(
        _context,
        'adhocrepeater-data-sendertest',
        [
            {
                'data' : [
                    'sendertest.cpp',
                    'sender.cpp',
                ],
            },
            {
                'ssid' : [
                    'changer.cpp',
                    'history.cpp',
                    'sender.cpp',
                    {
                        'manager' : [
                            'manager.cpp',
                            'ssid.cpp',
                            'eventhandlers.cpp',
                            'inserteventhandler.cpp',
                            'insertevent.cpp',
                        ],
                    },
                ],
            },
            {
                'iwlib' : [
                    'socket.cpp',
                    'ssidscan.cpp',
                ],
            },
            {
                'common' : [
                    'thread.cpp',
                    'time.cpp',
                    'file.cpp',
                ],
            },
            {
                'packet' : [
                    'manager.cpp',
                    'socket.cpp',
                    'packet.cpp',
                    'macaddress.cpp',
                    {
                        'raw' : [
                            'receiver.cpp',
                        ],
                    },
                    {
                        'udp' : [
                            'sender.cpp',
                        ],
                    },
                ],
            },
            {
                'config' : [
                    'config.cpp',
                    {
                        'element' : [
                            'element.cpp',
                            'string.cpp',
                            'list.cpp',
                            'map.cpp',
                        ],
                    },
                ],
            },
        ],
        lib = [
            'iw',
        ],
    )
