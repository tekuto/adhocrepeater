# -*- coding: utf-8 -*-

from .builder import cpp

def getDependModules(
):
    return [
    ]

def build(
    _context,
):
    cpp.gtest(
        _context,
        'adhocrepeater-data-receivertest',
        [
            {
                'data' : [
                    'receivertest.cpp',
                    'receiver.cpp',
                ],
            },
            {
                'common' : [
                    'thread.cpp',
                    'time.cpp',
                    'file.cpp',
                ],
            },
            {
                'packet' : [
                    'manager.cpp',
                    'socket.cpp',
                    'packet.cpp',
                    'macaddress.cpp',
                    {
                        'udp' : [
                            'receiver.cpp',
                        ],
                        'raw' : [
                            'sender.cpp',
                        ],
                    },
                ],
            },
            {
                'config' : [
                    'config.cpp',
                    {
                        'element' : [
                            'element.cpp',
                            'string.cpp',
                            'list.cpp',
                            'map.cpp',
                        ],
                    },
                ],
            },
        ],
    )
