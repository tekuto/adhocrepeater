# -*- coding: utf-8 -*-

from .builder import cpp

def getDependModules(
):
    return [
    ]

def build(
    _context,
):
    cpp.gtest(
        _context,
        'adhocrepeater-ssid-sendertest',
        [
            {
                'ssid' : [
                    'sendertest.cpp',
                    'sender.cpp',
                ],
            },
            {
                'common' : [
                    'thread.cpp',
                    'time.cpp',
                    'file.cpp',
                ],
            },
            {
                'packet' : [
                    'manager.cpp',
                    'socket.cpp',
                    'packet.cpp',
                    'macaddress.cpp',
                    {
                        'udp' : [
                            'sender.cpp',
                            'receiver.cpp',
                        ],
                    },
                ],
            },
            {
                'config' : [
                    'config.cpp',
                    {
                        'element' : [
                            'element.cpp',
                            'string.cpp',
                            'list.cpp',
                            'map.cpp',
                        ],
                    },
                ],
            },
        ],
    )
