# -*- coding: utf-8 -*-

from .builder import cpp

def getDependModules(
):
    return [
        'adhocrepeater_testdata_filetest',
    ]

def build(
    _context,
):
    cpp.gtest(
        _context,
        'adhocrepeater-common-filetest',
        {
            'common' : [
                'filetest.cpp',
                'file.cpp',
            ],
        },
    )
