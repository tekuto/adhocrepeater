# -*- coding: utf-8 -*-

from .builder import cpp

def getDependModules(
):
    return [
    ]

def build(
    _context,
):
    cpp.gtest(
        _context,
        'adhocrepeater-common-threadtest',
        {
            'common' : [
                'threadtest.cpp',
                'thread.cpp',
            ],
        },
    )
