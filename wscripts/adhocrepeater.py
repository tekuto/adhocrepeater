# -*- coding: utf-8 -*-

from .builder import cpp

def getDependModules(
):
    return [
        'adhocrepeater_common_threadtest',
        'adhocrepeater_common_filetest',
        'adhocrepeater_iwlib_sockettest',
        'adhocrepeater_iwlib_ssidscantest',
        'adhocrepeater_packet_packettest',
        'adhocrepeater_packet_managertest',
        'adhocrepeater_packet_udp_sendertest',
        'adhocrepeater_packet_udp_receivertest',
        'adhocrepeater_packet_raw_sendertest',
        'adhocrepeater_packet_raw_receivertest',
        'adhocrepeater_packet_macaddresstest',
        'adhocrepeater_ssid_manager_ssidtest',
        'adhocrepeater_ssid_manager_managertest',
        'adhocrepeater_ssid_manager_eventhandlerstest',
        'adhocrepeater_ssid_manager_inserteventtest',
        'adhocrepeater_ssid_manager_inserteventhandlertest',
        'adhocrepeater_ssid_historytest',
        'adhocrepeater_ssid_scannertest',
        'adhocrepeater_ssid_changertest',
        'adhocrepeater_ssid_sendertest',
        'adhocrepeater_ssid_receivertest',
        'adhocrepeater_data_sendertest',
        'adhocrepeater_data_receivertest',
        'adhocrepeater_config_element_elementtest',
        'adhocrepeater_config_element_maptest',
        'adhocrepeater_config_configtest',
    ]

def build(
    _context,
):
    cpp.program(
        _context,
        'adhocrepeater',
        [
            'main.cpp',
            {
                'common' : [
                    'thread.cpp',
                    'time.cpp',
                    'file.cpp',
                ],
            },
            {
                'config' : [
                    'config.cpp',
                    {
                        'element' : [
                            'element.cpp',
                            'string.cpp',
                            'list.cpp',
                            'map.cpp',
                        ],
                    },
                ],
            },
            {
                'iwlib' : [
                    'socket.cpp',
                    'ssidscan.cpp',
                ],
            },
            {
                'ssid' : [
                    'scanner.cpp',
                    'changer.cpp',
                    'history.cpp',
                    'sender.cpp',
                    'receiver.cpp',
                    {
                        'manager' : [
                            'manager.cpp',
                            'ssid.cpp',
                            'eventhandlers.cpp',
                            'inserteventhandler.cpp',
                            'insertevent.cpp',
                        ],
                    },
                ],
            },
            {
                'packet' : [
                    'socket.cpp',
                    'packet.cpp',
                    'manager.cpp',
                    'macaddress.cpp',
                    {
                        'udp' : [
                            'sender.cpp',
                            'receiver.cpp',
                        ],
                    },
                    {
                        'raw' : [
                            'sender.cpp',
                            'receiver.cpp',
                        ],
                    },
                ],
            },
            {
                'data' : [
                    'sender.cpp',
                    'receiver.cpp',
                ],
            },
        ],
        lib = [
            'iw',
        ],
    )
