# -*- coding: utf-8 -*-

APPNAME = 'adhocrepeater'

BUILD_DIR = 'build'
TEST_DIR = 'test'

SOURCE_DIR = 'src'
INCLUDE_DIR = 'inc'
TESTDATA_DIR = 'testdata'
