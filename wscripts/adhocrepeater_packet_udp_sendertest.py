# -*- coding: utf-8 -*-

from .builder import cpp

def getDependModules(
):
    return [
    ]

def build(
    _context,
):
    cpp.gtest(
        _context,
        'adhocrepeater-packet-udp-sendertest',
        {
            'packet' : [
                {
                    'udp' : [
                        'sendertest.cpp',
                        'sender.cpp',
                    ],
                },
                'socket.cpp',
                'packet.cpp',
            ],
        },
    )
