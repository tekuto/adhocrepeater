# -*- coding: utf-8 -*-

from .builder import cpp

def getDependModules(
):
    return [
    ]

def build(
    _context,
):
    cpp.gtest(
        _context,
        'adhocrepeater-iwlib-sockettest',
        {
            'iwlib' : [
                'sockettest.cpp',
                'socket.cpp',
            ],
        },
        lib = [
            'iw',
        ],
    )
