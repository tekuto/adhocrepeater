# -*- coding: utf-8 -*-

from .builder import cpp

def getDependModules(
):
    return [
    ]

def build(
    _context,
):
    cpp.gtest(
        _context,
        'adhocrepeater-config-element-elementtest',
        {
            'config' : {
                'element' : [
                    'elementtest.cpp',
                    'element.cpp',
                    'string.cpp',
                    'list.cpp',
                    'map.cpp',
                ],
            },
        },
    )
