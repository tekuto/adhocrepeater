# -*- coding: utf-8 -*-

from .common import generateSources
from .. import common

import os.path

def program(
    _context,
    _target,
    _sources,
    lib = None,
):
    _build(
        _context,
        [
            'cxxprogram',
        ],
        _target,
        _sources,
        lib,
    )

def shlib(
    _context,
    _target,
    _sources,
    lib = None,
):
    _build(
        _context,
        [
            'cxxshlib',
        ],
        _target,
        _sources,
        lib,
    )

def testShlib(
    _context,
    _testName,
    _target,
    _sources,
    lib = None,
):
    shlib(
        _context,
        os.path.join(
            common.TEST_DIR,
            _testName,
            _target,
        ),
        _sources,
        lib,
    )

def gtest(
    _context,
    _target,
    _sources,
    lib = None,
):
    libraries = [
        'gtest',
    ]
    if type( lib ) is list:
        libraries += lib

    _build(
        _context,
        [
            'cxxprogram',
            'test',
        ],
        _target,
        _sources,
        libraries,
    )

def _build(
    _context,
    _features,
    _target,
    _sources,
    _lib,
):
    _context(
        features = _generateFeatures( _features ),
        target = _target,
        source = _generateSources( _sources ),
        lib = _generateLib( _lib ),
    )

def _generateFeatures(
    _features,
):
    return [
        'cxx',
    ] + _features

def _generateSources(
    _sources,
):
    return generateSources(
        _sources,
        os.path.join(
            common.SOURCE_DIR,
            common.APPNAME,
        ),
    )

def _generateLib(
    _lib,
):
    if _lib is None:
        return []
    else:
        return _lib
