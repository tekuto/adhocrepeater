# -*- coding: utf-8 -*-

from .builder import cpp

def getDependModules(
):
    return [
    ]

def build(
    _context,
):
    cpp.gtest(
        _context,
        'adhocrepeater-ssid-changertest',
        [
            {
                'ssid' : [
                    'changertest.cpp',
                    'changer.cpp',
                    'history.cpp',
                    'sender.cpp',
                    {
                        'manager' : [
                            'manager.cpp',
                            'ssid.cpp',
                            'eventhandlers.cpp',
                            'inserteventhandler.cpp',
                            'insertevent.cpp',
                        ],
                    },
                ],
            },
            {
                'common' : [
                    'thread.cpp',
                    'time.cpp',
                    'file.cpp',
                ],
            },
            {
                'iwlib' : [
                    'socket.cpp',
                    'ssidscan.cpp',
                ],
            },
            {
                'config' : [
                    'config.cpp',
                    {
                        'element' : [
                            'element.cpp',
                            'string.cpp',
                            'list.cpp',
                            'map.cpp',
                        ],
                    },
                ],
            },
            {
                'packet' : [
                    'macaddress.cpp',
                    'manager.cpp',
                    'packet.cpp',
                    'socket.cpp',
                    {
                        'udp' : [
                            'sender.cpp',
                        ],
                    },
                ],
            },
        ],
        lib = [
            'iw',
        ],
    )
