# -*- coding: utf-8 -*-

from .builder import cpp

def getDependModules(
):
    return [
        'adhocrepeater_testdata_testconf',
    ]

def build(
    _context,
):
    cpp.gtest(
        _context,
        'adhocrepeater-ssid-scannertest',
        [
            {
                'ssid' : [
                    'scannertest.cpp',
                    'scanner.cpp',
                    {
                        'manager' : [
                            'manager.cpp',
                            'ssid.cpp',
                            'eventhandlers.cpp',
                            'inserteventhandler.cpp',
                            'insertevent.cpp',
                        ],
                    },
                ],
            },
            {
                'config' : [
                    'config.cpp',
                    {
                        'element' : [
                            'element.cpp',
                            'string.cpp',
                            'list.cpp',
                            'map.cpp',
                        ],
                    },
                ],
            },
            {
                'common' : [
                    'thread.cpp',
                    'time.cpp',
                    'file.cpp',
                ],
            },
            {
                'iwlib' : [
                    'socket.cpp',
                    'ssidscan.cpp',
                ],
            },
            {
                'packet' : [
                    'macaddress.cpp',
                ],
            },
        ],
        lib = [
            'iw',
        ],
    )
