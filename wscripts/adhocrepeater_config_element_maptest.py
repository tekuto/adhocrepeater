# -*- coding: utf-8 -*-

from .builder import cpp

def getDependModules(
):
    return [
    ]

def build(
    _context,
):
    cpp.gtest(
        _context,
        'adhocrepeater-config-element-maptest',
        {
            'config' : {
                'element' : [
                    'maptest.cpp',
                    'element.cpp',
                    'string.cpp',
                    'list.cpp',
                    'map.cpp',
                ],
            },
        },
    )
