# -*- coding: utf-8 -*-

from .builder import cpp

def getDependModules(
):
    return [
    ]

def build(
    _context,
):
    cpp.gtest(
        _context,
        'adhocrepeater-ssid-receivertest',
        [
            {
                'ssid' : [
                    'receivertest.cpp',
                    'receiver.cpp',
                    {
                        'manager' : [
                            'manager.cpp',
                            'eventhandlers.cpp',
                            'insertevent.cpp',
                            'inserteventhandler.cpp',
                            'ssid.cpp',
                        ],
                    },
                ],
            },
            {
                'common' : [
                    'thread.cpp',
                    'time.cpp',
                    'file.cpp',
                ],
            },
            {
                'packet' : [
                    'macaddress.cpp',
                    'manager.cpp',
                    'packet.cpp',
                    'socket.cpp',
                    {
                        'udp' : [
                            'sender.cpp',
                            'receiver.cpp',
                        ],
                    }
                ],
            },
            {
                'config' : [
                    'config.cpp',
                    {
                        'element' : [
                            'element.cpp',
                            'string.cpp',
                            'list.cpp',
                            'map.cpp',
                        ],
                    },
                ],
            },
        ],
    )
