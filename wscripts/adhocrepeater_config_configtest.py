# -*- coding: utf-8 -*-

from .builder import cpp

def getDependModules(
):
    return [
        'adhocrepeater_testdata_configtest',
    ]

def build(
    _context,
):
    cpp.gtest(
        _context,
        'adhocrepeater-config-configtest',
        [
            {
                'config' : [
                    'configtest.cpp',
                    'config.cpp',
                    {
                        'element' : [
                            'element.cpp',
                            'string.cpp',
                            'list.cpp',
                            'map.cpp',
                        ],
                    },
                ],
            },
            {
                'common' : [
                    'file.cpp',
                    'time.cpp',
                ],
            },
            {
                'packet' : [
                    'macaddress.cpp',
                ],
            },
        ],
    )
