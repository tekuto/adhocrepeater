# -*- coding: utf-8 -*-

from .builder import cpp

def getDependModules(
):
    return [
    ]

def build(
    _context,
):
    cpp.gtest(
        _context,
        'adhocrepeater-ssid-manager-ssidtest',
        {
            'ssid' : [
                {
                    'manager' : [
                        'ssidtest.cpp',
                        'ssid.cpp',
                    ],
                },
            ],
        },
    )
