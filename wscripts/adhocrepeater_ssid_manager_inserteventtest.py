# -*- coding: utf-8 -*-

from .builder import cpp

def getDependModules(
):
    return [
    ]

def build(
    _context,
):
    cpp.gtest(
        _context,
        'adhocrepeater-ssid-manager-inserteventtest',
        {
            'ssid' : [
                {
                    'manager' : [
                        'inserteventtest.cpp',
                        'insertevent.cpp',
                        'manager.cpp',
                        'ssid.cpp',
                        'eventhandlers.cpp',
                        'inserteventhandler.cpp',
                    ],
                },
            ],
        },
    )
