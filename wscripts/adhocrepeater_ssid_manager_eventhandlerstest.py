# -*- coding: utf-8 -*-

from .builder import cpp

def getDependModules(
):
    return [
    ]

def build(
    _context,
):
    cpp.gtest(
        _context,
        'adhocrepeater-ssid-manager-eventhandlerstest',
        {
            'ssid' : [
                {
                    'manager' : [
                        'eventhandlerstest.cpp',
                        'eventhandlers.cpp',
                        'inserteventhandler.cpp',
                        'insertevent.cpp',
                        'manager.cpp',
                        'ssid.cpp',
                    ],
                },
            ],
        },
    )
