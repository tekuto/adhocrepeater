﻿#include "adhocrepeater/util/test.h"
#include "adhocrepeater/common/file.h"

#include <string>

TEST(
    FileTest
    , ReadFile
)
{
    auto    content = std::string();
    ASSERT_TRUE(
        adhocrepeater::readFile(
            content
            , "test/filetest/filetest.txt"
        )
    );

    ASSERT_STREQ( "FILETEST\n", content.c_str() );
}
