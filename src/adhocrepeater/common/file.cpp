﻿#include "adhocrepeater/common/file.h"

#include <memory>
#include <array>
#include <new>
#include <utility>
#include <cstdio>

namespace {
    const auto  BUFFER_SIZE = 1024;

    typedef std::array< char, BUFFER_SIZE > Buffer;

    struct File
    {
        struct FileCloser
        {
            void operator()(
                std::FILE * _file
            ) const
            {
                std::fclose( _file );
            }
        };

        typedef std::unique_ptr<
            std::FILE
            , FileCloser
        > FileUnique;

        FileUnique  fileUnique;
    };

    typedef std::unique_ptr< File > FileUnique;

    File * newFile(
        const std::string & _FILE_PATH
    )
    {
        auto    fileUnique = File::FileUnique(
            std::fopen(
                _FILE_PATH.c_str()
                , "r"
            )
        );
        if( fileUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:ファイルのオープンに失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    thisUnique = FileUnique(
            new( std::nothrow )File{
                std::move( fileUnique ),
            }
        );
        if( thisUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:ファイルの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        return thisUnique.release();
    }

    bool isEof(
        File &  _this
    )
    {
        if( std::feof( _this.fileUnique.get() ) == 0 ) {
            return false;
        }

        return true;
    }

    bool read(
        File &      _this
        , Buffer &  _buffer
        , size_t &  _readSize
    )
    {
        auto    filePtr = _this.fileUnique.get();

        const auto  BUFFER_SIZE = _buffer.size();

        auto    bufferPtr = _buffer.data();

        const auto  READ_SIZE = std::fread(
            bufferPtr
            , 1
            , BUFFER_SIZE
            , filePtr
        );
        if( std::ferror( filePtr ) != 0 ) {
#ifdef  DEBUG
            std::printf( "E:ファイル読み込みエラー\n" );
#endif  // DEBUG

            return false;
        }

        _readSize = READ_SIZE;

        return true;
    }
}

namespace adhocrepeater {
    bool readFile(
        std::string &           _content
        , const std::string &   _FILE_PATH
    )
    {
        auto    fileUnique = FileUnique( newFile( _FILE_PATH ) );
        if( fileUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:ファイル生成に失敗\n" );
#endif  // DEBUG

            return false;
        }
        auto &  file = *fileUnique;

        auto    content = std::string();

        while( isEof( file ) == false ) {
            auto    buffer = Buffer();

            auto    readSize = size_t();

            if( read(
                file
                , buffer
                , readSize
            ) == false ) {
#ifdef  DEBUG
                std::printf( "E:ファイル読み込み中にエラーが発生\n" );
#endif  // DEBUG

                return false;
            }

            if( readSize > 0 ) {
                content.append(
                    buffer.data()
                    , readSize
                );
            }
        }

        _content = std::move( content );

        return true;
    }
}
