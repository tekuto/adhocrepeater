﻿#include "adhocrepeater/util/test.h"
#include "adhocrepeater/common/thread.h"
#include "adhocrepeater/common/unique.h"

#include <thread>
#include <condition_variable>

TEST(
    ThreadTest
    , New
)
{
    auto    threadUnique = adhocrepeater::unique(
        adhocrepeater::newThread(
            [](
                adhocrepeater::Thread &
            )
            {
            }
        )
    );
    ASSERT_NE( nullptr, threadUnique.get() );

    const auto &    THREAD = *threadUnique;
    ASSERT_TRUE( THREAD.running );
    ASSERT_NE( std::thread::id(), THREAD.thread.get_id() );
    ASSERT_NE( nullptr, THREAD.threadJoiner.get() );
}

void wait(
    adhocrepeater::Thread &     _thread
    , std::condition_variable & _cond
    , bool &                    _locked
)
{
    auto    lock = std::unique_lock< std::mutex >( _thread.mutex );

    _locked = true;
    _cond.notify_one();

    if( _thread.running == true ) {
        _cond.wait( lock );
    }
}

void notify(
    adhocrepeater::Thread &     _thread
    , std::condition_variable & _cond
    , const bool &              _LOCKED
)
{
    auto    lock = std::unique_lock< std::mutex >( _thread.mutex );

    if( _LOCKED == false ) {
        _cond.wait( lock );
    }

    _cond.notify_one();
}

TEST(
    ThreadTest
    , End
)
{
    std::condition_variable cond;
    auto                    running = bool( true );
    auto                    locked = bool( false );

    auto    threadUnique = adhocrepeater::unique(
        adhocrepeater::newThread(
            [
                &cond
                , &running
                , &locked
            ]
            (
                adhocrepeater::Thread & _this
            )
            {
                wait(
                    _this
                    , cond
                    , locked
                );

                running = _this.running;
            }
        )
    );
    ASSERT_NE( nullptr, threadUnique.get() );
    auto &  thread = *threadUnique;

    end( thread );

    notify(
        thread
        , cond
        , locked
    );

    ASSERT_FALSE( running );
}

void testIsRunning(
    bool &  _running
)
{
    auto    threadUnique = adhocrepeater::unique(
        adhocrepeater::newThread(
            [
                &_running
            ]
            (
                adhocrepeater::Thread & _this
            )
            {
                _running = adhocrepeater::isRunning( _this );
            }
        )
    );
    ASSERT_NE( nullptr, threadUnique.get() );
}

TEST(
    ThreadTest
    , IsRunning
)
{
    auto    running = bool( false );
    ASSERT_NO_FATAL_FAILURE( testIsRunning( running ) );
    ASSERT_TRUE( running );
}
