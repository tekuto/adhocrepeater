﻿#include "adhocrepeater/common/time.h"

#include <thread>
#include <utility>
#include <cstdlib>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace adhocrepeater {
    bool intervalProc(
        const Duration &    _INTERVAL
        , const LoopProc &  _PROC
    )
    {
        while( true ) {
            auto    loopEnd = false;

            const auto  RESULT = _PROC( loopEnd );

            if( loopEnd == true ) {
                return RESULT;
            }

            std::this_thread::sleep_for( _INTERVAL );
        }
    }

    bool intervalProc(
        const LoopProc &    _PROC
    )
    {
        while( true ) {
            auto    loopEnd = false;

            const auto  RESULT = _PROC( loopEnd );

            if( loopEnd == true ) {
                return RESULT;
            }
        }
    }

    bool timeoutProc(
        const Duration &    _TIMEOUT
        , const Duration &  _INTERVAL
        , const LoopProc &  _PROC
    )
    {
        const auto  START_TIME = Time::now();

        return intervalProc(
            _INTERVAL
            , [
                &_TIMEOUT
                , &_PROC
                , &START_TIME
            ]
            (
                bool &  _loopEnd
            )
            {
                const auto  RESULT = _PROC( _loopEnd );

                if( _loopEnd == true ) {
                    return RESULT;
                }

                const auto  ELAPSED_TIME = Time::now() - START_TIME;
                if( ELAPSED_TIME > _TIMEOUT ) {
#ifdef  DEBUG
                    std::printf( "I:タイムアウト\n" );
#endif  // DEBUG

                    _loopEnd = true;
                    return false;
                }

                return true;
            }
        );
    }

    bool timeoutProc(
        const Duration &    _TIMEOUT
        , const LoopProc &  _PROC
    )
    {
        const auto  START_TIME = Time::now();

        return intervalProc(
            [
                &_TIMEOUT
                , &_PROC
                , &START_TIME
            ]
            (
                bool &  _loopEnd
            )
            {
                const auto  RESULT = _PROC( _loopEnd );

                if( _loopEnd == true ) {
                    return RESULT;
                }

                const auto  ELAPSED_TIME = Time::now() - START_TIME;
                if( ELAPSED_TIME > _TIMEOUT ) {
#ifdef  DEBUG
                    std::printf( "I:タイムアウト\n" );
#endif  // DEBUG

                    _loopEnd = true;
                    return false;
                }

                return true;
            }
        );
    }

    bool stringToMicroSeconds(
        MicroSeconds &          _microSeconds
        , const std::string &   _STRING
    )
    {
        auto    endPtr = static_cast< char * >( nullptr );
        auto    microSeconds = MicroSeconds(
            std::strtoll(
                _STRING.c_str()
                , &endPtr
                , 10
            )
        );
        if( endPtr == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:マイクロ秒の数値変換に失敗\n" );
#endif  // DEBUG

            return false;
        }
        if( *endPtr != '\0' ) {
#ifdef  DEBUG
            std::printf( "E:マイクロ秒に数値以外の文字が含まれている\n" );
#endif  // DEBUG

            return false;
        }

        _microSeconds = std::move( microSeconds );

        return true;
    }
}
