﻿#include "adhocrepeater/common/thread.h"
#include "adhocrepeater/common/unique.h"

#include <new>
#include <thread>
#include <system_error>
#include <mutex>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    void join(
        adhocrepeater::Thread & _thread
    )
    {
        _thread.thread.join();
    }

    bool startThread(
        adhocrepeater::Thread &             _thread
        , const adhocrepeater::ThreadProc & _PROC
    )
    {
        try {
            _thread.thread = std::thread(
                [
                    &_thread
                    , _PROC
                ]
                {
                    _PROC( _thread );
                }
            );
        } catch( std::system_error & ) {
#ifdef  DEBUG
            std::printf( "E:スレッドの起動に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool initThread(
        adhocrepeater::Thread &             _thread
        , const adhocrepeater::ThreadProc & _PROC
    )
    {
        _thread.running = true;

        if( startThread(
            _thread
            , _PROC
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:スレッドの開始に失敗\n" );
#endif  // DEBUG

            return false;
        }

        _thread.threadJoiner.reset( &_thread );

        return true;
    }
}

namespace adhocrepeater {
    void Thread::JoinThread::operator()(
        Thread *    _threadPtr
    ) const
    {
        join( *_threadPtr );
    }

    Thread * newThread(
        const ThreadProc &  _PROC
    )
    {
        auto    thisUnique = unique( new( std::nothrow )Thread );
        if( thisUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:スレッドの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }
        auto &  thread = *thisUnique;

        if( initThread(
            thread
            , _PROC
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:スレッドの初期化に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        return thisUnique.release();
    }

    void free(
        Thread &    _this
    )
    {
        delete &_this;
    }

    void end(
        Thread &    _this
    )
    {
        auto    lock = std::unique_lock< std::mutex >( _this.mutex );

        _this.running = false;
    }

    bool isRunning(
        Thread &    _this
    )
    {
        auto    lock = std::unique_lock< std::mutex >( _this.mutex );

        return _this.running;
    }
}
