﻿#include "adhocrepeater/util/test.h"
#include "adhocrepeater/data/sender.h"
#include "adhocrepeater/ssid/sender.h"
#include "adhocrepeater/ssid/changer.h"
#include "adhocrepeater/config/config.h"
#include "adhocrepeater/common/thread.h"
#include "adhocrepeater/common/unique.h"

TEST(
    DataSenderTest
    , New
)
{
    auto    configUnique = adhocrepeater::unique( adhocrepeater::newConfig( "test/testconf/testconf.conf" ) );
    ASSERT_NE( nullptr, configUnique.get() );
    const auto &    CONFIG = *configUnique;

    auto    ssidSenderUnique = adhocrepeater::unique( adhocrepeater::newSsidSender( CONFIG ) );
    ASSERT_NE( nullptr, ssidSenderUnique.get() );
    auto &  ssidSender = *ssidSenderUnique;

    adhocrepeater::end( *( ssidSender.sendThreadUnique ) );

    auto    changerUnique = adhocrepeater::unique(
        adhocrepeater::newSsidChanger(
            CONFIG
            , ssidSender
        )
    );
    ASSERT_NE( nullptr, changerUnique.get() );
    auto &  changer = *changerUnique;

    adhocrepeater::end( *( changer.threadUnique ) );

    auto    dataSenderUnique = adhocrepeater::unique(
        adhocrepeater::newDataSender(
            CONFIG
            , changer
        )
    );
    ASSERT_NE( nullptr, dataSenderUnique.get() );

    auto &  dataSender = *dataSenderUnique;
    ASSERT_NE( nullptr, dataSender.packetManagerUnique.get() );
    ASSERT_NE( nullptr, dataSender.receiveThreadUnique.get() );
    ASSERT_NE( nullptr, dataSender.sendThreadUnique.get() );

    adhocrepeater::end( *( dataSender.receiveThreadUnique ) );
    adhocrepeater::end( *( dataSender.sendThreadUnique ) );
}
