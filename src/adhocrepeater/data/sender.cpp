﻿#include "adhocrepeater/data/sender.h"
#include "adhocrepeater/packet/manager.h"
#include "adhocrepeater/packet/buffer.h"
#include "adhocrepeater/packet/raw/receiver.h"
#include "adhocrepeater/packet/udp/sender.h"
#include "adhocrepeater/ssid/changer.h"
#include "adhocrepeater/config/config.h"
#include "adhocrepeater/common/thread.h"
#include "adhocrepeater/common/time.h"
#include "adhocrepeater/common/unique.h"

#include <new>
#include <utility>
#include <cstring>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    bool isTargetDevice(
        const adhocrepeater::Config &   _CONFIG
        , const adhocrepeater::Packet & _PACKET
    )
    {
        const auto &    SENDER = _CONFIG.sender;

        const auto  PACKET_SIZE = _PACKET.size();
        if( PACKET_SIZE < adhocrepeater::MAC_ADDRESS_LENGTH * 2 ) {
#ifdef  DEBUG
            std::printf( "E:パケットサイズが小さすぎる\n" );
#endif  // DEBUG

            return false;
        }

        const auto &    MAC_ADDRESS = SENDER.deviceAddress;

        if( std::memcmp(
            _PACKET.data() + adhocrepeater::MAC_ADDRESS_LENGTH
            , MAC_ADDRESS.data()
            , MAC_ADDRESS.size()
        ) != 0 ) {
#ifdef  DEBUG
            std::printf( "I:送信者MACアドレスが一致しない\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool pollAndReceive(
        const adhocrepeater::Config &   _CONFIG
        , adhocrepeater::RawReceiver &  _receiver
        , adhocrepeater::Packet &       _packet
    )
    {
        const auto &    SENDER = _CONFIG.sender;

        if( poll(
            _receiver
            , SENDER.pollingTimeoutMSeconds
        ) == false ) {
#ifdef  DEBUG
            std::printf( "I:ポーリングに失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( adhocrepeater::receive(
            _receiver
            , _packet
        ) == false ) {
#ifdef  DEDUB
            std::printf( "E:RAW受信に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( isTargetDevice(
            _CONFIG
            , _packet
        ) == false ) {
#ifdef  DEBUG
            std::printf( "I:中継対象のデバイスではない\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool receive(
        const adhocrepeater::Config &   _CONFIG
        , adhocrepeater::RawReceiver &  _receiver
        , adhocrepeater::Packet &       _packet
    )
    {
        const auto &    SENDER = _CONFIG.sender;

        return adhocrepeater::timeoutProc(
            SENDER.receiveRawTimeout
            , [
                &_CONFIG
                , &_receiver
                , &_packet
            ]
            (
                bool &  _loopEnd
            )
            {
                if( pollAndReceive(
                    _CONFIG
                    , _receiver
                    , _packet
                ) == true ) {
                    _loopEnd = true;

                    return true;
                }

                return false;
            }
        );
    }

    void receiveProc(
        const adhocrepeater::Config &       _CONFIG
        , adhocrepeater::SsidChanger &      _changer
        , adhocrepeater::PacketManager &    _manager
        , adhocrepeater::RawReceiver &      _receiver
    )
    {
        auto    packet = adhocrepeater::Packet();
        if( ::receive(
            _CONFIG
            , _receiver
            , packet
        ) == false ) {
#ifdef  DEBUG
            std::printf( "I:データ受信に失敗\n" );
#endif  // DEBUG

            adhocrepeater::disconnect( _changer );

            return;
        }

        if( adhocrepeater::add(
            _manager
            , std::move( packet )
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:パケットの追加に失敗\n" );
#endif  // DEBUG

            return;
        }
    }

    void receiveProc(
        adhocrepeater::Thread &             _thread
        , const adhocrepeater::Config &     _CONFIG
        , adhocrepeater::SsidChanger &      _changer
        , adhocrepeater::PacketManager &    _manager
    )
    {
        const auto &    INTERFACES = _CONFIG.interfaces;

        auto    receiverUnique = adhocrepeater::unique( adhocrepeater::newRawReceiver( INTERFACES.repeater ) );
        if( receiverUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:RAW受信の生成に失敗\n" );
#endif  // DEBUG

            return;
        }
        auto &  receiver = *receiverUnique;

        while( adhocrepeater::isRunning( _thread ) == true ) {
            receiveProc(
                _CONFIG
                , _changer
                , _manager
                , receiver
            );
        }
    }

    void sendProc(
        adhocrepeater::PacketManager &  _manager
        , adhocrepeater::UdpSender &    _sender
        , adhocrepeater::PacketBuffer & _buffer
    )
    {
        adhocrepeater::getBuffer(
            _manager
            , _buffer
        );

        for( const auto & PACKET : _buffer ) {
            if( adhocrepeater::send(
                _sender
                , PACKET
            ) == false ) {
#ifdef  DEBUG
                std::printf( "E:UDP送信に失敗\n" );
#endif  // DEBUG

                return;
            }
        }
    }

    void sendProc(
        adhocrepeater::Thread &             _thread
        , const adhocrepeater::Config &     _CONFIG
        , adhocrepeater::PacketManager &    _manager
    )
    {
        const auto &    PEER = _CONFIG.peer;

        auto    senderUnique = adhocrepeater::unique(
            adhocrepeater::newUdpSender(
                PEER.address
                , PEER.dataPort
            )
        );
        if( senderUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:UDP送信の生成に失敗\n" );
#endif  // DEBUG

            return;
        }
        auto &  sender = *senderUnique;

        auto    buffer = adhocrepeater::PacketBuffer();
        while( adhocrepeater::isRunning( _thread ) == true ) {
            sendProc(
                _manager
                , sender
                , buffer
            );
        }
    }
}

namespace adhocrepeater {
    DataSender * newDataSender(
        const Config &  _CONFIG
        , SsidChanger & _changer
    )
    {
        auto    packetManagerUnique = unique( newPacketManager() );
        if( packetManagerUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:パケットマネージャの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }
        auto &  packetManager = *packetManagerUnique;

        auto    receiveThreadUnique = unique(
            newThread(
                [
                    &_CONFIG
                    , &_changer
                    , &packetManager
                ]
                (
                    Thread &    _thread
                )
                {
                    receiveProc(
                        _thread
                        , _CONFIG
                        , _changer
                        , packetManager
                    );
                }
            )
        );
        if( receiveThreadUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:データ読み取りスレッドの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    sendThreadUnique = unique(
            newThread(
                [
                    &_CONFIG
                    , &packetManager
                ]
                (
                    Thread &    _thread
                )
                {
                    sendProc(
                        _thread
                        , _CONFIG
                        , packetManager
                    );
                }
            )
        );
        if( sendThreadUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:データ送信スレッドの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    thisUnique = unique(
            new( std::nothrow )DataSender{
                std::move( packetManagerUnique ),
                std::move( receiveThreadUnique ),
                std::move( sendThreadUnique ),
            }
        );
        if( thisUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:データ送信の生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        return thisUnique.release();
    }

    void free(
        DataSender &    _this
    )
    {
        delete &_this;
    }
}
