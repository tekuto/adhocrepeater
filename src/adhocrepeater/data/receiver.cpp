﻿#include "adhocrepeater/data/receiver.h"
#include "adhocrepeater/packet/manager.h"
#include "adhocrepeater/packet/buffer.h"
#include "adhocrepeater/packet/udp/receiver.h"
#include "adhocrepeater/packet/raw/sender.h"
#include "adhocrepeater/config/config.h"
#include "adhocrepeater/common/thread.h"
#include "adhocrepeater/common/time.h"
#include "adhocrepeater/common/unique.h"

#include <new>
#include <utility>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    bool pollAndReceive(
        const adhocrepeater::Config &   _CONFIG
        , adhocrepeater::UdpReceiver &  _receiver
        , adhocrepeater::Packet &       _packet
    )
    {
        const auto &    RECEIVER = _CONFIG.receiver;

        if( poll(
            _receiver
            , RECEIVER.pollingTimeoutMSeconds
        ) == false ) {
#ifdef  DEBUG
            std::printf( "I:ポーリングに失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( adhocrepeater::receive(
            _receiver
            , _packet
        ) == false ) {
#ifdef  DEDUB
            std::printf( "E:UDP受信に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool receive(
        const adhocrepeater::Config &   _CONFIG
        , adhocrepeater::UdpReceiver &  _receiver
        , adhocrepeater::Packet &       _packet
    )
    {
        const auto &    RECEIVER = _CONFIG.receiver;

        return adhocrepeater::timeoutProc(
            RECEIVER.receiveUdpTimeout
            , [
                &_CONFIG
                , &_receiver
                , &_packet
            ]
            (
                bool &  _loopEnd
            )
            {
                if( pollAndReceive(
                    _CONFIG
                    , _receiver
                    , _packet
                ) == true ) {
                    _loopEnd = true;

                    return true;
                }

                return false;
            }
        );
    }

    void receiveProc(
        const adhocrepeater::Config &       _CONFIG
        , adhocrepeater::PacketManager &    _manager
        , adhocrepeater::UdpReceiver &      _receiver
    )
    {
        auto    packet = adhocrepeater::Packet();
        if( ::receive(
            _CONFIG
            , _receiver
            , packet
        ) == false ) {
#ifdef  DEBUG
            std::printf( "I:データ受信に失敗\n" );
#endif  // DEBUG

            return;
        }

        if( adhocrepeater::add(
            _manager
            , std::move( packet )
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:パケットの追加に失敗\n" );
#endif  // DEBUG

            return;
        }
    }

    void receiveProc(
        adhocrepeater::Thread &             _thread
        , const adhocrepeater::Config &     _CONFIG
        , adhocrepeater::PacketManager &    _manager
    )
    {
        const auto &    PORTS = _CONFIG.ports;

        auto    receiverUnique = adhocrepeater::unique( adhocrepeater::newUdpReceiver( PORTS.data ) );
        if( receiverUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:UDP受信の生成に失敗\n" );
#endif  // DEBUG

            return;
        }
        auto &  receiver = *receiverUnique;

        while( adhocrepeater::isRunning( _thread ) == true ) {
            receiveProc(
                _CONFIG
                , _manager
                , receiver
            );
        }
    }

    void sendProc(
        adhocrepeater::PacketManager &  _manager
        , adhocrepeater::RawSender &    _sender
        , adhocrepeater::PacketBuffer & _buffer
    )
    {
        adhocrepeater::getBuffer(
            _manager
            , _buffer
        );

        for( const auto & PACKET : _buffer ) {
            if( adhocrepeater::send(
                _sender
                , PACKET
            ) == false ) {
#ifdef  DEBUG
                std::printf( "E:RAW送信に失敗\n" );
#endif  // DEBUG

                return;
            }
        }
    }

    void sendProc(
        adhocrepeater::Thread &             _thread
        , const adhocrepeater::Config &     _CONFIG
        , adhocrepeater::PacketManager &    _manager
    )
    {
        const auto &    INTERFACES = _CONFIG.interfaces;

        auto    senderUnique = adhocrepeater::unique( adhocrepeater::newRawSender( INTERFACES.repeater ) );
        if( senderUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:RAW送信の生成に失敗\n" );
#endif  // DEBUG

            return;
        }
        auto &  sender = *senderUnique;

        auto    buffer = adhocrepeater::PacketBuffer();
        while( adhocrepeater::isRunning( _thread ) == true ) {
            sendProc(
                _manager
                , sender
                , buffer
            );
        }
    }
}

namespace adhocrepeater {
    DataReceiver * newDataReceiver(
        const Config &  _CONFIG
    )
    {
        auto    packetManagerUnique = unique( newPacketManager() );
        if( packetManagerUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:パケットマネージャの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }
        auto &  packetManager = *packetManagerUnique;

        auto    receiveThreadUnique = unique(
            newThread(
                [
                    &_CONFIG
                    , &packetManager
                ]
                (
                    Thread &    _thread
                )
                {
                    receiveProc(
                        _thread
                        , _CONFIG
                        , packetManager
                    );
                }
            )
        );
        if( receiveThreadUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:データ受信スレッドの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    sendThreadUnique = unique(
            newThread(
                [
                    &_CONFIG
                    , &packetManager
                ]
                (
                    Thread &    _thread
                )
                {
                    sendProc(
                        _thread
                        , _CONFIG
                        , packetManager
                    );
                }
            )
        );
        if( sendThreadUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:データ送信スレッドの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    thisUnique = unique(
            new( std::nothrow )DataReceiver{
                std::move( packetManagerUnique ),
                std::move( receiveThreadUnique ),
                std::move( sendThreadUnique ),
            }
        );
        if( thisUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:データ受信の生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        return thisUnique.release();
    }

    void free(
        DataReceiver &  _this
    )
    {
        delete &_this;
    }
}
