﻿#include "adhocrepeater/util/test.h"
#include "adhocrepeater/data/receiver.h"
#include "adhocrepeater/config/config.h"
#include "adhocrepeater/common/thread.h"
#include "adhocrepeater/common/unique.h"

TEST(
    DataReceiverTest
    , New
)
{
    auto    configUnique = adhocrepeater::unique( adhocrepeater::newConfig( "test/testconf/testconf.conf" ) );
    ASSERT_NE( nullptr, configUnique.get() );
    const auto &    CONFIG = *configUnique;

    auto    receiverUnique = adhocrepeater::unique( adhocrepeater::newDataReceiver( CONFIG ) );
    ASSERT_NE( nullptr, receiverUnique.get() );

    auto &  receiver = *receiverUnique;
    ASSERT_NE( nullptr, receiver.packetManagerUnique.get() );
    ASSERT_NE( nullptr, receiver.receiveThreadUnique.get() );
    ASSERT_NE( nullptr, receiver.sendThreadUnique.get() );

    adhocrepeater::end( *( receiver.receiveThreadUnique ) );
    adhocrepeater::end( *( receiver.sendThreadUnique ) );
}
