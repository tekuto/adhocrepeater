﻿#include "adhocrepeater/util/test.h"
#include "adhocrepeater/config/config.h"
#include "adhocrepeater/common/time.h"
#include "adhocrepeater/common/unique.h"

#include <string>

TEST(
    ConfigTest
    , New
)
{
    auto    configUnique = adhocrepeater::unique(
        adhocrepeater::newConfig(
            "test/configtest/configtest.conf"
        )
    );
    ASSERT_NE( nullptr, configUnique.get() );
    const auto &    CONFIG = *configUnique;

    const auto &    INTERFACES = CONFIG.interfaces;
    ASSERT_STREQ( "scannerInterface", INTERFACES.scanner.c_str() );
    ASSERT_STREQ( "repeaterInterface", INTERFACES.repeater.c_str() );

    const auto &    TARGET_SSID = CONFIG.targetSsid;
    ASSERT_STREQ( "ssidPrefix", TARGET_SSID.prefix.c_str() );
    ASSERT_EQ( 10, TARGET_SSID.channel );
    ASSERT_EQ( 20, TARGET_SSID.prefixSize );

    const auto &    PORTS = CONFIG.ports;
    ASSERT_EQ( 10, PORTS.data );
    ASSERT_EQ( 20, PORTS.ssid );

    const auto &    PEER = CONFIG.peer;
    ASSERT_STREQ( "127.0.0.1", PEER.address.c_str() );
    ASSERT_EQ( 10, PEER.dataPort );
    ASSERT_EQ( 20, PEER.ssidPort );

    const auto &    SCANNER = CONFIG.scanner;
    ASSERT_EQ( 10, SCANNER.scanInterval.count() );
    ASSERT_EQ( 20, SCANNER.getScanDataTimeout.count() );
    ASSERT_EQ( 30, SCANNER.getScanDataInterval.count() );

    const auto &    CHANGER = CONFIG.changer;
    ASSERT_EQ( 10, CHANGER.disconnectableTime.count() );
    ASSERT_EQ( 20, CHANGER.ssidManagerKeepTime.count() );
    ASSERT_EQ( 30, CHANGER.findSsidTimeout.count() );
    ASSERT_EQ( 40, CHANGER.findSsidInterval.count() );
    ASSERT_EQ( 50, CHANGER.getScanDataTimeout.count() );
    ASSERT_EQ( 60, CHANGER.getScanDataInterval.count() );
    ASSERT_EQ( 70, CHANGER.getSsidWaitTime.count() );
    ASSERT_EQ( 80, CHANGER.ssidHistorySize );

    const auto &    SENDER = CONFIG.sender;
    ASSERT_EQ( 0x12, SENDER.deviceAddress[ 0 ] );
    ASSERT_EQ( 0x34, SENDER.deviceAddress[ 1 ] );
    ASSERT_EQ( 0x56, SENDER.deviceAddress[ 2 ] );
    ASSERT_EQ( 0x78, SENDER.deviceAddress[ 3 ] );
    ASSERT_EQ( 0x9a, SENDER.deviceAddress[ 4 ] );
    ASSERT_EQ( 0xbc, SENDER.deviceAddress[ 5 ] );
    ASSERT_EQ( 10, SENDER.receiveRawTimeout.count() );
    ASSERT_EQ( 20, SENDER.pollingTimeoutMSeconds );

    const auto &    RECEIVER = CONFIG.receiver;
    ASSERT_EQ( 20, RECEIVER.receiveUdpTimeout.count() );
    ASSERT_EQ( 30, RECEIVER.pollingTimeoutMSeconds );
}
