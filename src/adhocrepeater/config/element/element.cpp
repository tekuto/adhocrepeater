﻿#include "adhocrepeater/config/element/element.h"
#include "adhocrepeater/config/element/string.h"
#include "adhocrepeater/config/element/list.h"
#include "adhocrepeater/config/element/map.h"
#include "adhocrepeater/common/unique.h"

#include <string>
#include <new>
#include <utility>
#include <cctype>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    const char * init(
        adhocrepeater::Unique< adhocrepeater::ConfigElementString > &   _stringUnique
        , adhocrepeater::Unique< adhocrepeater::ConfigElementList > &   _listUnique
        , adhocrepeater::Unique< adhocrepeater::ConfigElementMap > &    _mapUnique
        , const char *                                                  _ELEMENT
        , const char *                                                  _END
    )
    {
        const auto  END_STRING_ELEMENT = adhocrepeater::init(
            _stringUnique
            , _ELEMENT
            , _END
        );
        if( END_STRING_ELEMENT != nullptr ) {
            return END_STRING_ELEMENT;
        }

        const auto  END_LIST_ELEMENT = adhocrepeater::init(
            _listUnique
            , _ELEMENT
            , _END
        );
        if( END_LIST_ELEMENT != nullptr ) {
            return END_LIST_ELEMENT;
        }

        const auto  END_MAP_ELEMENT = adhocrepeater::init(
            _mapUnique
            , _ELEMENT
            , _END
        );
        if( END_MAP_ELEMENT != nullptr ) {
            return END_MAP_ELEMENT;
        }

        return nullptr;
    }
}

namespace adhocrepeater {
    ConfigElement * newConfigElement(
        const std::string & _CONFIG
    )
    {
        const auto  CONFIG = _CONFIG.c_str();
        const auto  END = CONFIG + _CONFIG.length();

        const auto  ELEMENT = getElement(
            CONFIG
            , END
        );
        if( ELEMENT == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:要素開始位置取得に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    thisUnique = Unique< ConfigElement >();

        const auto  END_ELEMENT = init(
            thisUnique
            , ELEMENT
            , END
        );
        if( END_ELEMENT == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:ConfigElementの初期化に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        if( getElement(
            END_ELEMENT
            , END
        ) != nullptr ) {
#ifdef  DEBUG
            std::printf( "E:要素終了位置よりも後に要素が存在する\n" );
#endif  // DEBUG

            return nullptr;
        }

        return thisUnique.release();
    }

    void free(
        ConfigElement & _this
    )
    {
        delete &_this;
    }

    const char * getElement(
        const char *    _CONFIG
        , const char *  _END
    )
    {
        auto    element = static_cast< const char * >( nullptr );

        const auto  LENGTH = size_t( _END - _CONFIG );
        for( auto i = size_t( 0 ) ; i < LENGTH ; i++ ) {
            const auto &    CHAR_REF = _CONFIG[ i ];
            if( std::isspace( CHAR_REF ) == 0 ) {
                element = &CHAR_REF;

                break;
            }
        }

        if( element == nullptr ) {
            return nullptr;
        }

        return element;
    }

    const char * init(
        Unique< ConfigElement > &   _configElementUnique
        , const char *              _ELEMENT
        , const char *              _END
    )
    {
        auto    stringUnique = Unique< ConfigElementString >();
        auto    listUnique = Unique< ConfigElementList >();
        auto    mapUnique = Unique< ConfigElementMap >();

        const auto  END_ELEMENT = ::init(
            stringUnique
            , listUnique
            , mapUnique
            , _ELEMENT
            , _END
        );
        if( END_ELEMENT == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:要素の生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    configElementUnique = unique(
            new( std::nothrow )ConfigElement{
                std::move( stringUnique ),
                std::move( listUnique ),
                std::move( mapUnique ),
            }
        );
        if( configElementUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:ConfigElement生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        _configElementUnique = std::move( configElementUnique );

        return END_ELEMENT;
    }

    const ConfigElementString * getString(
        const ConfigElement &   _THIS
    )
    {
        return _THIS.stringUnique.get();
    }

    const ConfigElementList * getList(
        const ConfigElement &   _THIS
    )
    {
        return _THIS.listUnique.get();
    }

    const ConfigElementMap * getMap(
        const ConfigElement &   _THIS
    )
    {
        return _THIS.mapUnique.get();
    }
}
