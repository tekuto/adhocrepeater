﻿#include "adhocrepeater/util/test.h"
#include "adhocrepeater/config/element/element.h"
#include "adhocrepeater/config/element/string.h"
#include "adhocrepeater/config/element/list.h"
#include "adhocrepeater/config/element/map.h"
#include "adhocrepeater/common/unique.h"

#include <string>
#include <vector>
#include <map>

void testNew_Failed(
    const std::string & _CONFIG
)
{
    auto    configElementUnique = adhocrepeater::unique(
        adhocrepeater::newConfigElement(
            _CONFIG
        )
    );
    ASSERT_EQ( nullptr, configElementUnique.get() );
}

TEST(
    ConfigElementTest
    , New_Failed
)
{
    ASSERT_NO_FATAL_FAILURE( testNew_Failed( "" ) );
    ASSERT_NO_FATAL_FAILURE( testNew_Failed( "ABCDE" ) );
}

void testNewFromString(
    const std::string & _CONFIG
)
{
    auto    configElementUnique = adhocrepeater::unique(
        adhocrepeater::newConfigElement(
            _CONFIG
        )
    );
    ASSERT_NE( nullptr, configElementUnique.get() );
}

TEST(
    ConfigElementTest
    , NewFromString
)
{
    ASSERT_NO_FATAL_FAILURE( testNewFromString( "\"\"" ) );
    ASSERT_NO_FATAL_FAILURE( testNewFromString( "\"ABCDE\"" ) );
}

TEST(
    ConfigElementTest
    , NewFromString_Failed
)
{
    ASSERT_NO_FATAL_FAILURE( testNew_Failed( "\"" ) );
    ASSERT_NO_FATAL_FAILURE( testNew_Failed( "\"ABCDE" ) );
}

void testNewFromString_ReturnValue(
    const std::string & _CONFIG
    , const char *      _STRING
)
{
    auto    configElementUnique = adhocrepeater::unique(
        adhocrepeater::newConfigElement(
            _CONFIG
        )
    );
    ASSERT_NE( nullptr, configElementUnique.get() );

    const auto &    CONFIG_ELEMENT = *configElementUnique;
    const auto      STRING_PTR = CONFIG_ELEMENT.stringUnique.get();
    ASSERT_NE( nullptr, STRING_PTR );
    const auto &    STRING = adhocrepeater::getString( *STRING_PTR );
    ASSERT_STREQ( _STRING, STRING.c_str() );
}

TEST(
    ConfigElementTest
    , NewFromString_ReturnValue
)
{
    ASSERT_NO_FATAL_FAILURE(
        testNewFromString_ReturnValue(
            "\"\""
            , ""
        )
    );
    ASSERT_NO_FATAL_FAILURE(
        testNewFromString_ReturnValue(
            "\"Hello, world!!\""
            , "Hello, world!!"
        )
    );
}

TEST(
    ConfigElementTest
    , New_BeforeSpaces
)
{
    ASSERT_NO_FATAL_FAILURE(
        testNewFromString_ReturnValue(
            " \t\n\"Hello, world!!\""
            , "Hello, world!!"
        )
    );
}

TEST(
    ConfigElementTest
    , New_AfterSpaces
)
{
    ASSERT_NO_FATAL_FAILURE(
        testNewFromString_ReturnValue(
            "\"Hello, world!!\" \t\n"
            , "Hello, world!!"
        )
    );
}

TEST(
    ConfigElementTest
    , New_AfterSpaces_Failed
)
{
    ASSERT_NO_FATAL_FAILURE(
        testNew_Failed(
            "\"Hello, world!!\" \t\nABCED"
        )
    );
}

TEST(
    ConfigElementTest
    , NewFromString_EscapeDoubleQuotation
)
{
    ASSERT_NO_FATAL_FAILURE(
        testNewFromString_ReturnValue(
            "\"\\\"Hello, world!!\\\"\""
            , "\"Hello, world!!\""
        )
    );
}

TEST(
    ConfigElementTest
    , NewFromString_EscapeYenSign
)
{
    ASSERT_NO_FATAL_FAILURE(
        testNewFromString_ReturnValue(
            "\"\\\\1,234,567\""
            , "\\1,234,567"
        )
    );
}

void testNewFromList(
    const std::string & _CONFIG
)
{
    auto    configElementUnique = adhocrepeater::unique(
        adhocrepeater::newConfigElement(
            _CONFIG
        )
    );
    ASSERT_NE( nullptr, configElementUnique.get() );
}

TEST(
    ConfigElementTest
    , NewFromList_Empty
)
{
    ASSERT_NO_FATAL_FAILURE( testNewFromList( "[]" ) );
    ASSERT_NO_FATAL_FAILURE( testNewFromList( " [ ] " ) );
}

TEST(
    ConfigElementTest
    , NewFromList_Failed
)
{
    ASSERT_NO_FATAL_FAILURE( testNew_Failed( "[" ) );
}

TEST(
    ConfigElementTest
    , NewFromList_AfterString_Failed
)
{
    ASSERT_NO_FATAL_FAILURE( testNew_Failed( "[] ABCDE" ) );
}

TEST(
    ConfigElementTest
    , NewFromList_AElement
)
{
    ASSERT_NO_FATAL_FAILURE( testNewFromList( "[ \"ABCDE\" ]" ) );
}

TEST(
    ConfigElementTest
    , NewFromList_ReturnValue_Empty
)
{
    auto    configElementUnique = adhocrepeater::unique(
        adhocrepeater::newConfigElement(
            "[]"
        )
    );
    ASSERT_NE( nullptr, configElementUnique.get() );

    const auto &    CONFIG_ELEMENT = *configElementUnique;
    const auto      LIST_PTR = CONFIG_ELEMENT.listUnique.get();
    ASSERT_NE( nullptr, LIST_PTR );
    const auto &    LIST = adhocrepeater::getList( *LIST_PTR );
    ASSERT_EQ( 0, LIST.size() );
}

void testNewFromList_ReturnValue_AElement(
    const std::string & _CONFIG
    , const char *      _STRING
)
{
    auto    configElementUnique = adhocrepeater::unique(
        adhocrepeater::newConfigElement(
            _CONFIG
        )
    );
    ASSERT_NE( nullptr, configElementUnique.get() );

    const auto &    CONFIG_ELEMENT = *configElementUnique;
    const auto      LIST_PTR = CONFIG_ELEMENT.listUnique.get();
    ASSERT_NE( nullptr, LIST_PTR );
    const auto &    LIST = adhocrepeater::getList( *LIST_PTR );
    ASSERT_EQ( 1, LIST.size() );

    const auto &    CONFIG_ELEMENT_UNIQUE = LIST.at( 0 );
    ASSERT_NE( nullptr, CONFIG_ELEMENT_UNIQUE.get() );

    const auto  STRING_PTR = CONFIG_ELEMENT_UNIQUE->stringUnique.get();
    ASSERT_NE( nullptr, STRING_PTR );
    const auto &    STRING = adhocrepeater::getString( *STRING_PTR );
    ASSERT_STREQ( _STRING, STRING.c_str() );
}

TEST(
    ConfigElementTest
    , NewFromList_ReturnValue_AElement
)
{
    ASSERT_NO_FATAL_FAILURE(
        testNewFromList_ReturnValue_AElement(
            "[ \"\" ]"
            , ""
        )
    );
    ASSERT_NO_FATAL_FAILURE(
        testNewFromList_ReturnValue_AElement(
            "[ \"Hello, world!!\" ]"
            , "Hello, world!!"
        )
    );
}

void testNewFromList_ReturnValue_Elements(
    const std::string &                     _CONFIG
    , const std::vector< const char * > &   _STRINGS
)
{
    auto    configElementUnique = adhocrepeater::unique(
        adhocrepeater::newConfigElement(
            _CONFIG
        )
    );
    ASSERT_NE( nullptr, configElementUnique.get() );

    const auto &    CONFIG_ELEMENT = *configElementUnique;
    const auto      LIST_PTR = CONFIG_ELEMENT.listUnique.get();
    ASSERT_NE( nullptr, LIST_PTR );
    const auto &    LIST = adhocrepeater::getList( *LIST_PTR );
    ASSERT_EQ( _STRINGS.size(), LIST.size() );

    auto    index = 0;
    for( const auto & STRING_ELEMENT : _STRINGS ) {
        const auto &    CONFIG_ELEMENT_UNIQUE = LIST.at( index );
        ASSERT_NE( nullptr, CONFIG_ELEMENT_UNIQUE.get() );

        const auto  STRING_PTR = CONFIG_ELEMENT_UNIQUE->stringUnique.get();
        ASSERT_NE( nullptr, STRING_PTR );
        const auto &    STRING = adhocrepeater::getString( *STRING_PTR );
        ASSERT_STREQ( STRING_ELEMENT, STRING.c_str() );

        index++;
    }
}

TEST(
    ConfigElementTest
    , NewFromList_ReturnValue_Elements
)
{
    ASSERT_NO_FATAL_FAILURE(
        testNewFromList_ReturnValue_Elements(
            "[ \"Hello, world!!\", \"ABCDE\" ]"
            , {
                "Hello, world!!",
                "ABCDE",
            }
        )
    );
    ASSERT_NO_FATAL_FAILURE(
        testNewFromList_ReturnValue_Elements(
            "[ \"Hello, world!!\", \"ABCDE\", \"\" ]"
            , {
                "Hello, world!!",
                "ABCDE",
                "",
            }
        )
    );
}

TEST(
    ConfigElementTest
    , NewFromList_ReturnValue_Elements_AfterSeparator
)
{
    ASSERT_NO_FATAL_FAILURE(
        testNewFromList_ReturnValue_Elements(
            "[ \"Hello, world!!\", \"ABCDE\", ]"
            , {
                "Hello, world!!",
                "ABCDE",
            }
        )
    );
}

void testNewFromMap(
    const std::string & _CONFIG
)
{
    auto    configElementUnique = adhocrepeater::unique(
        adhocrepeater::newConfigElement(
            _CONFIG
        )
    );
    ASSERT_NE( nullptr, configElementUnique.get() );
}

TEST(
    ConfigElementTest
    , NewFromMap_Empty
)
{
    ASSERT_NO_FATAL_FAILURE( testNewFromMap( "{}" ) );
    ASSERT_NO_FATAL_FAILURE( testNewFromMap( " { } " ) );
}

TEST(
    ConfigElementTest
    , NewFromMap_Failed
)
{
    ASSERT_NO_FATAL_FAILURE( testNew_Failed( "{" ) );
}

void testNewFromMap_AElement(
    const char *    _CONFIG
    , const char *  _KEY
    , const char *  _STRING
)
{
    auto    configElementUnique = adhocrepeater::unique(
        adhocrepeater::newConfigElement(
            _CONFIG
        )
    );
    ASSERT_NE( nullptr, configElementUnique.get() );

    const auto &    CONFIG_ELEMENT = *configElementUnique;
    const auto      MAP_PTR = CONFIG_ELEMENT.mapUnique.get();
    ASSERT_NE( nullptr, MAP_PTR );
    const auto &    MAP = adhocrepeater::getMap( *MAP_PTR );
    ASSERT_EQ( 1, MAP.size() );

    const auto  END = MAP.end();

    const auto  IT = MAP.find( _KEY );
    ASSERT_NE( END, IT );

    const auto &    CONFIG_ELEMENT_UNIQUE = IT->second;
    ASSERT_NE( nullptr, CONFIG_ELEMENT_UNIQUE.get() );

    const auto  STRING_PTR = CONFIG_ELEMENT_UNIQUE->stringUnique.get();
    ASSERT_NE( nullptr, STRING_PTR );
    const auto &    STRING = adhocrepeater::getString( *STRING_PTR );
    ASSERT_STREQ( _STRING, STRING.c_str() );
}

TEST(
    ConfigElementTest
    , NewFromMap_AElement
)
{
    ASSERT_NO_FATAL_FAILURE(
        testNewFromMap_AElement(
            "{ \"Hello, world!! key\" : \"Hello, world!!\" }"
            , "Hello, world!! key"
            , "Hello, world!!"
        )
    );
    ASSERT_NO_FATAL_FAILURE(
        testNewFromMap_AElement(
            "{ \"ABCDE key\" : \"ABCDE\" }"
            , "ABCDE key"
            , "ABCDE"
        )
    );
}

void testNewFromMap_Elements(
    const char *                                        _CONFIG
    , const std::map< const char *, const char * > &    _STRING_MAP
)
{
    auto    configElementUnique = adhocrepeater::unique(
        adhocrepeater::newConfigElement(
            _CONFIG
        )
    );
    ASSERT_NE( nullptr, configElementUnique.get() );

    const auto &    CONFIG_ELEMENT = *configElementUnique;
    const auto      MAP_PTR = CONFIG_ELEMENT.mapUnique.get();
    ASSERT_NE( nullptr, MAP_PTR );
    const auto &    MAP = adhocrepeater::getMap( *MAP_PTR );
    ASSERT_EQ( _STRING_MAP.size(), MAP.size() );

    const auto  END = MAP.end();

    for( const auto & PAIR : _STRING_MAP ) {
        const auto &    KEY = PAIR.first;
        const auto &    STRING_ELEMENT = PAIR.second;

        const auto  IT = MAP.find( KEY );
        ASSERT_NE( END, IT );

        const auto &    CONFIG_ELEMENT_UNIQUE = IT->second;
        ASSERT_NE( nullptr, CONFIG_ELEMENT_UNIQUE.get() );

        const auto  STRING_PTR = CONFIG_ELEMENT_UNIQUE->stringUnique.get();
        ASSERT_NE( nullptr, STRING_PTR );
        const auto &    STRING = adhocrepeater::getString( *STRING_PTR );
        ASSERT_STREQ( STRING_ELEMENT, STRING.c_str() );
    }
}

TEST(
    ConfigElementTest
    , NewFromMap_Elements
)
{
    ASSERT_NO_FATAL_FAILURE(
        testNewFromMap_Elements(
            "{ \"Hello, world!! key\" : \"Hello, world!!\", \"ABCDE key\" : \"ABCDE\" }"
            , {
                { "Hello, world!! key", "Hello, world!!" },
                { "ABCDE key", "ABCDE" },
            }
        )
    );
    ASSERT_NO_FATAL_FAILURE(
        testNewFromMap_Elements(
            "{ \"Hello, world!! key\" : \"Hello, world!!\", \"ABCDE key\" : \"ABCDE\", \"key\" : \"\" }"
            , {
                { "Hello, world!! key", "Hello, world!!" },
                { "ABCDE key", "ABCDE" },
                { "key", "" },
            }
        )
    );
}

TEST(
    ConfigElementTest
    , NewFromMap_Elements_AfterSeparator
)
{
    ASSERT_NO_FATAL_FAILURE(
        testNewFromMap_Elements(
            "{ \"Hello, world!! key\" : \"Hello, world!!\", \"ABCDE key\" : \"ABCDE\", }"
            , {
                { "Hello, world!! key", "Hello, world!!" },
                { "ABCDE key", "ABCDE" },
            }
        )
    );
}

TEST(
    ConfigElementTest
    , NewFromMap_Elements_Duplicate
)
{
    ASSERT_NO_FATAL_FAILURE(
        testNewFromMap_Elements(
            "{ \"Hello, world!! key\" : \"DUPLICATE\", \"ABCDE key\" : \"ABCDE\", \"Hello, world!! key\" : \"Hello, world!!\" }"
            , {
                { "Hello, world!! key", "Hello, world!!" },
                { "ABCDE key", "ABCDE" },
            }
        )
    );
}

void testGetString(
    const char *    _CONFIG
    , const char *  _STRING
)
{
    auto    configElementUnique = adhocrepeater::unique(
        adhocrepeater::newConfigElement(
            _CONFIG
        )
    );
    ASSERT_NE( nullptr, configElementUnique.get() );

    const auto &    CONFIG_ELEMENT = *configElementUnique;

    const auto  STRING_PTR = adhocrepeater::getString( CONFIG_ELEMENT );
    ASSERT_NE( nullptr, STRING_PTR );
    const auto &    STRING = adhocrepeater::getString( *STRING_PTR );
    ASSERT_STREQ( _STRING, STRING.c_str() );
}

TEST(
    ConfigElementTest
    , GetString
)
{
    ASSERT_NO_FATAL_FAILURE(
        testGetString(
            "\"Hello, world!!\""
            , "Hello, world!!"
        )
    );
    ASSERT_NO_FATAL_FAILURE(
        testGetString(
            "\"ABCDE\""
            , "ABCDE"
        )
    );
}

void testGetString_Failed(
    const char *    _CONFIG
)
{
    auto    configElementUnique = adhocrepeater::unique(
        adhocrepeater::newConfigElement(
            _CONFIG
        )
    );
    ASSERT_NE( nullptr, configElementUnique.get() );

    const auto &    CONFIG_ELEMENT = *configElementUnique;

    const auto  STRING_PTR = adhocrepeater::getString( CONFIG_ELEMENT );
    ASSERT_EQ( nullptr, STRING_PTR );
}

TEST(
    ConfigElementTest
    , GetString_Failed
)
{
    ASSERT_NO_FATAL_FAILURE(
        testGetString_Failed(
            "[]"
        )
    );
    ASSERT_NO_FATAL_FAILURE(
        testGetString_Failed(
            "{}"
        )
    );
}

void testGetList(
    const std::string &                     _CONFIG
    , const std::vector< const char * > &   _STRINGS
)
{
    auto    configElementUnique = adhocrepeater::unique(
        adhocrepeater::newConfigElement(
            _CONFIG
        )
    );
    ASSERT_NE( nullptr, configElementUnique.get() );

    const auto &    CONFIG_ELEMENT = *configElementUnique;

    const auto  LIST_PTR = adhocrepeater::getList( CONFIG_ELEMENT );
    ASSERT_NE( nullptr, LIST_PTR );
    const auto &    LIST = adhocrepeater::getList( *LIST_PTR );
    ASSERT_EQ( _STRINGS.size(), LIST.size() );

    auto    index = 0;
    for( const auto & STRING_ELEMENT : _STRINGS ) {
        const auto &    CONFIG_ELEMENT_UNIQUE = LIST.at( index );
        ASSERT_NE( nullptr, CONFIG_ELEMENT_UNIQUE.get() );

        const auto  STRING_PTR = CONFIG_ELEMENT_UNIQUE->stringUnique.get();
        ASSERT_NE( nullptr, STRING_PTR );
        const auto &    STRING = adhocrepeater::getString( *STRING_PTR );
        ASSERT_STREQ( STRING_ELEMENT, STRING.c_str() );

        index++;
    }
}

TEST(
    ConfigElementTest
    , GetList
)
{
    ASSERT_NO_FATAL_FAILURE(
        testGetList(
            "[ \"Hello, world!!\", \"ABCDE\" ]"
            , {
                "Hello, world!!",
                "ABCDE",
            }
        )
    );
    ASSERT_NO_FATAL_FAILURE(
        testGetList(
            "[ \"Hello, world!!\", \"ABCDE\", \"\" ]"
            , {
                "Hello, world!!",
                "ABCDE",
                "",
            }
        )
    );
}

void testGetList_Failed(
    const char *    _CONFIG
)
{
    auto    configElementUnique = adhocrepeater::unique(
        adhocrepeater::newConfigElement(
            _CONFIG
        )
    );
    ASSERT_NE( nullptr, configElementUnique.get() );

    const auto &    CONFIG_ELEMENT = *configElementUnique;

    const auto  LIST_PTR = adhocrepeater::getList( CONFIG_ELEMENT );
    ASSERT_EQ( nullptr, LIST_PTR );
}

TEST(
    ConfigElementTest
    , GetList_Failed
)
{
    ASSERT_NO_FATAL_FAILURE(
        testGetList_Failed(
            "\"\""
        )
    );
    ASSERT_NO_FATAL_FAILURE(
        testGetList_Failed(
            "{}"
        )
    );
}

void testGetMap(
    const char *                                        _CONFIG
    , const std::map< const char *, const char * > &    _STRING_MAP
)
{
    auto    configElementUnique = adhocrepeater::unique(
        adhocrepeater::newConfigElement(
            _CONFIG
        )
    );
    ASSERT_NE( nullptr, configElementUnique.get() );

    const auto &    CONFIG_ELEMENT = *configElementUnique;

    const auto  MAP_PTR = adhocrepeater::getMap( CONFIG_ELEMENT );
    ASSERT_NE( nullptr, MAP_PTR );
    const auto &    MAP = adhocrepeater::getMap( *MAP_PTR );
    ASSERT_EQ( _STRING_MAP.size(), MAP.size() );

    const auto  END = MAP.end();

    for( const auto & PAIR : _STRING_MAP ) {
        const auto &    KEY = PAIR.first;
        const auto &    STRING_ELEMENT = PAIR.second;

        const auto  IT = MAP.find( KEY );
        ASSERT_NE( END, IT );

        const auto &    CONFIG_ELEMENT_UNIQUE = IT->second;
        ASSERT_NE( nullptr, CONFIG_ELEMENT_UNIQUE.get() );

        const auto  STRING_PTR = CONFIG_ELEMENT_UNIQUE->stringUnique.get();
        ASSERT_NE( nullptr, STRING_PTR );
        const auto &    STRING = adhocrepeater::getString( *STRING_PTR );
        ASSERT_STREQ( STRING_ELEMENT, STRING.c_str() );
    }
}

TEST(
    ConfigElementTest
    , GetMap
)
{
    ASSERT_NO_FATAL_FAILURE(
        testGetMap(
            "{ \"Hello, world!! key\" : \"Hello, world!!\", \"ABCDE key\" : \"ABCDE\" }"
            , {
                { "Hello, world!! key", "Hello, world!!" },
                { "ABCDE key", "ABCDE" },
            }
        )
    );
    ASSERT_NO_FATAL_FAILURE(
        testGetMap(
            "{ \"Hello, world!! key\" : \"Hello, world!!\", \"ABCDE key\" : \"ABCDE\", \"key\" : \"\" }"
            , {
                { "Hello, world!! key", "Hello, world!!" },
                { "ABCDE key", "ABCDE" },
                { "key", "" },
            }
        )
    );
}

void testGetMap_Failed(
    const char *    _CONFIG
)
{
    auto    configElementUnique = adhocrepeater::unique(
        adhocrepeater::newConfigElement(
            _CONFIG
        )
    );
    ASSERT_NE( nullptr, configElementUnique.get() );

    const auto &    CONFIG_ELEMENT = *configElementUnique;

    const auto  MAP_PTR = adhocrepeater::getMap( CONFIG_ELEMENT );
    ASSERT_EQ( nullptr, MAP_PTR );
}

TEST(
    ConfigElementTest
    , GetMap_Failed
)
{
    ASSERT_NO_FATAL_FAILURE(
        testGetMap_Failed(
            "\"\""
        )
    );
    ASSERT_NO_FATAL_FAILURE(
        testGetMap_Failed(
            "[]"
        )
    );
}
