﻿#include "adhocrepeater/config/element/map.h"
#include "adhocrepeater/config/element/element.h"
#include "adhocrepeater/config/element/string.h"
#include "adhocrepeater/common/unique.h"

#include <utility>
#include <new>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    const auto  BEGIN_MAP_ELEMENT = '{';
    const auto  BEGIN_MAP_ELEMENT_LENGTH = 1;
    const auto  END_MAP_ELEMENT = '}';
    const auto  END_MAP_ELEMENT_LENGTH = 1;

    const auto  MAP_ELEMENT_SEPARATOR = ':';
    const auto  MAP_ELEMENT_SEPARATOR_LENGTH = 1;

    const auto  SEPARATOR = ',';
    const auto  SEPARATOR_LENGTH = 1;

    const char * getMapElements(
        const char *    _ELEMENT
        , const char *  _END
    )
    {
        const auto  MAP_ELEMENTS = _ELEMENT + BEGIN_MAP_ELEMENT_LENGTH;

        if( MAP_ELEMENTS > _END ) {
#ifdef  DEBUG
            std::printf( "D:マップ要素の始端文字が出現する前に文字列の最後に達している\n" );
#endif  // DEBUG

            return nullptr;
        }

        if( _ELEMENT[ 0 ] != BEGIN_MAP_ELEMENT ) {
#ifdef  DEBUG
            std::printf(
                "D:%cで始まっていないためマップ要素ではない\n"
                , BEGIN_MAP_ELEMENT
            );
#endif  // DEBUG

            return nullptr;
        }

        return MAP_ELEMENTS;
    }

    const char * getEndMap(
        const char *    _MAP_ELEMENT
        , const char *  _END
    )
    {
        const auto  END_MAP = _MAP_ELEMENT;

        if( END_MAP > _END ) {
#ifdef  DEBUG
            std::printf( "D:マップ要素の始端文字が出現する前に文字列の最後に達している\n" );
#endif  // DEBUG

            return nullptr;
        }

        if( _MAP_ELEMENT[ 0 ] != END_MAP_ELEMENT ) {
#ifdef  DEBUG
            std::printf( "D:マップ要素の終端文字ではない\n" );
#endif  // DEBUG

            return nullptr;
        }

        return END_MAP;
    }

    const char * getEndSeparator(
        const char *    _MAP_ELEMENT
        , const char *  _END
    )
    {
        const auto  END_SEPARATOR = _MAP_ELEMENT + SEPARATOR_LENGTH;

        if( _MAP_ELEMENT[ 0 ] != SEPARATOR ) {
            return nullptr;
        }

        return END_SEPARATOR;
    }

    const char * getEndMapElementSeparator(
        const char *    _MAP_ELEMENT
        , const char *  _END
    )
    {
        const auto  END_SEPARATOR = _MAP_ELEMENT + MAP_ELEMENT_SEPARATOR_LENGTH;

        if( _MAP_ELEMENT[ 0 ] != MAP_ELEMENT_SEPARATOR ) {
            return nullptr;
        }

        return END_SEPARATOR;
    }

    bool insert(
        adhocrepeater::ConfigElementMap::Map &                      _map
        , std::string &&                                            _key
        , adhocrepeater::Unique< adhocrepeater::ConfigElement > &&  _valueUnique
    )
    {
        const auto  IT = _map.find( _key );
        if( IT != _map.end() ) {
#ifdef  DEBUG
            std::printf( "D:マップの要素に重複があるので追加済みの要素を削除\n" );
#endif  // DEBUG

            _map.erase( IT );
        }

        const auto  RESULT = _map.insert(
            std::make_pair(
                std::move( _key )
                , std::move( _valueUnique )
            )
        );
        if( RESULT.second == false ) {
#ifdef  DEBUG
            std::printf( "E:マップへの要素追加に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    const char * initMap(
        adhocrepeater::ConfigElementMap::Map &   _map
        , const char *                          _MAP_ELEMENTS
        , const char *                          _END
    )
    {
        auto    ptr = _MAP_ELEMENTS;

        auto    endMap = static_cast< const char * >( nullptr );

        while( 1 ) {
            auto    mapElement = adhocrepeater::getElement(
                ptr
                , _END
            );
            if( mapElement == nullptr ) {
#ifdef  DEBUG
                std::printf( "E:マップ要素取得に失敗\n" );
#endif  // DEBUG

                return nullptr;
            }

            endMap = getEndMap(
                mapElement
                , _END
            );
            if( endMap != nullptr ) {
                break;
            }

            if( ptr > _MAP_ELEMENTS ) {
                const auto  END_SEPARATOR = getEndSeparator(
                    mapElement
                    , _END
                );
                if( END_SEPARATOR == nullptr ) {
#ifdef  DEBUG
                    std::printf( "E:マップ区切り文字が存在しない\n" );
#endif  // DEBUG

                    return nullptr;
                }

                mapElement = adhocrepeater::getElement(
                    END_SEPARATOR
                    , _END
                );
                if( mapElement == nullptr ) {
#ifdef  DEBUG
                    std::printf( "E:マップ要素取得に失敗\n" );
#endif  // DEBUG

                    return nullptr;
                }

                endMap = getEndMap(
                    mapElement
                    , _END
                );
                if( endMap != nullptr ) {
                    break;
                }
            }

            auto    keyElementUnique = adhocrepeater::Unique< adhocrepeater::ConfigElementString >();

            const auto  END_MAP_ELEMENT_KEY = adhocrepeater::init(
                keyElementUnique
                , mapElement
                , _END
            );
            if( END_MAP_ELEMENT_KEY == nullptr ) {
#ifdef  DEBUG
                std::printf( "E:マップ要素のキー生成に失敗\n" );
#endif  // DEBUG

                return nullptr;
            }
            auto &  keyElement = *keyElementUnique;

            auto &  key = adhocrepeater::getString( keyElement );

            const auto  MAP_ELEMENT_SEPARATOR = adhocrepeater::getElement(
                END_MAP_ELEMENT_KEY
                , _END
            );
            if( MAP_ELEMENT_SEPARATOR == nullptr ) {
#ifdef  DEBUG
                std::printf( "E:マップ要素の区切り文字取得に失敗\n" );
#endif  // DEBUG

                return nullptr;
            }

            const auto  END_MAP_ELEMENT_SEPARATOR = getEndMapElementSeparator(
                MAP_ELEMENT_SEPARATOR
                , _END
            );
            if( END_MAP_ELEMENT_SEPARATOR == nullptr ) {
#ifdef  DEBUG
                std::printf( "E:マップ要素の区切り文字の終端取得に失敗\n" );
#endif  // DEBUG

                return nullptr;
            }

            const auto  MAP_ELEMENT_VALUE = adhocrepeater::getElement(
                END_MAP_ELEMENT_SEPARATOR
                , _END
            );
            if( MAP_ELEMENT_VALUE == nullptr ) {
#ifdef  DEBUG
                std::printf( "E:マップ要素の値取得に失敗\n" );
#endif  // DEBUG

                return nullptr;
            }

            auto    valueUnique = adhocrepeater::Unique< adhocrepeater::ConfigElement >();

            const auto  END_MAP_ELEMENT_VALUE = adhocrepeater::init(
                valueUnique
                , MAP_ELEMENT_VALUE
                , _END
            );
            if( END_MAP_ELEMENT_VALUE == nullptr ) {
#ifdef  DEBUG
                std::printf( "E:マップ要素の値生成に失敗\n" );
#endif  // DEBUG

                return nullptr;
            }

            if( insert(
                _map
                , std::move( key )
                , std::move( valueUnique )
            ) == false ) {
#ifdef  DEBUG
                std::printf( "E:マップへの要素追加に失敗\n" );
#endif  // DEBUG

                return nullptr;
            }

            ptr = END_MAP_ELEMENT_VALUE;
        }

        return endMap;
    }

    const char * getEndElement(
        const char *    _END_MAP
    )
    {
        return _END_MAP + END_MAP_ELEMENT_LENGTH;
    }
}

namespace adhocrepeater {
    void free(
        ConfigElementMap &  _this
    )
    {
        delete &_this;
    }

    const char * init(
        Unique< ConfigElementMap > &    _mapUnique
        , const char *                  _ELEMENT
        , const char *                  _END
    )
    {
        const auto  MAP_ELEMENTS = getMapElements(
            _ELEMENT
            , _END
        );
        if( MAP_ELEMENTS == nullptr ) {
#ifdef  DEBUG
            std::printf( "D:マップ要素開始位置取得に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    map = ConfigElementMap::Map();

        const auto  END_MAP = initMap(
            map
            , MAP_ELEMENTS
            , _END
        );
        if( END_MAP == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:マップの要素取得に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        const auto  END_ELEMENT = getEndElement( END_MAP );
        if( END_ELEMENT == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:要素終了位置取得に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    mapUnique = unique(
            new( std::nothrow )ConfigElementMap{
                std::move( map ),
            }
        );
        if( mapUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:マップ生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        _mapUnique = std::move( mapUnique );

        return END_ELEMENT;
    }

    const ConfigElementString * getString(
        const ConfigElementMap &    _THIS
        , const char *              _KEY
    )
    {
        const auto &    MAP = getMap( _THIS );

        const auto  IT = MAP.find( _KEY );
        if( IT == MAP.end() ) {
#ifdef  DEBUG
            std::printf(
                "E:マップに%s要素が存在しない\n"
                , _KEY
            );
#endif  // DEBUG

            return nullptr;
        }
        const auto &    CONFIG_ELEMENT = *( IT->second );

        const auto  STRING_PTR = getString( CONFIG_ELEMENT );
        if( STRING_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf(
                "E:マップの%s要素が文字列要素ではない\n"
                , _KEY
            );
#endif  // DEBUG

            return nullptr;
        }

        return STRING_PTR;
    }

    const ConfigElementMap::Map & getMap(
        const ConfigElementMap &    _THIS
    )
    {
        return _THIS.data;
    }

    ConfigElementMap::Map & getMap(
        ConfigElementMap &  _this
    )
    {
        return _this.data;
    }
}
