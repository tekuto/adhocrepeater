﻿#include "adhocrepeater/config/element/list.h"
#include "adhocrepeater/config/element/element.h"
#include "adhocrepeater/common/unique.h"

#include <new>
#include <utility>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    const auto  BEGIN_LIST_ELEMENT = '[';
    const auto  BEGIN_LIST_ELEMENT_LENGTH = 1;
    const auto  END_LIST_ELEMENT = ']';
    const auto  END_LIST_ELEMENT_LENGTH = 1;

    const auto  SEPARATOR = ',';
    const auto  SEPARATOR_LENGTH = 1;

    const char * getListElements(
        const char *    _ELEMENT
        , const char *  _END
    )
    {
        const auto  LIST_ELEMENTS = _ELEMENT + BEGIN_LIST_ELEMENT_LENGTH;

        if( LIST_ELEMENTS > _END ) {
#ifdef  DEBUG
            std::printf( "D:リスト要素の始端文字が出現する前に文字列の最後に達している\n" );
#endif  // DEBUG

            return nullptr;
        }

        if( _ELEMENT[ 0 ] != BEGIN_LIST_ELEMENT ) {
#ifdef  DEBUG
            std::printf(
                "D:%cで始まっていないためリスト要素ではない\n"
                , BEGIN_LIST_ELEMENT
            );
#endif  // DEBUG

            return nullptr;
        }

        return LIST_ELEMENTS;
    }

    const char * getEndList(
        const char *    _LIST_ELEMENT
        , const char *  _END
    )
    {
        const auto  END_LIST = _LIST_ELEMENT;

        if( END_LIST > _END ) {
#ifdef  DEBUG
            std::printf( "D:リスト要素の始端文字が出現する前に文字列の最後に達している\n" );
#endif  // DEBUG

            return nullptr;
        }

        if( _LIST_ELEMENT[ 0 ] != END_LIST_ELEMENT ) {
#ifdef  DEBUG
            std::printf( "D:リスト要素の終端文字ではない\n" );
#endif  // DEBUG

            return nullptr;
        }

        return END_LIST;
    }

    const char * getEndSeparator(
        const char *    _LIST_ELEMENT
        , const char *  _END
    )
    {
        const auto  END_SEPARATOR = _LIST_ELEMENT + SEPARATOR_LENGTH;

        if( _LIST_ELEMENT[ 0 ] != SEPARATOR ) {
            return nullptr;
        }

        return END_SEPARATOR;
    }

    const char * initList(
        adhocrepeater::ConfigElementList::List &    _list
        , const char *                              _LIST_ELEMENTS
        , const char *                              _END
    )
    {
        auto    ptr = _LIST_ELEMENTS;

        auto    endList = static_cast< const char * >( nullptr );

        while( 1 ) {
            auto    listElement = adhocrepeater::getElement(
                ptr
                , _END
            );
            if( listElement == nullptr ) {
#ifdef  DEBUG
                std::printf( "E:リスト要素取得に失敗\n" );
#endif  // DEBUG

                return nullptr;
            }

            endList = getEndList(
                listElement
                , _END
            );
            if( endList != nullptr ) {
                break;
            }

            if( ptr > _LIST_ELEMENTS ) {
                const auto  END_SEPARATOR = getEndSeparator(
                    listElement
                    , _END
                );
                if( END_SEPARATOR == nullptr ) {
#ifdef  DEBUG
                    std::printf( "E:リスト区切り文字が存在しない\n" );
#endif  // DEBUG

                    return nullptr;
                }

                listElement = adhocrepeater::getElement(
                    END_SEPARATOR
                    , _END
                );
                if( listElement == nullptr ) {
#ifdef  DEBUG
                    std::printf( "E:リスト要素取得に失敗\n" );
#endif  // DEBUG

                    return nullptr;
                }

                endList = getEndList(
                    listElement
                    , _END
                );
                if( endList != nullptr ) {
                    break;
                }
            }

            auto    elementUnique = adhocrepeater::Unique< adhocrepeater::ConfigElement >();

            const auto  END_ELEMENT = adhocrepeater::init(
                elementUnique
                , listElement
                , _END
            );
            if( END_ELEMENT == nullptr ) {
#ifdef  DEBUG
                std::printf( "E:リストの要素生成に失敗\n" );
#endif  // DEBUG

                return nullptr;
            }

            _list.push_back( std::move( elementUnique ) );

            ptr = END_ELEMENT;
        }

        return endList;
    }

    const char * getEndElement(
        const char *    _END_LIST
    )
    {
        return _END_LIST + END_LIST_ELEMENT_LENGTH;
    }
}

namespace adhocrepeater {
    void free(
        ConfigElementList & _this
    )
    {
        delete &_this;
    }

    const char * init(
        Unique< ConfigElementList > &   _listUnique
        , const char *                  _ELEMENT
        , const char *                  _END
    )
    {
        const auto  LIST_ELEMENTS = getListElements(
            _ELEMENT
            , _END
        );
        if( LIST_ELEMENTS == nullptr ) {
#ifdef  DEBUG
            std::printf( "D:リスト要素開始位置取得に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    list = ConfigElementList::List();

        const auto  END_LIST = initList(
            list
            , LIST_ELEMENTS
            , _END
        );
        if( END_LIST == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:リストの要素取得に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        const auto  END_ELEMENT = getEndElement( END_LIST );
        if( END_ELEMENT == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:要素終了位置取得に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    listUnique = unique(
            new( std::nothrow )ConfigElementList{
                std::move( list ),
            }
        );
        if( listUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:リスト生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        _listUnique = std::move( listUnique );

        return END_ELEMENT;
    }

    const ConfigElementList::List & getList(
        const ConfigElementList &   _THIS
    )
    {
        return _THIS.data;
    }

    ConfigElementList::List & getList(
        ConfigElementList & _this
    )
    {
        return _this.data;
    }
}
