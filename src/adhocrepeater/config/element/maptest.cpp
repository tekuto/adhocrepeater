﻿#include "adhocrepeater/util/test.h"
#include "adhocrepeater/config/element/map.h"
#include "adhocrepeater/config/element/element.h"
#include "adhocrepeater/config/element/string.h"
#include "adhocrepeater/common/unique.h"

#include <map>

void testGetString(
    const char *    _CONFIG
    , const char *  _KEY
    , const char *  _VALUE
)
{
    auto    configElementUnique = adhocrepeater::unique(
        adhocrepeater::newConfigElement(
            _CONFIG
        )
    );
    ASSERT_NE( nullptr, configElementUnique.get() );

    const auto &    CONFIG_ELEMENT = *configElementUnique;
    const auto  MAP_PTR = adhocrepeater::getMap( CONFIG_ELEMENT );
    ASSERT_NE( nullptr, MAP_PTR );
    const auto &    MAP = *MAP_PTR;

    const auto  STRING_PTR = adhocrepeater::getString(
        MAP
        , _KEY
    );
    ASSERT_NE( nullptr, STRING_PTR );
    const auto &    STRING = *STRING_PTR;

    ASSERT_STREQ( _VALUE, adhocrepeater::getString( STRING ).c_str() );
}

TEST(
    ConfigElementTest
    , GetString
)
{
    ASSERT_NO_FATAL_FAILURE(
        testGetString(
            "{ \"key\" : \"string\" }"
            , "key"
            , "string"
        )
    );
    ASSERT_NO_FATAL_FAILURE(
        testGetString(
            "{ \"key2\" : \"string2\" }"
            , "key2"
            , "string2"
        )
    );
}
