﻿#include "adhocrepeater/config/config.h"
#include "adhocrepeater/config/element/element.h"
#include "adhocrepeater/config/element/map.h"
#include "adhocrepeater/config/element/string.h"
#include "adhocrepeater/packet/macaddress.h"
#include "adhocrepeater/common/file.h"
#include "adhocrepeater/common/time.h"
#include "adhocrepeater/common/unique.h"

#include <string>
#include <new>
#include <utility>
#include <cstdlib>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    const auto  USHORT_MAX = static_cast< unsigned short >( ~0 );

    const auto  KEY_INTERFACES = "interfaces";
    const auto  KEY_INTERFACES_SCANNER = "scanner";
    const auto  KEY_INTERFACES_REPEATER = "repeater";

    const auto  KEY_TARGET_SSID = "targetSsid";
    const auto  KEY_TARGET_SSID_PREFIX = "prefix";
    const auto  KEY_TARGET_SSID_PREFIX_SIZE = "prefixSize";
    const auto  KEY_TARGET_SSID_CHANNEL = "channel";

    const auto  KEY_PORTS = "ports";
    const auto  KEY_PORTS_DATA = "data";
    const auto  KEY_PORTS_SSID = "ssid";

    const auto  KEY_PEER = "peer";
    const auto  KEY_PEER_ADDRESS = "address";
    const auto  KEY_PEER_DATA_PORT = "dataPort";
    const auto  KEY_PEER_SSID_PORT = "ssidPort";

    const auto  KEY_SCANNER = "scanner";
    const auto  KEY_SCANNER_SCAN_INTERVAL_USECONDS = "scanIntervalUSeconds";
    const auto  KEY_SCANNER_GET_SCAN_DATA_TIMEOUT_USECONDS = "getScanDataTimeoutUSeconds";
    const auto  KEY_SCANNER_GET_SCAN_DATA_INTERVAL_USECONDS = "getScanDataIntervalUSeconds";

    const auto  KEY_CHANGER = "changer";
    const auto  KEY_CHANGER_GET_SSID_WAIT_TIME_USECONDS = "getSsidWaitTimeUSeconds";
    const auto  KEY_CHANGER_SSID_HISTORY_SIZE = "ssidHistorySize";
    const auto  KEY_CHANGER_DISCONNECTABLE_TIME_USECONDS = "disconnectableTimeUSeconds";
    const auto  KEY_CHANGER_SSID_MANAGER_KEEP_TIME_USECONDS = "ssidManagerKeepTimeUSeconds";
    const auto  KEY_CHANGER_FIND_SSID_TIMEOUT_USECONDS = "findSsidTimeoutUSeconds";
    const auto  KEY_CHANGER_FIND_SSID_INTERVAL_USECONDS = "findSsidIntervalUSeconds";
    const auto  KEY_CHANGER_GET_SCAN_DATA_TIMEOUT_USECONDS = "getScanDataTimeoutUSeconds";
    const auto  KEY_CHANGER_GET_SCAN_DATA_INTERVAL_USECONDS = "getScanDataIntervalUSeconds";

    const auto  KEY_SENDER = "sender";
    const auto  KEY_SENDER_DEVICE_ADDRESS = "deviceAddress";
    const auto  KEY_SENDER_RECEIVE_RAW_TIMEOUT_USECONDS = "receiveRawTimeoutUSeconds";
    const auto  KEY_SENDER_POLLING_TIMEOUT_MSECONDS = "pollingTimeoutMSeconds";

    const auto  KEY_RECEIVER = "receiver";
    const auto  KEY_RECEIVER_RECEIVE_UDP_TIMEOUT_USECONDS = "receiveUdpTimeoutUSeconds";
    const auto  KEY_RECEIVER_POLLING_TIMEOUT_MSECONDS = "pollingTimeoutMSeconds";

    const adhocrepeater::ConfigElementMap * getMap(
        const adhocrepeater::ConfigElementMap::Map &    _MAP
        , const char *                                  _KEY
    )
    {
        const auto  IT = _MAP.find( _KEY );
        if( IT == _MAP.end() ) {
#ifdef  DEBUG
            std::printf(
                "E:設定ファイルに%s要素が存在しない\n"
                , _KEY
            );
#endif  // DEBUG

            return nullptr;
        }
        const auto &    CONFIG_ELEMENT = *( IT->second );

        const auto  MAP_PTR = adhocrepeater::getMap( CONFIG_ELEMENT );
        if( MAP_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf(
                "E:%s要素がマップではない\n"
                , _KEY
            );
#endif  // DEBUG

            return nullptr;
        }

        return MAP_PTR;
    }

    bool initString(
        std::string &                                   _string
        , const adhocrepeater::ConfigElementMap::Map &  _MAP
        , const char *                                  _PARENT_KEY
        , const char *                                  _KEY
    )
    {
        const auto  IT = _MAP.find( _KEY );
        if( IT == _MAP.end() ) {
#ifdef  DEBUG
            std::printf(
                "E:設定ファイルに%s.%s要素が存在しない\n"
                , _PARENT_KEY
                , _KEY
            );
#endif  // DEBUG

            return false;
        }
        const auto &    CONFIG_ELEMENT = *( IT->second );

        const auto  STRING_PTR = adhocrepeater::getString( CONFIG_ELEMENT );
        if( STRING_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf(
                "E:%s.%s要素が文字列ではない\n"
                , _PARENT_KEY
                , _KEY
            );
#endif  // DEBUG

            return false;
        }
        const auto  STRING = adhocrepeater::getString( *STRING_PTR );

        _string = STRING;

        return true;
    }

    bool stringToInt(
        int &                   _i
        , const std::string &   _STRING
    )
    {
        auto    endPtr = static_cast< char * >( nullptr );
        auto    i = std::strtol(
            _STRING.c_str()
            , &endPtr
            , 10
        );
        if( endPtr == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:文字列の数値変換に失敗\n" );
#endif  // DEBUG

            return false;
        }
        if( *endPtr != '\0' ) {
#ifdef  DEBUG
            std::printf( "E:文字列に数値以外の文字が含まれている\n" );
#endif  // DEBUG

            return false;
        }

        _i = std::move( i );

        return true;
    }

    bool initInteger(
        int &                                           _i
        , const adhocrepeater::ConfigElementMap::Map &  _MAP
        , const char *                                  _PARENT_KEY
        , const char *                                  _KEY
    )
    {
        auto    integerString = std::string();
        if( initString(
            integerString
            , _MAP
            , _PARENT_KEY
            , _KEY
        ) == false ) {
#ifdef  DEBUG
            std::printf(
                "E:%s.%s要素の初期化に失敗\n"
                , _PARENT_KEY
                , _KEY
            );
#endif  // DEBUG

            return false;
        }

        auto    i = int();
        if( stringToInt(
            i
            , integerString
        ) == false ) {
#ifdef  DEBUG
            std::printf(
                "E:%s.%s要素の数値化に失敗\n"
                , _PARENT_KEY
                , _KEY
            );
#endif  // DEBUG

            return false;
        }

        _i = std::move( i );

        return true;
    }

    bool initUShort(
        unsigned short &                                _uShort
        , const adhocrepeater::ConfigElementMap::Map &  _MAP
        , const char *                                  _PARENT_KEY
        , const char *                                  _KEY
    )
    {
        auto    i = int();
        if( initInteger(
            i
            , _MAP
            , _PARENT_KEY
            , _KEY
        ) == false ) {
#ifdef  DEBUG
            std::printf(
                "E:%s.%s要素の初期化に失敗\n"
                , _PARENT_KEY
                , _KEY
            );
#endif  // DEBUG

            return false;
        }

        if( i < 0 ) {
#ifdef  DEBUG
            std::printf(
                "E:%s.%s要素が0より小さい\n"
                , _PARENT_KEY
                , _KEY
            );
#endif  // DEBUG

            return false;
        }

        if( i > USHORT_MAX ) {
#ifdef  DEBUG
            std::printf(
                "E:%s.%s要素がunsigned shortの最大より大きい\n"
                , _PARENT_KEY
                , _KEY
            );
#endif  // DEBUG

            return false;
        }

        _uShort = static_cast< unsigned short >( i );

        return true;
    }

    bool initMicroSeconds(
        adhocrepeater::MicroSeconds &                   _microSeconds
        , const adhocrepeater::ConfigElementMap::Map &  _MAP
        , const char *                                  _PARENT_KEY
        , const char *                                  _KEY
    )
    {
        auto    microSecondsString = std::string();
        if( initString(
            microSecondsString
            , _MAP
            , _PARENT_KEY
            , _KEY
        ) == false ) {
#ifdef  DEBUG
            std::printf(
                "E:%s.%s要素の初期化に失敗\n"
                , _PARENT_KEY
                , _KEY
            );
#endif  // DEBUG

            return false;
        }

        auto    microSeconds = adhocrepeater::MicroSeconds();
        if( adhocrepeater::stringToMicroSeconds(
            microSeconds
            , microSecondsString
        ) == false ) {
#ifdef  DEBUG
            std::printf(
                "E:%s.%s要素のマイクロ秒変換に失敗\n"
                , _PARENT_KEY
                , _KEY
            );
#endif  // DEBUG

            return false;
        }

        _microSeconds = std::move( microSeconds );

        return true;
    }

    bool initMacAddress(
        adhocrepeater::MacAddress &                     _macAddress
        , const adhocrepeater::ConfigElementMap::Map &  _MAP
        , const char *                                  _PARENT_KEY
        , const char *                                  _KEY
    )
    {
        auto    macAddressString = std::string();
        if( initString(
            macAddressString
            , _MAP
            , _PARENT_KEY
            , _KEY
        ) == false ) {
#ifdef  DEBUG
            std::printf(
                "E:%s.%s要素の初期化に失敗\n"
                , _PARENT_KEY
                , _KEY
            );
#endif  // DEBUG

            return false;
        }

        auto    macAddress = adhocrepeater::MacAddress();
        if( adhocrepeater::stringToMacAddress(
            macAddress
            , macAddressString
        ) == false ) {
#ifdef  DEBUG
            std::printf(
                "E:%s.%s要素のMACアドレス変換に失敗\n"
                , _PARENT_KEY
                , _KEY
            );
#endif  // DEBUG

            return false;
        }

        _macAddress = std::move( macAddress );

        return true;
    }

    bool initInteraces(
        adhocrepeater::Config::Interfaces &             _interfaces
        , const adhocrepeater::ConfigElementMap::Map &  _MAP
    )
    {
        const auto  MAP_PTR = getMap(
            _MAP
            , KEY_INTERFACES
        );
        if( MAP_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf(
                "E:%sマップ要素の取得に失敗\n"
                , KEY_INTERFACES
            );
#endif  // DEBUG

            return false;
        }
        const auto &    MAP = adhocrepeater::getMap( *MAP_PTR );

        if( initString(
            _interfaces.scanner
            , MAP
            , KEY_INTERFACES
            , KEY_INTERFACES_SCANNER
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSIDスキャン用インターフェース名の初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( initString(
            _interfaces.repeater
            , MAP
            , KEY_INTERFACES
            , KEY_INTERFACES_REPEATER
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:データ中継用インターフェース名の初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool initTargetSsid(
        adhocrepeater::Config::TargetSsid &             _targetSsid
        , const adhocrepeater::ConfigElementMap::Map &  _MAP
    )
    {
        const auto  MAP_PTR = getMap(
            _MAP
            , KEY_TARGET_SSID
        );
        if( MAP_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf(
                "E:%sマップ要素の取得に失敗\n"
                , KEY_TARGET_SSID
            );
#endif  // DEBUG

            return false;
        }
        const auto &    MAP = adhocrepeater::getMap( *MAP_PTR );

        if( initString(
            _targetSsid.prefix
            , MAP
            , KEY_TARGET_SSID
            , KEY_TARGET_SSID_PREFIX
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:対象SSIDの接頭辞の初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( initInteger(
            _targetSsid.prefixSize
            , MAP
            , KEY_TARGET_SSID
            , KEY_TARGET_SSID_PREFIX_SIZE
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:対象SSIDの接頭辞サイズの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( initInteger(
            _targetSsid.channel
            , MAP
            , KEY_TARGET_SSID
            , KEY_TARGET_SSID_CHANNEL
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:対象SSIDのチャンネルの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool initPorts(
        adhocrepeater::Config::Ports &                  _ports
        , const adhocrepeater::ConfigElementMap::Map &  _MAP
    )
    {
        const auto  MAP_PTR = getMap(
            _MAP
            , KEY_PORTS
        );
        if( MAP_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf(
                "E:%sマップ要素の取得に失敗\n"
                , KEY_PORTS
            );
#endif  // DEBUG

            return false;
        }
        const auto &    MAP = adhocrepeater::getMap( *MAP_PTR );

        if( initUShort(
            _ports.data
            , MAP
            , KEY_PORTS
            , KEY_PORTS_DATA
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:データ受信ポートの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( initUShort(
            _ports.ssid
            , MAP
            , KEY_PORTS
            , KEY_PORTS_SSID
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSID受信ポートの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool initPeer(
        adhocrepeater::Config::Peer &                   _peer
        , const adhocrepeater::ConfigElementMap::Map &  _MAP
    )
    {
        const auto  MAP_PTR = getMap(
            _MAP
            , KEY_PEER
        );
        if( MAP_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf(
                "E:%sマップ要素の取得に失敗\n"
                , KEY_PEER
            );
#endif  // DEBUG

            return false;
        }
        const auto &    MAP = adhocrepeater::getMap( *MAP_PTR );

        if( initString(
            _peer.address
            , MAP
            , KEY_PEER
            , KEY_PEER_ADDRESS
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ピアのIPアドレスの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( initUShort(
            _peer.dataPort
            , MAP
            , KEY_PEER
            , KEY_PEER_DATA_PORT
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ピアのデータ受信ポートの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( initUShort(
            _peer.ssidPort
            , MAP
            , KEY_PEER
            , KEY_PEER_SSID_PORT
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ピアのSSID受信ポートの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool initScanner(
        adhocrepeater::Config::Scanner &                _scanner
        , const adhocrepeater::ConfigElementMap::Map &  _MAP
    )
    {
        const auto  MAP_PTR = getMap(
            _MAP
            , KEY_SCANNER
        );
        if( MAP_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf(
                "E:%sマップ要素の取得に失敗\n"
                , KEY_SCANNER
            );
#endif  // DEBUG

            return false;
        }
        const auto &    MAP = adhocrepeater::getMap( *MAP_PTR );

        if( initMicroSeconds(
            _scanner.scanInterval
            , MAP
            , KEY_SCANNER
            , KEY_SCANNER_SCAN_INTERVAL_USECONDS
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSIDスキャンのスキャンインターバルの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( initMicroSeconds(
            _scanner.getScanDataTimeout
            , MAP
            , KEY_SCANNER
            , KEY_SCANNER_GET_SCAN_DATA_TIMEOUT_USECONDS
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSIDスキャンのスキャンデータ取得タイムアウトの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( initMicroSeconds(
            _scanner.getScanDataInterval
            , MAP
            , KEY_SCANNER
            , KEY_SCANNER_GET_SCAN_DATA_INTERVAL_USECONDS
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSIDスキャンのスキャンデータ取得インターバルの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool initChanger(
        adhocrepeater::Config::Changer &                _changer
        , const adhocrepeater::ConfigElementMap::Map &  _MAP
    )
    {
        const auto  MAP_PTR = getMap(
            _MAP
            , KEY_CHANGER
        );
        if( MAP_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf(
                "E:%sマップ要素の取得に失敗\n"
                , KEY_CHANGER
            );
#endif  // DEBUG

            return false;
        }
        const auto &    MAP = adhocrepeater::getMap( *MAP_PTR );

        if( initMicroSeconds(
            _changer.getSsidWaitTime
            , MAP
            , KEY_CHANGER
            , KEY_CHANGER_GET_SSID_WAIT_TIME_USECONDS
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSIDチェンジャのSSID取得待機時間の初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( initInteger(
            _changer.ssidHistorySize
            , MAP
            , KEY_CHANGER
            , KEY_CHANGER_SSID_HISTORY_SIZE
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSIDチェンジャのSSID履歴サイズの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( initMicroSeconds(
            _changer.disconnectableTime
            , MAP
            , KEY_CHANGER
            , KEY_CHANGER_DISCONNECTABLE_TIME_USECONDS
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSIDチェンジャの切断可能時間の初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( initMicroSeconds(
            _changer.ssidManagerKeepTime
            , MAP
            , KEY_CHANGER
            , KEY_CHANGER_SSID_MANAGER_KEEP_TIME_USECONDS
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSIDチェンジャのSSIDマネージャSSID持続時間の初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( initMicroSeconds(
            _changer.findSsidTimeout
            , MAP
            , KEY_CHANGER
            , KEY_CHANGER_FIND_SSID_TIMEOUT_USECONDS
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSIDチェンジャのSSID検索タイムアウトの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( initMicroSeconds(
            _changer.findSsidInterval
            , MAP
            , KEY_CHANGER
            , KEY_CHANGER_FIND_SSID_INTERVAL_USECONDS
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSIDチェンジャのSSID検索インターバルの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( initMicroSeconds(
            _changer.getScanDataTimeout
            , MAP
            , KEY_CHANGER
            , KEY_CHANGER_GET_SCAN_DATA_TIMEOUT_USECONDS
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSIDチェンジャのスキャンデータ取得タイムアウトの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( initMicroSeconds(
            _changer.getScanDataInterval
            , MAP
            , KEY_CHANGER
            , KEY_CHANGER_GET_SCAN_DATA_INTERVAL_USECONDS
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSIDチェンジャのスキャンデータ取得インターバルの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool initSender(
        adhocrepeater::Config::Sender &                 _sender
        , const adhocrepeater::ConfigElementMap::Map &  _MAP
    )
    {
        const auto  MAP_PTR = getMap(
            _MAP
            , KEY_SENDER
        );
        if( MAP_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf(
                "E:%sマップ要素の取得に失敗\n"
                , KEY_SENDER
            );
#endif  // DEBUG

            return false;
        }
        const auto &    MAP = adhocrepeater::getMap( *MAP_PTR );

        if( initMacAddress(
            _sender.deviceAddress
            , MAP
            , KEY_SENDER
            , KEY_SENDER_DEVICE_ADDRESS
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:デバイスのMACアドレスの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( initMicroSeconds(
            _sender.receiveRawTimeout
            , MAP
            , KEY_SENDER
            , KEY_SENDER_RECEIVE_RAW_TIMEOUT_USECONDS
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:RAWソケットからのデータ受信タイムアウトの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( initInteger(
            _sender.pollingTimeoutMSeconds
            , MAP
            , KEY_SENDER
            , KEY_SENDER_POLLING_TIMEOUT_MSECONDS
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ポーリングのタイムアウトの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool initReceiver(
        adhocrepeater::Config::Receiver &               _receiver
        , const adhocrepeater::ConfigElementMap::Map &  _MAP
    )
    {
        const auto  MAP_PTR = getMap(
            _MAP
            , KEY_RECEIVER
        );
        if( MAP_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf(
                "E:%sマップ要素の取得に失敗\n"
                , KEY_RECEIVER
            );
#endif  // DEBUG

            return false;
        }
        const auto &    MAP = adhocrepeater::getMap( *MAP_PTR );

        if( initMicroSeconds(
            _receiver.receiveUdpTimeout
            , MAP
            , KEY_RECEIVER
            , KEY_RECEIVER_RECEIVE_UDP_TIMEOUT_USECONDS
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:UDPソケットからのデータ受信タイムアウトの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( initInteger(
            _receiver.pollingTimeoutMSeconds
            , MAP
            , KEY_RECEIVER
            , KEY_RECEIVER_POLLING_TIMEOUT_MSECONDS
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ポーリングのタイムアウトの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }
}

namespace adhocrepeater {
    Config * newConfig(
        const std::string & _FILE_PATH
    )
    {
        auto    configFile = std::string();
        if( readFile(
            configFile
            , _FILE_PATH
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:設定ファイルの読み込みに失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    configElementUnique = unique( newConfigElement( configFile ) );
        if( configElementUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:設定要素の生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }
        const auto &    CONFIG_ELEMENT = *configElementUnique;

        const auto  MAP_PTR = getMap( CONFIG_ELEMENT );
        if( MAP_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:設定ファイルのルート要素がマップではない\n" );
#endif  // DEBUG

            return nullptr;
        }
        const auto &    MAP = getMap( *MAP_PTR );

        auto    interfaces = Config::Interfaces();
        if( initInteraces(
            interfaces
            , MAP
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:インターフェース設定の初期化に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    targetSsid = Config::TargetSsid();
        if( initTargetSsid(
            targetSsid
            , MAP
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:対象SSIDの設定の初期化に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    ports = Config::Ports();
        if( initPorts(
            ports
            , MAP
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ポート設定の初期化に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    peer = Config::Peer();
        if( initPeer(
            peer
            , MAP
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ピア設定の初期化に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    scanner = Config::Scanner();
        if( initScanner(
            scanner
            , MAP
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSIDスキャナの設定の初期化に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    changer = Config::Changer();
        if( initChanger(
            changer
            , MAP
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSIDチェンジャの設定の初期化に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    sender = Config::Sender();
        if( initSender(
            sender
            , MAP
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:データ送信の設定の初期化に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    receiver = Config::Receiver();
        if( initReceiver(
            receiver
            , MAP
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:データ受信の設定の初期化に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    thisUnique = unique(
            new( std::nothrow )Config{
                std::move( interfaces ),
                std::move( targetSsid ),
                std::move( ports ),
                std::move( peer ),
                std::move( scanner ),
                std::move( changer ),
                std::move( sender ),
                std::move( receiver ),
            }
        );
        if( thisUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:設定の生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        return thisUnique.release();
    }

    void free(
        Config &    _this
    )
    {
        delete &_this;
    }
}
