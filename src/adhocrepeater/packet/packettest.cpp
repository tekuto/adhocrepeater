﻿#include "adhocrepeater/util/test.h"
#include "adhocrepeater/packet/packet.h"

#include <string>

TEST(
    PacketTest
    , SetDataFromString
)
{
    auto    packet = adhocrepeater::Packet();
    ASSERT_TRUE(
        adhocrepeater::setData(
            packet
            , "TESTDATA"
        )
    );

    const auto  SIZE = packet.size();
    ASSERT_EQ( 8, SIZE );

    const auto  DATA = std::string(
        packet.data()
        , SIZE
    );

    ASSERT_STREQ( "TESTDATA", DATA.c_str() );
}

TEST(
    PacketTest
    , SetDataFromPtr
)
{
    auto    packet = adhocrepeater::Packet();
    ASSERT_TRUE(
        adhocrepeater::setData(
            packet
            , "TESTDATA_ABCDE"
            , 8
        )
    );

    const auto  SIZE = packet.size();
    ASSERT_EQ( 8, SIZE );

    const auto  DATA = std::string(
        packet.data()
        , SIZE
    );

    ASSERT_STREQ( "TESTDATA", DATA.c_str() );
}
