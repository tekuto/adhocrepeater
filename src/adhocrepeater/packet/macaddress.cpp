﻿#include "adhocrepeater/packet/macaddress.h"

#include <string>
#include <utility>
#include <algorithm>
#include <cstdlib>
#include <cstring>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    const auto  OCTET_LENGTH = 2;

    bool strToLl(
        long long &             _ll
        , const std::string &   _STRING
    )
    {
        auto    endPtr = static_cast< char * >( nullptr );
        auto    ll = std::strtoll(
            _STRING.c_str()
            , &endPtr
            , 16
        );
        if( endPtr == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:MACアドレスの数値変換に失敗\n" );
#endif  // DEBUG

            return false;
        }
        if( *endPtr != '\0' ) {
#ifdef  DEBUG
            std::printf( "E:MACアドレスに16進数以外の文字が含まれている\n" );
#endif  // DEBUG

            return false;
        }

        _ll = std::move( ll );

        return true;
    }

    void llToMacAddress(
        adhocrepeater::MacAddress & _macAddress
        , long long                 _ll
    )
    {
        std::memcpy(
            _macAddress.data()
            , &_ll
            , adhocrepeater::MAC_ADDRESS_LENGTH
        );

        std::reverse(
            _macAddress.begin()
            , _macAddress.end()
        );
    }
}

namespace adhocrepeater {
    bool stringToMacAddress(
        MacAddress &            _macAddress
        , const std::string &   _STRING
    )
    {
        if( _STRING.size() != MAC_ADDRESS_LENGTH * OCTET_LENGTH ) {
#ifdef  DEBUG
            std::printf( "E:文字列の長さがMACアドレスの長さではない\n" );
#endif  // DEBUG

            return false;
        }

        long long   macAddressLl;
        if( strToLl(
            macAddressLl
            , _STRING
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:MACアドレスの数値変換に失敗\n" );
#endif  // DEBUG

            return false;
        }

        auto    macAddress = MacAddress();
        llToMacAddress(
            macAddress
            , macAddressLl
        );

        _macAddress = std::move( macAddress );

        return true;
    }
}
