﻿#include "adhocrepeater/packet/raw/sender.h"
#include "adhocrepeater/packet/socket.h"
#include "adhocrepeater/common/unique.h"

#include <new>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace adhocrepeater {
    RawSender * newRawSender(
        const std::string & _INTERFACE
    )
    {
        auto    thisUnique = unique( new( std::nothrow )Socket );
        if( thisUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:RAW送信の生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }
        auto &  sender = *thisUnique;

        auto &  socket = sender.socket;
        socket = socketRaw();
        if( socket == -1 ) {
#ifdef  DEBUG
            std::printf( "E:ソケット生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }
        sender.socketCloser.reset( &socket );

        if( bindRaw(
            sender
            , _INTERFACE
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:接続に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        return reinterpret_cast< RawSender * >( thisUnique.release() );
    }

    void free(
        RawSender & _this
    )
    {
        free( reinterpret_cast< Socket & >( _this ) );
    }

    bool send(
        RawSender &         _this
        , const Packet &    _PACKET
    )
    {
        return send(
            reinterpret_cast< Socket & >( _this )
            , _PACKET
        );
    }
}
