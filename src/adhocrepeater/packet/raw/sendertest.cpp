﻿#include "adhocrepeater/util/test.h"
#include "adhocrepeater/packet/raw/sender.h"
#include "adhocrepeater/packet/socket.h"
#include "adhocrepeater/packet/packet.h"
#include "adhocrepeater/common/unique.h"

TEST(
    RawSenderTest
    , New
)
{
    auto    rawSenderUnique = adhocrepeater::unique( adhocrepeater::newRawSender( "ra0" ) );
    ASSERT_NE( nullptr, rawSenderUnique.get() );

    const auto &    SOCKET = reinterpret_cast< adhocrepeater::Socket & >( *rawSenderUnique );
    ASSERT_NE( -1, SOCKET.socket );
    ASSERT_EQ( &( SOCKET.socket ), SOCKET.socketCloser.get() );
}

TEST(
    RawSenderTest
    , Send
)
{
    auto    rawSenderUnique = adhocrepeater::unique( adhocrepeater::newRawSender( "ra0" ) );
    ASSERT_NE( nullptr, rawSenderUnique.get() );
    auto &  rawSender = *rawSenderUnique;

    auto    packet = adhocrepeater::Packet();
    ASSERT_TRUE(
        adhocrepeater::setData(
            packet
            , "TADDR FADDR TESTDATA"
        )
    );

    ASSERT_TRUE(
        adhocrepeater::send(
            rawSender
            , packet
        )
    );
}
