﻿#include "adhocrepeater/util/test.h"
#include "adhocrepeater/packet/raw/receiver.h"
#include "adhocrepeater/packet/raw/sender.h"
#include "adhocrepeater/packet/socket.h"
#include "adhocrepeater/packet/packet.h"
#include "adhocrepeater/common/unique.h"

#include <string>

TEST(
    RawReceiverTest
    , New
)
{
    auto    rawReceiverUnique = adhocrepeater::unique( adhocrepeater::newRawReceiver( "ra0" ) );
    ASSERT_NE( nullptr, rawReceiverUnique.get() );

    const auto &    SOCKET = reinterpret_cast< adhocrepeater::Socket & >( *rawReceiverUnique );
    ASSERT_NE( -1, SOCKET.socket );
    ASSERT_EQ( &( SOCKET.socket ), SOCKET.socketCloser.get() );
}

TEST(
    RawReceiverTest
    , Receive
)
{
    auto    rawReceiverUnique = adhocrepeater::unique( adhocrepeater::newRawReceiver( "ra0" ) );
    ASSERT_NE( nullptr, rawReceiverUnique.get() );
    auto &  rawReceiver = *rawReceiverUnique;

    auto    rawSenderUnique = adhocrepeater::unique( adhocrepeater::newRawSender( "ra0" ) );
    ASSERT_NE( nullptr, rawSenderUnique.get() );
    auto &  rawSender = *rawSenderUnique;

    auto    sendPacket = adhocrepeater::Packet();
    ASSERT_TRUE(
        adhocrepeater::setData(
            sendPacket
            , "TADDR FADDR TESTDATA"
        )
    );

    ASSERT_TRUE(
        adhocrepeater::send(
            rawSender
            , sendPacket
        )
    );

    auto    receivePacket = adhocrepeater::Packet();
    ASSERT_TRUE(
        adhocrepeater::receive(
            rawReceiver
            , receivePacket
        )
    );

    const auto  SIZE = receivePacket.size();
    ASSERT_EQ( 20, SIZE );

    const auto  DATA = std::string(
        receivePacket.data()
        , SIZE
    );

    ASSERT_STREQ( "TADDR FADDR TESTDATA", DATA.c_str() );
}

TEST(
    RawReceiverTest
    , Poll
)
{
    auto    rawReceiverUnique = adhocrepeater::unique( adhocrepeater::newRawReceiver( "ra0" ) );
    ASSERT_NE( nullptr, rawReceiverUnique.get() );
    auto &  rawReceiver = *rawReceiverUnique;

    ASSERT_FALSE(
        adhocrepeater::poll(
            rawReceiver
            , 100
        )
    );

    auto    rawSenderUnique = adhocrepeater::unique( adhocrepeater::newRawSender( "ra0" ) );
    ASSERT_NE( nullptr, rawSenderUnique.get() );
    auto &  rawSender = *rawSenderUnique;

    auto    sendPacket = adhocrepeater::Packet();
    ASSERT_TRUE(
        adhocrepeater::setData(
            sendPacket
            , "TADDR FADDR TESTDATA"
        )
    );

    ASSERT_TRUE(
        adhocrepeater::send(
            rawSender
            , sendPacket
        )
    );

    ASSERT_TRUE(
        adhocrepeater::poll(
            rawReceiver
            , 100
        )
    );
}
