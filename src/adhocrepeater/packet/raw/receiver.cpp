﻿#include "adhocrepeater/packet/raw/receiver.h"
#include "adhocrepeater/packet/socket.h"
#include "adhocrepeater/common/unique.h"

#include <new>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace adhocrepeater {
    RawReceiver * newRawReceiver(
        const std::string & _INTERFACE
    )
    {
        auto    thisUnique = unique( new( std::nothrow )Socket );
        if( thisUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:RAW受信の生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }
        auto &  sender = *thisUnique;

        auto &  socket = sender.socket;
        socket = socketRaw();
        if( socket == -1 ) {
#ifdef  DEBUG
            std::printf( "E:ソケット生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }
        sender.socketCloser.reset( &socket );

        if( bindRaw(
            sender
            , _INTERFACE
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:接続に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        return reinterpret_cast< RawReceiver * >( thisUnique.release() );
    }

    void free(
        RawReceiver &   _this
    )
    {
        free( reinterpret_cast< Socket & >( _this ) );
    }

    bool receive(
        RawReceiver &   _this
        , Packet &      _packet
    )
    {
        return receive(
            reinterpret_cast< Socket & >( _this )
            , _packet
        );
    }

    bool poll(
        RawReceiver &   _this
        , int           _timeoutMilliSeconds
    )
    {
        return poll(
            reinterpret_cast< Socket & >( _this )
            , _timeoutMilliSeconds
        );
    }
}
