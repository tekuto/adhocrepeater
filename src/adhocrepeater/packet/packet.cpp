﻿#include "adhocrepeater/packet/packet.h"

#include <cstring>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace adhocrepeater {
    bool setData(
        Packet &                _this
        , const std::string &   _DATA
    )
    {
        return setData(
            _this
            , _DATA.c_str()
            , _DATA.size()
        );
    }

    bool setData(
        Packet &        _this
        , const void *  _DATA_PTR
        , size_t        _size
    )
    {
        const auto  DATA_PTR = static_cast< const char * >( _DATA_PTR );

        _this.assign(
            DATA_PTR
            , DATA_PTR + _size
        );

        return true;
    }
}
