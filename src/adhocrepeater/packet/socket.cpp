﻿#include "adhocrepeater/packet/socket.h"
#include "adhocrepeater/packet/packet.h"

#include <array>
#include <cstring>
#include <unistd.h>
#include <sys/socket.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <poll.h>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    typedef std::array< char, static_cast< unsigned short >( ~0 ) > Buffer;
}

namespace adhocrepeater {
    void Socket::CloseSocket::operator()(
        int *   _socketPtr
    ) const
    {
        close( *_socketPtr );
    }

    void free(
        Socket &    _this
    )
    {
        delete &_this;
    }

    int socketUdp(
    )
    {
        return socket(
            AF_INET
            , SOCK_DGRAM
            , 0
        );
    }

    int socketRaw(
    )
    {
        return socket(
            AF_PACKET
            , SOCK_RAW
            , htons( ETH_P_ALL )
        );
    }

    bool connectUdp(
        Socket &                _this
        , const std::string &   _ADDRESS
        , unsigned short        _port
    )
    {
        auto    addr = sockaddr_in();
        std::memset(
            &addr
            , 0
            , sizeof( addr )
        );
        addr.sin_family = AF_INET;
        addr.sin_addr.s_addr = inet_addr( _ADDRESS.c_str() );
        addr.sin_port = htons( _port );

        if( connect(
            _this.socket
            , reinterpret_cast< const sockaddr * >( &addr )
            , sizeof( addr )
        ) != 0 ) {
#ifdef  DEBUG
            std::printf( "E:connect()に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool bindUdp(
        Socket &            _this
        , unsigned short    _port
    )
    {
        auto    addr = sockaddr_in();
        std::memset(
            &addr
            , 0
            , sizeof( addr )
        );
        addr.sin_family = AF_INET;
        addr.sin_addr.s_addr = INADDR_ANY;
        addr.sin_port = htons( _port );

        if( bind(
            _this.socket
            , reinterpret_cast< const sockaddr * >( &addr )
            , sizeof( addr )
        ) != 0 ) {
#ifdef  DEBUG
            std::printf( "E:bind()に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool bindRaw(
        Socket &                _this
        , const std::string &   _INTERFACE
    )
    {
        const auto  INTERFACE_INDEX = if_nametoindex( _INTERFACE.c_str() );
        if( INTERFACE_INDEX == 0 ) {
#ifdef  DEBUG
            std::printf( "E:インターフェースに対応するインデックス取得に失敗\n" );
#endif  // DEBUG

            return false;
        }

        auto    addr = sockaddr_ll();
        std::memset(
            &addr
            , 0
            , sizeof( addr )
        );
        addr.sll_family = AF_PACKET;
        addr.sll_protocol = htons( ETH_P_ALL );
        addr.sll_ifindex = INTERFACE_INDEX;

        if( bind(
            _this.socket
            , reinterpret_cast< const sockaddr * >( &addr )
            , sizeof( addr )
        ) != 0 ) {
#ifdef  DEBUG
            std::printf( "E:bind()に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool send(
        Socket &            _this
        , const Packet &    _PACKET
    )
    {
        const auto  PACKET_SIZE = _PACKET.size();

        const auto  WRITE_SIZE = write(
            _this.socket
            , _PACKET.data()
            , PACKET_SIZE
        );
        if( WRITE_SIZE < 0 ) {
#ifdef  DEBUG
            std::printf( "E:write()に失敗\n" );
#endif  // DEBUG

            return false;
        } else if( static_cast< size_t >( WRITE_SIZE ) != PACKET_SIZE ) {
#ifdef  DEBUG
            std::printf( "E:書き込まれたサイズに差がある\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool receive(
        Socket &    _this
        , Packet &  _packet
    )
    {
        auto    buffer = Buffer();

        auto    dataPtr = buffer.data();

        const auto  READ_SIZE = read(
            _this.socket
            , dataPtr
            , buffer.size()
        );
        if( READ_SIZE < 0 ) {
#ifdef  DEBUG
            std::printf( "E:receive()に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( setData(
            _packet
            , dataPtr
            , READ_SIZE
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:受信データの格納に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool poll(
        Socket &    _this
        , int       _timeoutMilliSeconds
    )
    {
        auto    pollFd = pollfd();
        std::memset(
            &pollFd
            , 0
            , sizeof( pollFd )
        );
        pollFd.fd = _this.socket;
        pollFd.events = POLLIN;

        const auto  RESULT = ::poll(
            &pollFd
            , 1
            , _timeoutMilliSeconds
        );
        if( RESULT < 0 ) {
#ifdef  DEBUG
            std::printf( "E:poll()に失敗\n" );
#endif  // DEBUG

            return false;
        }
        if( RESULT == 0 ) {
            return false;
        }

        return true;
    }
}
