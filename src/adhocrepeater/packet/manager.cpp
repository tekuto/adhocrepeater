﻿#include "adhocrepeater/packet/manager.h"
#include "adhocrepeater/packet/packet.h"
#include "adhocrepeater/common/unique.h"

#include <new>
#include <utility>
#include <mutex>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace adhocrepeater {
    PacketManager * newPacketManager(
    )
    {
        auto    thisUnique = unique( new( std::nothrow )PacketManager );
        if( thisUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:パケットマネージャの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        return thisUnique.release();
    }

    void free(
        PacketManager & _this
    )
    {
        delete &_this;
    }

    bool add(
        PacketManager & _this
        , Packet &&     _packet
    )
    {
        auto    lock = std::unique_lock< std::mutex >( _this.mutex );

        _this.buffer.push_back( std::move( _packet ) );

        _this.cond.notify_all();

        return true;
    }

    void getBuffer(
        PacketManager &     _this
        , PacketBuffer &    _buffer
    )
    {
        _buffer.clear();

        auto &  buffer = _this.buffer;

        auto    lock = std::unique_lock< std::mutex >( _this.mutex );

        while( buffer.size() <= 0 ) {
            _this.cond.wait( lock );
        }

        std::swap(
            _buffer
            , buffer
        );
    }
}
