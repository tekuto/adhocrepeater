﻿#include "adhocrepeater/util/test.h"
#include "adhocrepeater/packet/manager.h"
#include "adhocrepeater/packet/packet.h"
#include "adhocrepeater/packet/buffer.h"
#include "adhocrepeater/common/unique.h"

#include <utility>

TEST(
    PacketManagerTest
    , New
)
{
    auto    managerUnique = adhocrepeater::unique( adhocrepeater::newPacketManager() );
    ASSERT_NE( nullptr, managerUnique.get() );

    const auto &    MANAGER = *managerUnique;
    ASSERT_EQ( 0, MANAGER.buffer.size() );
}

TEST(
    PacketManagerTest
    , Add
)
{
    auto    managerUnique = adhocrepeater::unique( adhocrepeater::newPacketManager() );
    ASSERT_NE( nullptr, managerUnique.get() );
    auto &  manager = *managerUnique;

    auto    packet = adhocrepeater::Packet();
    ASSERT_TRUE(
        adhocrepeater::setData(
            packet
            , "TESTDATA"
        )
    );

    ASSERT_TRUE(
        adhocrepeater::add(
            manager
            , std::move( packet )
        )
    );

    const auto &    BUFFER = manager.buffer;
    ASSERT_EQ( 1, BUFFER.size() );

    const auto &    PACKET = BUFFER[ 0 ];

    const auto  SIZE = PACKET.size();
    ASSERT_EQ( 8, SIZE );

    const auto  DATA = std::string(
        PACKET.data()
        , SIZE
    );

    ASSERT_STREQ( "TESTDATA", DATA.c_str() );
}

TEST(
    PacketManagerTest
    , GetBuffer
)
{
    auto    managerUnique = adhocrepeater::unique( adhocrepeater::newPacketManager() );
    ASSERT_NE( nullptr, managerUnique.get() );
    auto &  manager = *managerUnique;

    auto    packet = adhocrepeater::Packet();
    ASSERT_TRUE(
        adhocrepeater::setData(
            packet
            , "TESTDATA"
        )
    );

    ASSERT_TRUE(
        adhocrepeater::add(
            manager
            , std::move( packet )
        )
    );

    auto    buffer = adhocrepeater::PacketBuffer();
    adhocrepeater::getBuffer(
        manager
        , buffer
    );

    ASSERT_EQ( 0, manager.buffer.size() );

    ASSERT_EQ( 1, buffer.size() );

    const auto &    PACKET = buffer[ 0 ];

    const auto  SIZE = PACKET.size();
    ASSERT_EQ( 8, SIZE );

    const auto  DATA = std::string(
        PACKET.data()
        , SIZE
    );

    ASSERT_STREQ( "TESTDATA", DATA.c_str() );
}
