﻿#include "adhocrepeater/util/test.h"
#include "adhocrepeater/packet/udp/sender.h"
#include "adhocrepeater/packet/socket.h"
#include "adhocrepeater/packet/packet.h"
#include "adhocrepeater/common/unique.h"

TEST(
    UdpSenderTest
    , New
)
{
    auto    udpSenderUnique = adhocrepeater::unique(
        adhocrepeater::newUdpSender(
            "127.0.0.1"
            , 12345
        )
    );
    ASSERT_NE( nullptr, udpSenderUnique.get() );

    const auto &    SOCKET = reinterpret_cast< adhocrepeater::Socket & >( *udpSenderUnique );
    ASSERT_NE( -1, SOCKET.socket );
    ASSERT_EQ( &( SOCKET.socket ), SOCKET.socketCloser.get() );
}

TEST(
    UdpSenderTest
    , Send
)
{
    auto    udpSenderUnique = adhocrepeater::unique(
        adhocrepeater::newUdpSender(
            "127.0.0.1"
            , 12345
        )
    );
    ASSERT_NE( nullptr, udpSenderUnique.get() );
    auto &  udpSender = *udpSenderUnique;

    auto    packet = adhocrepeater::Packet();
    ASSERT_TRUE(
        adhocrepeater::setData(
            packet
            , "TESTDATA"
        )
    );

    ASSERT_TRUE(
        adhocrepeater::send(
            udpSender
            , packet
        )
    );
}
