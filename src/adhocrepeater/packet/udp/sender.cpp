﻿#include "adhocrepeater/packet/udp/sender.h"
#include "adhocrepeater/packet/packet.h"
#include "adhocrepeater/packet/socket.h"
#include "adhocrepeater/common/unique.h"

#include <new>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace adhocrepeater {
    UdpSender * newUdpSender(
        const std::string & _ADDRESS
        , unsigned short    _port
    )
    {
        auto    thisUnique = unique( new( std::nothrow )Socket );
        if( thisUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:UDP送信の生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }
        auto &  sender = *thisUnique;

        auto &  socket = sender.socket;
        socket = socketUdp();
        if( socket == -1 ) {
#ifdef  DEBUG
            std::printf( "E:ソケット生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }
        sender.socketCloser.reset( &socket );

        if( connectUdp(
            sender
            , _ADDRESS
            , _port
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:接続に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        return reinterpret_cast< UdpSender * >( thisUnique.release() );
    }

    void free(
        UdpSender & _this
    )
    {
        free( reinterpret_cast< Socket & >( _this ) );
    }

    bool send(
        UdpSender &         _this
        , const Packet &    _PACKET
    )
    {
        return send(
            reinterpret_cast< Socket & >( _this )
            , _PACKET
        );
    }
}
