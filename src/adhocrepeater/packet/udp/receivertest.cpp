﻿#include "adhocrepeater/util/test.h"
#include "adhocrepeater/packet/udp/receiver.h"
#include "adhocrepeater/packet/udp/sender.h"
#include "adhocrepeater/packet/socket.h"
#include "adhocrepeater/packet/packet.h"
#include "adhocrepeater/common/unique.h"

#include <string>

TEST(
    UdpReceiverTest
    , New
)
{
    auto    udpReceiverUnique = adhocrepeater::unique( adhocrepeater::newUdpReceiver( 12345 ) );
    ASSERT_NE( nullptr, udpReceiverUnique.get() );

    const auto &    SOCKET = reinterpret_cast< adhocrepeater::Socket & >( *udpReceiverUnique );
    ASSERT_NE( -1, SOCKET.socket );
    ASSERT_EQ( &( SOCKET.socket ), SOCKET.socketCloser.get() );
}

TEST(
    UdpReceiverTest
    , Receive
)
{
    auto    udpReceiverUnique = adhocrepeater::unique( adhocrepeater::newUdpReceiver( 12345 ) );
    ASSERT_NE( nullptr, udpReceiverUnique.get() );
    auto &  udpReceiver = *udpReceiverUnique;

    auto    udpSenderUnique = adhocrepeater::unique(
        adhocrepeater::newUdpSender(
            "127.0.0.1"
            , 12345
        )
    );
    ASSERT_NE( nullptr, udpSenderUnique.get() );
    auto &  udpSender = *udpSenderUnique;

    auto    sendPacket = adhocrepeater::Packet();
    ASSERT_TRUE(
        adhocrepeater::setData(
            sendPacket
            , "TESTDATA"
        )
    );

    ASSERT_TRUE(
        adhocrepeater::send(
            udpSender
            , sendPacket
        )
    );

    auto    receivePacket = adhocrepeater::Packet();
    ASSERT_TRUE(
        adhocrepeater::receive(
            udpReceiver
            , receivePacket
        )
    );

    const auto  SIZE = receivePacket.size();
    ASSERT_EQ( 8, SIZE );

    const auto  DATA = std::string(
        receivePacket.data()
        , SIZE
    );

    ASSERT_STREQ( "TESTDATA", DATA.c_str() );
}

TEST(
    UdpReceiverTest
    , Poll
)
{
    auto    udpReceiverUnique = adhocrepeater::unique( adhocrepeater::newUdpReceiver( 12345 ) );
    ASSERT_NE( nullptr, udpReceiverUnique.get() );
    auto &  udpReceiver = *udpReceiverUnique;

    ASSERT_FALSE(
        adhocrepeater::poll(
            udpReceiver
            , 100
        )
    );

    auto    udpSenderUnique = adhocrepeater::unique(
        adhocrepeater::newUdpSender(
            "127.0.0.1"
            , 12345
        )
    );
    ASSERT_NE( nullptr, udpSenderUnique.get() );
    auto &  udpSender = *udpSenderUnique;

    auto    sendPacket = adhocrepeater::Packet();
    ASSERT_TRUE(
        adhocrepeater::setData(
            sendPacket
            , "TESTDATA"
        )
    );

    ASSERT_TRUE(
        adhocrepeater::send(
            udpSender
            , sendPacket
        )
    );

    ASSERT_TRUE(
        adhocrepeater::poll(
            udpReceiver
            , 100
        )
    );
}
