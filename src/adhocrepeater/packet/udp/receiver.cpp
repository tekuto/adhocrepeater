﻿#include "adhocrepeater/packet/udp/receiver.h"
#include "adhocrepeater/packet/socket.h"
#include "adhocrepeater/common/unique.h"

#include <new>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace adhocrepeater {
    UdpReceiver * newUdpReceiver(
        unsigned short  _port
    )
    {
        auto    thisUnique = unique( new( std::nothrow )Socket );
        if( thisUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:UDP受信の生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }
        auto &  receiver = *thisUnique;

        auto &  socket = receiver.socket;
        socket = socketUdp();
        if( socket == -1 ) {
#ifdef  DEBUG
            std::printf( "E:ソケット生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }
        receiver.socketCloser.reset( &socket );

        if( bindUdp(
            receiver
            , _port
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:バインドに失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        return reinterpret_cast< UdpReceiver * >( thisUnique.release() );
    }

    void free(
        UdpReceiver &   _this
    )
    {
        free( reinterpret_cast< Socket & >( _this ) );
    }

    bool receive(
        UdpReceiver &   _this
        , Packet &      _packet
    )
    {
        return receive(
            reinterpret_cast< Socket & >( _this )
            , _packet
        );
    }

    bool poll(
        UdpReceiver &   _this
        , int           _timeoutMilliSeconds
    )
    {
        return poll(
            reinterpret_cast< Socket & >( _this )
            , _timeoutMilliSeconds
        );
    }
}
