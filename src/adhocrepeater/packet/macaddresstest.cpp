﻿#include "adhocrepeater/util/test.h"
#include "adhocrepeater/packet/macaddress.h"

TEST(
    MacAddressTest
    , StringToMacAddress
)
{
    auto    macAddress = adhocrepeater::MacAddress( { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 } );
    ASSERT_TRUE(
        adhocrepeater::stringToMacAddress(
            macAddress
            , "123456789abc"
        )
    );

    ASSERT_EQ( 0x12, macAddress[ 0 ] );
    ASSERT_EQ( 0x34, macAddress[ 1 ] );
    ASSERT_EQ( 0x56, macAddress[ 2 ] );
    ASSERT_EQ( 0x78, macAddress[ 3 ] );
    ASSERT_EQ( 0x9a, macAddress[ 4 ] );
    ASSERT_EQ( 0xbc, macAddress[ 5 ] );
}

TEST(
    MacAddressTest
    , StringToMacAddress2
)
{
    auto    macAddress = adhocrepeater::MacAddress( { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 } );
    ASSERT_TRUE(
        adhocrepeater::stringToMacAddress(
            macAddress
            , "000123456789"
        )
    );

    ASSERT_EQ( 0x00, macAddress[ 0 ] );
    ASSERT_EQ( 0x01, macAddress[ 1 ] );
    ASSERT_EQ( 0x23, macAddress[ 2 ] );
    ASSERT_EQ( 0x45, macAddress[ 3 ] );
    ASSERT_EQ( 0x67, macAddress[ 4 ] );
    ASSERT_EQ( 0x89, macAddress[ 5 ] );
}
