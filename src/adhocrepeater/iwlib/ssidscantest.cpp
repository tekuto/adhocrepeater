﻿#include "adhocrepeater/util/test.h"
#include "adhocrepeater/iwlib/ssidscan.h"
#include "adhocrepeater/iwlib/socket.h"
#include "adhocrepeater/common/time.h"
#include "adhocrepeater/common/unique.h"

TEST(
    SsidScan
    , InitSsidScan
)
{
    auto    iwSocketUnique = adhocrepeater::unique( adhocrepeater::newIwSocket( "ra0" ) );
    ASSERT_NE( nullptr, iwSocketUnique.get() );
    auto &  iwSocket = *iwSocketUnique;

    ASSERT_TRUE( adhocrepeater::initSsidScan( iwSocket ) );
}

TEST(
    SsidScan
    , GetSsidScanData
)
{
    auto    iwSocketUnique = adhocrepeater::unique( adhocrepeater::newIwSocket( "ra0" ) );
    ASSERT_NE( nullptr, iwSocketUnique.get() );
    auto &  iwSocket = *iwSocketUnique;

    ASSERT_TRUE( adhocrepeater::initSsidScan( iwSocket ) );

    auto    ssidScanData = adhocrepeater::SsidScanData();
    ssidScanData.size = 0;
    ASSERT_TRUE(
        adhocrepeater::getSsidScanData(
            ssidScanData
            , iwSocket
            , adhocrepeater::MicroSeconds( 10000000 )
            , adhocrepeater::MicroSeconds( 500000 )
        )
    );

    ASSERT_NE( 0, ssidScanData.size );
}

TEST(
    SsidScan
    , InitEventStream
)
{
    auto    ssidScanData = adhocrepeater::SsidScanData();
    ssidScanData.size = 10;

    auto    stream = stream_descr();
    adhocrepeater::initEventStream(
        stream
        , ssidScanData
    );

    ASSERT_EQ( ssidScanData.buffer.data(), stream.current );
    ASSERT_EQ( ssidScanData.buffer.data() + ssidScanData.size, stream.end );
}

TEST(
    SsidScan
    , GetScannedSsid
)
{
    auto    iwSocketUnique = adhocrepeater::unique( adhocrepeater::newIwSocket( "ra0" ) );
    ASSERT_NE( nullptr, iwSocketUnique.get() );
    auto &  iwSocket = *iwSocketUnique;

    ASSERT_TRUE( adhocrepeater::initSsidScan( iwSocket ) );

    auto    ssidScanData = adhocrepeater::SsidScanData();
    ssidScanData.size = 0;
    ASSERT_TRUE(
        adhocrepeater::getSsidScanData(
            ssidScanData
            , iwSocket
            , adhocrepeater::MicroSeconds( 10000000 )
            , adhocrepeater::MicroSeconds( 500000 )
        )
    );

    auto    stream = stream_descr();
    adhocrepeater::initEventStream(
        stream
        , ssidScanData
    );

    auto    scannedSsid = adhocrepeater::ScannedSsid();
    ASSERT_TRUE(
        adhocrepeater::getScannedSsid(
            scannedSsid
            , stream
            , iwSocket
        )
    );

    ASSERT_NE( 0, scannedSsid.mode );

    auto    channel = int( 0 );
    ASSERT_TRUE(
        adhocrepeater::toChannel(
            channel
            , iwSocket
            , scannedSsid.iwFreq
        )
    );
    ASSERT_NE( 0, channel );
}
