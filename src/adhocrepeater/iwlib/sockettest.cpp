﻿#include "adhocrepeater/util/test.h"
#include "adhocrepeater/iwlib/socket.h"
#include "adhocrepeater/common/unique.h"

TEST(
    IwSocketTest
    , New
)
{
    auto    iwSocketUnique = adhocrepeater::unique( adhocrepeater::newIwSocket( "ra0" ) );
    ASSERT_NE( nullptr, iwSocketUnique.get() );

    const auto &    IW_SOCKET = *iwSocketUnique;
    ASSERT_STREQ( "ra0", IW_SOCKET.interface.c_str() );
    ASSERT_NE( 0, IW_SOCKET.socket );
    ASSERT_NE( nullptr, IW_SOCKET.socketCloser.get() );
    ASSERT_NE( 0, IW_SOCKET.iwRange.num_frequency );
}

TEST(
    IwSocketTest
    , ToChannel
)
{
    auto    iwSocketUnique = adhocrepeater::unique( adhocrepeater::newIwSocket( "ra0" ) );
    ASSERT_NE( nullptr, iwSocketUnique.get() );
    auto &  iwSocket = *iwSocketUnique;

    auto    freq = double();
    ASSERT_LT(
        0
        , iw_channel_to_freq(
            11
            , &freq
            , &( iwSocket.iwRange )
        )
    );

    auto    iwFreq = iw_freq();
    iw_float2freq(
        freq
        , &iwFreq
    );

    auto    channel = int( 0 );
    ASSERT_TRUE(
        adhocrepeater::toChannel(
            channel
            , iwSocket
            , iwFreq
        )
    );

    ASSERT_EQ( 11, channel );
}

TEST(
    IwSocketTest
    , ToIwFreq
)
{
    auto    iwSocketUnique = adhocrepeater::unique( adhocrepeater::newIwSocket( "ra0" ) );
    ASSERT_NE( nullptr, iwSocketUnique.get() );
    auto &  iwSocket = *iwSocketUnique;

    auto    iwFreq = iw_freq();
    ASSERT_TRUE(
        adhocrepeater::toIwFreq(
            iwFreq
            , iwSocket
            , 11
        )
    );

    auto    freq = double();
    ASSERT_LT(
        0
        , iw_channel_to_freq(
            11
            , &freq
            , &( iwSocket.iwRange )
        )
    );

    ASSERT_EQ( freq, iw_freq2float( &iwFreq ) );
}
