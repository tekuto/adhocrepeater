﻿#include "adhocrepeater/iwlib/ssidscan.h"
#include "adhocrepeater/iwlib/socket.h"
#include "adhocrepeater/common/time.h"

#include <string>
#include <cstring>
#include <cerrno>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    bool getSsidScanData(
        adhocrepeater::SsidScanData &   _scanData
        , adhocrepeater::IwSocket &     _socket
        , bool &                        _loopEnd
    )
    {
        errno = 0;
        if( adhocrepeater::iwGetExt(
            _socket
            , SIOCGIWSCAN
            , [
                &_scanData
            ]
            (
                iwreq & _iwReq
            )
            {
                auto &  buffer = _scanData.buffer;

                _iwReq.u.data.pointer = buffer.data();
                _iwReq.u.data.flags = 0;
                _iwReq.u.data.length = buffer.size();
            }
            , [
                &_scanData
            ]
            (
                const iwreq &   _IW_REQ
            )
            {
                _scanData.size = _IW_REQ.u.data.length;
            }
        ) == false ) {
            if( errno != EAGAIN ) {
#ifdef  DEBUG
                std::printf( "E:iw_get_ext()がEAGAIN以外で失敗\n" );
#endif  // DEBUG

                _loopEnd = true;
                return false;
            }

            return true;
        }

        _loopEnd = true;
        return true;
    }

    void setMode(
        __u32 &                         _mode
        , bool &                        _exists
        , const iw_event &              _IW_EVENT
    )
    {
        _mode = _IW_EVENT.u.mode;

        _exists = true;
    }

    void setSsid(
        std::string &       _ssid
        , bool &            _exists
        , const iw_event &  _IW_EVENT
    )
    {
        const auto &    IW_ESSID = _IW_EVENT.u.essid;

        _ssid.assign(
            static_cast< const char * >( IW_ESSID.pointer )
            , IW_ESSID.length
        );

        _exists = true;
    }

    void setIwFreq(
        iw_freq &           _freq
        , bool &            _exists
        , const iw_event &  _IW_EVENT
    )
    {
        std::memcpy(
            &( _freq )
            , &( _IW_EVENT.u.freq )
            , sizeof( _freq )
        );

        _exists = true;
    }
}

namespace adhocrepeater {
    bool initSsidScan(
        IwSocket &  _socket
    )
    {
        return adhocrepeater::iwSetExt(
            _socket
            , SIOCSIWSCAN
            , [](
                iwreq & _iwReq
            )
            {
                auto &  data = _iwReq.u.data;

                data.pointer = nullptr;
                data.flags = 0;
                data.length = 0;
            }
        );
    }

    bool getSsidScanData(
        SsidScanData &      _scanData
        , IwSocket &        _socket
        , const Duration &  _TIMEOUT
        , const Duration &  _INTERVAL
    )
    {
        if( adhocrepeater::timeoutProc(
            _TIMEOUT
            , _INTERVAL
            , [
                &_scanData
                , &_socket
            ]
            (
                bool &  _loopEnd
            )
            {
                return ::getSsidScanData(
                    _scanData
                    , _socket
                    , _loopEnd
                );
            }
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSIDスキャン結果取得がタイムアウト\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    void initEventStream(
        stream_descr &      _stream
        , SsidScanData &    _scanData
    )
    {
        iw_init_event_stream(
            &_stream
            , _scanData.buffer.data()
            , _scanData.size
        );
    }

    bool getScannedSsid(
        ScannedSsid &       _scannedSsid
        , stream_descr &    _stream
        , IwSocket &        _socket
    )
    {
        auto    existsMode = false;
        auto    existsSsid = false;
        auto    existsIwFreq = false;

        auto    iwEvent = iw_event();
        while( iw_extract_event_stream(
            &_stream
            , &iwEvent
            , _socket.iwRange.we_version_compiled
        ) > 0 ) {
            switch( iwEvent.cmd ) {
            case SIOCGIWMODE:
                setMode(
                    _scannedSsid.mode
                    , existsMode
                    , iwEvent
                );
                break;

            case SIOCGIWESSID:
                setSsid(
                    _scannedSsid.ssid
                    , existsSsid
                    , iwEvent
                );
                break;

            case SIOCGIWFREQ:
                setIwFreq(
                    _scannedSsid.iwFreq
                    , existsIwFreq
                    , iwEvent
                );
                break;

            default:
                break;
            }

            if( existsMode == false ) {
                continue;
            } else if( existsSsid == false ) {
                continue;
            } else if( existsIwFreq == false ) {
                continue;
            }

            return true;
        }

        return false;
    }
}
