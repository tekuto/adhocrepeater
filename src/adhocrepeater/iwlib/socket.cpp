﻿#include "adhocrepeater/iwlib/socket.h"
#include "adhocrepeater/common/unique.h"

#include <iwlib.h>
#include <new>
#include <cstring>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    bool initIwRange(
        iw_range &              _iwRange
        , const int &           _SOCKET
        , const std::string &   _INTERFACE
    )
    {
        if( iw_get_range_info(
            _SOCKET
            , _INTERFACE.c_str()
            , &_iwRange
        ) != 0 ) {
#ifdef  DEBUG
            std::printf( "E:レンジ情報取得に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    struct IwGetExt
    {
        bool operator()(
            adhocrepeater::IwSocket &           _socket
            , int                               _request
            , iwreq &                           _iwReq
            , const adhocrepeater::GetIwReq &   _GET_IW_REQ
        ) const
        {
            if( iw_get_ext(
                _socket.socket
                , _socket.interface.c_str()
                , _request
                , &_iwReq
            ) < 0 ) {
#ifdef  DEBUG
                std::printf( "E:iw_get_ext()に失敗\n" );
#endif  // DEBUG

                return false;
            }

            _GET_IW_REQ( _iwReq );

            return true;
        }
    };

    struct IwSetExt
    {
        bool operator()(
            adhocrepeater::IwSocket &           _socket
            , int                               _request
            , iwreq &                           _iwReq
            , const adhocrepeater::GetIwReq &
        ) const
        {
            if( iw_set_ext(
                _socket.socket
                , _socket.interface.c_str()
                , _request
                , &_iwReq
            ) < 0 ) {
#ifdef  DEBUG
                std::printf( "E:iw_set_ext()に失敗\n" );
#endif  // DEBUG

                return false;
            }

            return true;
        }
    };

    template< typename IW_EXT >
    bool iwExt(
        adhocrepeater::IwSocket &           _socket
        , int                               _request
        , const adhocrepeater::SetIwReq &   _SET_IW_REQ
        , const adhocrepeater::GetIwReq &   _GET_IW_REQ
    )
    {
        auto    iwReq = iwreq();
        std::memset(
            &iwReq
            , 0
            , sizeof( iwReq )
        );

        _SET_IW_REQ( iwReq );

        return IW_EXT()(
            _socket
            , _request
            , iwReq
            , _GET_IW_REQ
        );
    }
}

namespace adhocrepeater {
    void IwSocket::CloseSocket::operator()(
        int *   _socketPtr
    ) const
    {
        iw_sockets_close( *_socketPtr );
    }

    IwSocket * newIwSocket(
        const std::string & _INTERFACE
    )
    {
        auto    socket = iw_sockets_open();
        if( socket < 0 ) {
#ifdef  DEBUG
            std::printf( "E:ソケットのオープンに失敗\n" );
#endif  // DEBUG

            return nullptr;
        }
        auto    socketCloser = IwSocket::SocketCloser( &socket );

        auto    iwRange = iw_range();
        if( initIwRange(
            iwRange
            , socket
            , _INTERFACE
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:レンジ情報の初期化に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    thisUnique = unique(
            new( std::nothrow )IwSocket{
                _INTERFACE,
            }
        );
        if( thisUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:ソケットの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }
        auto &  iwSocket = *thisUnique;

        socketCloser.release();

        iwSocket.socket = std::move( socket );
        iwSocket.socketCloser.reset( &( iwSocket.socket ) );
        std::memcpy(
            &( iwSocket.iwRange )
            , &iwRange
            , sizeof( iwSocket.iwRange )
        );

        return thisUnique.release();
    }

    void free(
        IwSocket &  _this
    )
    {
        delete &_this;
    }

    bool iwGetExt(
        IwSocket &          _this
        , int               _request
        , const SetIwReq &  _SET_IW_REQ
        , const GetIwReq &  _GET_IW_REQ
    )
    {
        return iwExt< IwGetExt >(
            _this
            , _request
            , _SET_IW_REQ
            , _GET_IW_REQ
        );
    }

    bool iwSetExt(
        IwSocket &          _this
        , int               _request
        , const SetIwReq &  _SET_IW_REQ
    )
    {
        return iwExt< IwSetExt >(
            _this
            , _request
            , _SET_IW_REQ
            , nullptr
        );
    }

    bool toChannel(
        int &               _channel
        , IwSocket &        _this
        , const iw_freq &   _IW_FREQ
    )
    {
        const auto  CHANNEL = iw_freq_to_channel(
            iw_freq2float( &_IW_FREQ )
            , &_this.iwRange
        );
        if( CHANNEL < 0 ) {
#ifdef  DEBUG
            std::printf( "E:周波数からチャンネルへの変換に失敗\n" );
#endif  // DEBUG

            return false;
        }

        _channel = CHANNEL;

        return true;
    }

    bool toIwFreq(
        iw_freq &       _iwFreq
        , IwSocket &    _this
        , int           _channel
    )
    {
        auto    freq = double();
        if( iw_channel_to_freq(
            _channel
            , &freq
            , &( _this.iwRange )
        ) < 0 ) {
#ifdef  DEBUG
            std::printf( "E:チャンネルから周波数への変換に失敗\n" );
#endif  // DEBUG

            return false;
        }

        iw_float2freq(
            freq
            , &_iwFreq
        );

        return true;
    }
}
