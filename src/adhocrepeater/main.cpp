﻿#include "adhocrepeater/ssid/manager/eventhandlers.h"
#include "adhocrepeater/ssid/manager/inserteventhandler.h"
#include "adhocrepeater/ssid/manager/insertevent.h"
#include "adhocrepeater/ssid/manager/manager.h"
#include "adhocrepeater/ssid/sender.h"
#include "adhocrepeater/ssid/receiver.h"
#include "adhocrepeater/ssid/changer.h"
#include "adhocrepeater/ssid/scanner.h"
#include "adhocrepeater/data/sender.h"
#include "adhocrepeater/data/receiver.h"
#include "adhocrepeater/config/config.h"
#include "adhocrepeater/common/unique.h"

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    adhocrepeater::Config * newConfig(
        int         _argc
        , char **   _argv
    )
    {
        if( _argc < 2 ) {
#ifdef  DEBUG
            std::printf( "E:コマンドライン引数で設定ファイルパスを指定していない\n" );
#endif  // DEBUG

            return nullptr;
        }

        const auto  CONFIG_FILE_PATH = _argv[ 1 ];

        auto    thisUnique = adhocrepeater::unique( adhocrepeater::newConfig( CONFIG_FILE_PATH ) );
        if( thisUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:設定の生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        return thisUnique.release();
    }
}

int main(
    int         _argc
    , char **   _argv
)
{
    auto    configUnique = adhocrepeater::unique(
        newConfig(
            _argc
            , _argv
        )
    );
    if( configUnique.get() == nullptr ) {
#ifdef  DEBUG
        std::printf( "E:設定の生成に失敗\n" );
#endif  // DEBUG

        return 1;
    }
    const auto &    CONFIG = *configUnique;

    auto    ssidSenderUnique = adhocrepeater::unique( adhocrepeater::newSsidSender( CONFIG ) );
    if( ssidSenderUnique.get() == nullptr ) {
#ifdef  DEBUG
        std::printf( "E:SSID送信の生成に失敗\n" );
#endif  // DEBUG

        return 1;
    }
    auto &  ssidSender = *ssidSenderUnique;

    auto    changerUnique = adhocrepeater::unique(
        adhocrepeater::newSsidChanger(
            CONFIG
            , ssidSender
        )
    );
    if( changerUnique.get() == nullptr ) {
#ifdef  DEBUG
        std::printf( "E:SSIDチェンジャの生成に失敗\n" );
#endif  // DEBUG

        return 1;
    }
    auto &  changer = *changerUnique;

    auto &  manager = adhocrepeater::getSsidManager( changer );

    auto    ssidReceiverUnique = adhocrepeater::unique(
        adhocrepeater::newSsidReceiver(
            CONFIG
            , manager
        )
    );
    if( ssidReceiverUnique.get() == nullptr ) {
#ifdef  DEBUG
        std::printf( "E:SSID受信の生成に失敗\n" );
#endif  // DEBUG

        return 1;
    }

    auto    scannerUnique = adhocrepeater::unique(
        adhocrepeater::newSsidScanner(
            CONFIG
            , manager
        )
    );
    if( scannerUnique.get() == nullptr ) {
#ifdef  DEBUG
        std::printf( "E:SSIDスキャナの生成に失敗\n" );
#endif  // DEBUG

        return 1;
    }

    auto    dataSenderUnique = adhocrepeater::unique(
        adhocrepeater::newDataSender(
            CONFIG
            , changer
        )
    );
    if( dataSenderUnique.get() == nullptr ) {
#ifdef  DEBUG
        std::printf( "E:データ送信の生成に失敗\n" );
#endif  // DEBUG

        return 1;
    }

    auto    receiverUnique = adhocrepeater::unique( adhocrepeater::newDataReceiver( CONFIG ) );
    if( receiverUnique.get() == nullptr ) {
#ifdef  DEBUG
        std::printf( "E:データ受信の生成に失敗\n" );
#endif  // DEBUG

        return 1;
    }

    return 0;
}
