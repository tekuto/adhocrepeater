﻿#include "adhocrepeater/ssid/changer.h"
#include "adhocrepeater/ssid/history.h"
#include "adhocrepeater/ssid/sender.h"
#include "adhocrepeater/ssid/manager/inserteventhandler.h"
#include "adhocrepeater/ssid/manager/insertevent.h"
#include "adhocrepeater/ssid/manager/eventhandlers.h"
#include "adhocrepeater/ssid/manager/manager.h"
#include "adhocrepeater/iwlib/socket.h"
#include "adhocrepeater/iwlib/ssidscan.h"
#include "adhocrepeater/config/config.h"
#include "adhocrepeater/common/thread.h"
#include "adhocrepeater/common/time.h"
#include "adhocrepeater/common/unique.h"

#include <iwlib.h>
#include <new>
#include <utility>
#include <string>
#include <mutex>
#include <cstring>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    adhocrepeater::SsidManagerInsertEventHandler * newSsidManagerInsertEventHandler(
        adhocrepeater::SsidChanger &    _changer
        , adhocrepeater::SsidSender&    _sender
    )
    {
        auto    thisUnique = adhocrepeater::unique(
            adhocrepeater::newSsidManagerInsertEventHandler(
                [
                    &_changer
                    , &_sender
                ]
                (
                    adhocrepeater::SsidManagerInsertEvent & _event
                )
                {
                    const auto &    SSID = adhocrepeater::getSsid( _event );

                    adhocrepeater::send(
                        _sender
                        , SSID
                    );

                    adhocrepeater::change(
                        _changer
                        , SSID
                    );
                }
            )
        );
        if( thisUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:SSIDマネージャの挿入イベントハンドラの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        return thisUnique.release();
    }

    adhocrepeater::SsidManager * newSsidManager(
        adhocrepeater::SsidChanger &    _changer
    )
    {
        const auto &    CHANGER = _changer.CONFIG.changer;

        auto    eventHandlersUnique = adhocrepeater::unique( adhocrepeater::newSsidManagerEventHandlers() );
        if( eventHandlersUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:SSIDマネージャのイベントハンドラ集合の生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }
        auto &  eventHandlers = *eventHandlersUnique;

        if( adhocrepeater::add(
            eventHandlers
            , *( _changer.insertEventHandlerUnique )
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:イベントハンドラ集合へのイベントハンドラ追加に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    thisUnique = adhocrepeater::unique(
            adhocrepeater::newSsidManager(
                eventHandlers
                , CHANGER.ssidManagerKeepTime
            )
        );
        if( thisUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:SSIDマネージャの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        return thisUnique.release();
    }

    bool getSsidFromSsidList(
        std::string &                   _ssid
        , adhocrepeater::SsidChanger &  _changer
        , adhocrepeater::SsidHistory &  _history
    )
    {
        auto &  ssidManager = *( _changer.ssidManagerUnique );

        auto    ssidList = adhocrepeater::SsidList();
        if( adhocrepeater::getSsidList(
            ssidManager
            , ssidList
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSIDリストの取得に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( ssidList.size() <= 0 ) {
#ifdef  DEBUG
            std::printf( "I:SSIDリストにSSIDがない\n" );
#endif  // DEBUG

            return false;
        }

        if( adhocrepeater::getNextSsid(
            _ssid
            , _history
            , ssidList
        ) == false ) {
#ifdef  DEBUG
            std::printf( "I:次のSSID取得に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    void getSsid(
        std::string &                   _ssid
        , adhocrepeater::SsidChanger &  _changer
        , adhocrepeater::SsidHistory &  _history
    )
    {
        const auto &    CONFIG = _changer.CONFIG;
        const auto &    CHANGER = CONFIG.changer;

        auto    lock = std::unique_lock< std::mutex >( _changer.mutex );

        while( 1 ) {
            if( _changer.connected == true ) {
                _changer.cond.wait( lock );

                continue;
            }

            if( _changer.existsNextSsid == true ) {
                _changer.existsNextSsid = false;
                _ssid = _changer.nextSsid;

                break;
            }

            if( getSsidFromSsidList(
                _ssid
                , _changer
                , _history
            ) == true ) {
                break;
            }

            _changer.cond.wait_for(
                lock
                , CHANGER.getSsidWaitTime
            );
        }
    }

    bool initIwFreq(
        iw_freq &                       _iwFreq
        , const adhocrepeater::Config & _CONFIG
        , adhocrepeater::IwSocket &     _socket
    )
    {
        const auto &    TARGET_SSID = _CONFIG.targetSsid;

        if( adhocrepeater::toIwFreq(
            _iwFreq
            , _socket
            , TARGET_SSID.channel
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:iw_freqへの変換に失敗\n" );
#endif  // DEBUG

            return false;
        }

        _iwFreq.flags = IW_FREQ_FIXED;

        return true;
    }

    bool changeSsid(
        adhocrepeater::IwSocket &   _socket
        , const std::string &       _SSID
    )
    {
        return adhocrepeater::iwSetExt(
            _socket
            , SIOCSIWESSID
            , [
                &_SSID
            ]
            (
                iwreq & _iwReq
            )
            {
                auto &  essid = _iwReq.u.essid;

                essid.flags = 1;
                essid.pointer = const_cast< char * >( _SSID.c_str() );
                essid.length = _SSID.size();
                if( iw_get_kernel_we_version() < 21 ) {
                    essid.length++;
                }
            }
        );
    }

    bool changeFreq(
        adhocrepeater::IwSocket &   _socket
        , const iw_freq &           _IW_FREQ
    )
    {
        return adhocrepeater::iwSetExt(
            _socket
            , SIOCSIWFREQ
            , [
                &_IW_FREQ
            ]
            (
                iwreq & _iwReq
            )
            {
                auto &  freq = _iwReq.u.freq;

                std::memcpy(
                    &freq
                    , &_IW_FREQ
                    , sizeof( freq )
                );
            }
        );
    }

    bool isTargetSsid(
        const adhocrepeater::ScannedSsid &  _SCANNED_SSID
        , const adhocrepeater::Config &     _CONFIG
        , adhocrepeater::IwSocket &         _socket
        , const std::string &               _TARGET_SSID
    )
    {
        const auto &    TARGET_SSID = _CONFIG.targetSsid;

        if( _SCANNED_SSID.mode != IW_MODE_ADHOC ) {
#ifdef  DEBUG
            std::printf( "I:アドホックモードのSSIDではない\n" );
#endif  // DEBUG

            return false;
        }

        if( _SCANNED_SSID.ssid != _TARGET_SSID ) {
#ifdef  DEBUG
            std::printf( "I:SSIDがマッチしない\n" );
#endif  // DEBUG

            return false;
        }

        auto    channel = int();
        if( adhocrepeater::toChannel(
            channel
            , _socket
            , _SCANNED_SSID.iwFreq
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:チャンネルへの変換に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( channel != TARGET_SSID.channel ) {
#ifdef  DEBUG
            std::printf( "I:チャンネルがマッチしない\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool isExistsSsid(
        adhocrepeater::SsidScanData &   _scanData
        , const adhocrepeater::Config & _CONFIG
        , adhocrepeater::IwSocket &     _socket
        , const std::string &           _SSID
    )
    {
        auto    stream = stream_descr();
        adhocrepeater::initEventStream(
            stream
            , _scanData
        );

        while( 1 ) {
            auto    scannedSsid = adhocrepeater::ScannedSsid();
            if( adhocrepeater::getScannedSsid(
                scannedSsid
                , stream
                , _socket
            ) == false ) {
                break;
            }

            if( isTargetSsid(
                scannedSsid
                , _CONFIG
                , _socket
                , _SSID
            ) == false ) {
#ifdef  DEBUG
                std::printf( "I:対象外のSSID\n" );
#endif  // DEBUG

                continue;
            }

            return true;
        }

        return false;
    }

    bool findSsid(
        const adhocrepeater::Config &   _CONFIG
        , adhocrepeater::IwSocket &     _socket
        , const std::string &           _SSID
        , bool &                        _loopEnd
    )
    {
        const auto &    CHANGER = _CONFIG.changer;

        if( adhocrepeater::initSsidScan( _socket ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSIDのスキャン開始に失敗\n" );
#endif  // DEBUG

            return false;
        }

        auto    scanData = adhocrepeater::SsidScanData();
        if( adhocrepeater::getSsidScanData(
            scanData
            , _socket
            , CHANGER.getScanDataTimeout
            , CHANGER.getScanDataInterval
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSIDスキャンデータの取得に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( isExistsSsid(
            scanData
            , _CONFIG
            , _socket
            , _SSID
        ) == false ) {
#ifdef  DEBUG
            std::printf( "I:SSIDがスキャンデータ内に存在しない\n" );
#endif  // DEBUG

            return false;
        }

        _loopEnd = true;
        return true;
    }

    bool findSsid(
        const adhocrepeater::Config &   _CONFIG
        , adhocrepeater::IwSocket &     _socket
        , const std::string &           _SSID
    )
    {
        const auto &    CHANGER = _CONFIG.changer;

        return adhocrepeater::timeoutProc(
            CHANGER.findSsidTimeout
            , CHANGER.findSsidInterval
            , [
                &_CONFIG
                , &_socket
                , &_SSID
            ]
            (
                bool &  _loopEnd
            )
            {
                return findSsid(
                    _CONFIG
                    , _socket
                    , _SSID
                    , _loopEnd
                );
            }
        );
    }

    void connectEnd(
        adhocrepeater::SsidChanger &    _changer
        , const std::string &           _SSID
    )
    {
        auto    lock = std::unique_lock< std::mutex >( _changer.mutex );

        _changer.connected = true;
        _changer.currentSsid = _SSID;
        _changer.connectTime = adhocrepeater::Time::now();

#ifdef  DEBUG
        std::printf(
            "I:SSIDの変更完了: %s\n"
            , _SSID.c_str()
        );
#endif  // DEBUG
    }

    void threadProc(
        adhocrepeater::SsidChanger &    _changer
        , adhocrepeater::SsidHistory &  _history
        , adhocrepeater::IwSocket &     _socket
        , const iw_freq &               _IW_FREQ
    )
    {
        auto    ssid = std::string();

        getSsid(
            ssid
            , _changer
            , _history
        );
#ifdef  DEBUG
        std::printf(
            "I:SSIDの変更: %s\n"
            , ssid.c_str()
        );
#endif  // DEBUG

        if( adhocrepeater::add(
            _history
            , ssid
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSID履歴へのSSID追加に失敗\n" );
#endif  // DEBUG

            return;
        }

        if( changeSsid(
            _socket
            , ssid
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSIDの変更に失敗\n" );
#endif  // DEBUG

            return;
        }

        if( changeFreq(
            _socket
            , _IW_FREQ
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:チャンネルの変更に失敗\n" );
#endif  // DEBUG

            return;
        }

        if( findSsid(
            _changer.CONFIG
            , _socket
            , ssid
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSIDの検索に失敗\n" );
#endif  // DEBUG

            return;
        }

        connectEnd(
            _changer
            , ssid
        );
    }

    void threadProc(
        adhocrepeater::Thread &         _thread
        , adhocrepeater::SsidChanger &  _changer
    )
    {
        const auto &    CONFIG = _changer.CONFIG;
        const auto &    INTERFACES = CONFIG.interfaces;
        const auto &    CHANGER = CONFIG.changer;

        auto    ssidHistoryUnique = adhocrepeater::unique( adhocrepeater::newSsidHistory( CHANGER.ssidHistorySize ) );
        if( ssidHistoryUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:SSID履歴の生成に失敗\n" );
#endif  // DEBUG

            return;
        }
        auto &  ssidHistory = *ssidHistoryUnique;

        auto    socketUnique = adhocrepeater::unique( adhocrepeater::newIwSocket( INTERFACES.repeater ) );
        if( socketUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:SSIDスキャン用ソケットの生成に失敗\n" );
#endif  // DEBUG

            return;
        }
        auto &  socket = *socketUnique;

        auto    iwFreq = iw_freq();
        if( initIwFreq(
            iwFreq
            , CONFIG
            , socket
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:iw_freqの初期化に失敗\n" );
#endif  // DEBUG

            return;
        }

        while( adhocrepeater::isRunning( _thread ) == true ) {
            threadProc(
                _changer
                , ssidHistory
                , socket
                , iwFreq
            );
        }
    }

    adhocrepeater::Thread * newThread(
        adhocrepeater::SsidChanger &    _changer
    )
    {
        auto    thisUnique = adhocrepeater::unique(
            adhocrepeater::newThread(
                [
                    &_changer
                ]
                (
                    adhocrepeater::Thread & _thread
                )
                {
                    threadProc(
                        _thread
                        , _changer
                    );
                }
            )
        );
        if( thisUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:スレッドの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        return thisUnique.release();
    }

    bool isForceChange(
        const adhocrepeater::SsidChanger &  _CHANGER
        , const std::string &               _SSID
    )
    {
        const auto &    CONFIG = _CHANGER.CONFIG;
        const auto &    TARGET_SSID = CONFIG.targetSsid;

        if( _CHANGER.connected == false ) {
#ifdef  DEBUG
            std::printf( "I:接続済みでないためSSID強制変更不可\n" );
#endif  // DEBUG

            return false;
        }

        if( std::strncmp(
            _SSID.c_str()
            , _CHANGER.currentSsid.c_str()
            , TARGET_SSID.prefixSize
        ) != 0 ) {
#ifdef  DEBUG
            std::printf( "I:接頭辞が合致しないためSSID強制変更不可\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    void disconnect(
        adhocrepeater::SsidChanger &    _this
    )
    {
        _this.connected = false;
    }
}

namespace adhocrepeater {
    SsidChanger * newSsidChanger(
        const Config &  _CONFIG
        , SsidSender &  _sender
    )
    {
        auto    thisUnique = unique(
            new( std::nothrow )SsidChanger{
                _CONFIG,
            }
        );
        if( thisUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:SSIDチェンジャの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }
        auto &  changer = *thisUnique;

        changer.existsNextSsid = false;
        changer.connected = false;

        auto    insertEventHandlerUnique = unique(
            ::newSsidManagerInsertEventHandler(
                changer
                , _sender
            )
        );
        if( insertEventHandlerUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:SSIDマネージャの挿入イベントハンドラの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }
        changer.insertEventHandlerUnique = std::move( insertEventHandlerUnique );

        auto    ssidManagerUnique = unique( ::newSsidManager( changer ) );
        if( ssidManagerUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:SSIDマネージャの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }
        changer.ssidManagerUnique = std::move( ssidManagerUnique );

        auto    threadUnique = unique( ::newThread( changer ) );
        if( threadUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:スレッドの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }
        changer.threadUnique = std::move( threadUnique );

        return thisUnique.release();
    }

    void free(
        SsidChanger &   _this
    )
    {
        delete &_this;
    }

    void change(
        SsidChanger &           _this
        , const std::string &   _SSID
    )
    {
        auto &  existsNextSsid = _this.existsNextSsid;

        auto    lock = std::unique_lock< std::mutex >( _this.mutex );

        if( isForceChange(
            _this
            , _SSID
        ) == true ) {
#ifdef  DEBUG
            std::printf( "I:SSIDを強制変更のため切断\n" );
#endif  // DEBUG

            ::disconnect( _this );
        } else if( existsNextSsid == true ) {
#ifdef  DEBUG
            std::printf( "I:新しいSSIDが設定済みのため変更不可\n" );
#endif  // DEBUG

            return;
        }

        existsNextSsid = true;
        _this.nextSsid = _SSID;

        _this.cond.notify_all();

#ifdef  DEBUG
        std::printf(
            "I:SSID変更要求: %s\n"
            , _SSID.c_str()
        );
#endif  // DEBUG
    }

    void disconnect(
        SsidChanger &   _this
    )
    {
        const auto &    CHANGER = _this.CONFIG.changer;

        const auto  CURRENT_TIME = Time::now();

        auto    lock = std::unique_lock< std::mutex >( _this.mutex );

        if( _this.connected == false ) {
#ifdef  DEBUG
            std::printf( "I:SSID変更前のため切断不可\n" );
#endif  // DEBUG

            return;
        }

        const auto  ELAPSED_TIME = CURRENT_TIME - _this.connectTime;
        if( ELAPSED_TIME < CHANGER.disconnectableTime ) {
#ifdef  DEBUG
            std::printf( "I:一定時間経過していないため切断不可\n" );
#endif  // DEBUG

            return;
        }

        ::disconnect( _this );

        _this.cond.notify_all();
    }

    SsidManager & getSsidManager(
        SsidChanger &   _this
    )
    {
        return *( _this.ssidManagerUnique );
    }
}
