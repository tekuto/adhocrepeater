﻿#include "adhocrepeater/ssid/manager/manager.h"
#include "adhocrepeater/ssid/manager/eventhandlers.h"
#include "adhocrepeater/ssid/manager/insertevent.h"
#include "adhocrepeater/ssid/manager/ssid.h"
#include "adhocrepeater/common/time.h"
#include "adhocrepeater/common/unique.h"

#include <new>
#include <utility>
#include <algorithm>
#include <mutex>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    void eraseTimeoutSsid(
        adhocrepeater::SsidManager &        _manager
        , const adhocrepeater::TimePoint &  _CURRENT_TIME
    )
    {
        auto &  ssidSet = _manager.ssidSet;

        auto    ssidList = adhocrepeater::SsidList();
        for( const auto & SSID : ssidSet ) {
            const auto  TIMEOUT_TIME = SSID.updateTime + _manager.KEEP_TIME;
            if( _CURRENT_TIME > TIMEOUT_TIME ) {
                ssidList.push_back( SSID.ssid );
            }
        }

        auto    lock = std::unique_lock< std::mutex >( _manager.mutex );

        for( const auto & SSID_STRING : ssidList ) {
            const auto  END = ssidSet.end();

            auto    it = std::find_if(
                ssidSet.begin()
                , END
                , [
                    &SSID_STRING
                ]
                (
                    const adhocrepeater::Ssid & _SSID
                )
                {
                    return _SSID == SSID_STRING;
                }
            );
            if( it == END ) {
                continue;
            }

            ssidSet.erase( it );
        }
    }

    adhocrepeater::Ssid * findSsid(
        adhocrepeater::SsidManager::SsidSet &   _ssidSet
        , const std::string &                   _SSID_STRING
    )
    {
        const auto  END = _ssidSet.end();

        auto    it = std::find_if(
            _ssidSet.begin()
            , END
            , [
                &_SSID_STRING
            ]
            (
                const adhocrepeater::Ssid & _SSID
            )
            {
                return _SSID == _SSID_STRING;
            }
        );
        if( it == END ) {
#ifdef  DEBUG
            std::printf( "I:対応するSSIDが存在しない\n" );
#endif  // DEBUG

            return nullptr;
        }

        return const_cast< adhocrepeater::Ssid * >( &( *it ) );
    }

    bool update(
        adhocrepeater::Ssid &               _ssid
        , const adhocrepeater::TimePoint &  _CURRENT_TIME
    )
    {
        adhocrepeater::update(
            _ssid
            , _CURRENT_TIME
        );

        return true;
    }

    bool callInsertEventHandler(
        adhocrepeater::SsidManager &    _manager
        , const std::string &           _SSID
    )
    {
        auto    eventUnique = adhocrepeater::unique(
            adhocrepeater::newSsidManagerInsertEvent(
                _manager
                , _SSID
            )
        );
        if( eventUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:SSIDマネージャの挿入イベントの生成に失敗\n" );
#endif  // DEBUG

            return false;
        }
        auto &  event = *eventUnique;

        adhocrepeater::call(
            *( _manager.eventHandlersUnique )
            , event
        );

        return true;
    }

    bool insertWithLock(
        adhocrepeater::SsidManager &        _manager
        , const std::string &               _SSID
        , const adhocrepeater::TimePoint &  _CURRENT_TIME
    )
    {
        auto    lock = std::unique_lock< std::mutex >( _manager.mutex );

        const auto  RESULT = _manager.ssidSet.insert(
            adhocrepeater::Ssid{
                _SSID,
                _CURRENT_TIME,
            }
        );
        if( RESULT.second == false ) {
#ifdef  DEBUG
            std::printf( "E:SSID集合への要素追加に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool insert(
        adhocrepeater::SsidManager &        _manager
        , const std::string &               _SSID
        , const adhocrepeater::TimePoint &  _CURRENT_TIME
    )
    {
        if( insertWithLock(
            _manager
            , _SSID
            , _CURRENT_TIME
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSID集合への要素追加に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( callInsertEventHandler(
            _manager
            , _SSID
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:イベントハンドラの呼び出しに失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool initSsidList(
        adhocrepeater::SsidList &       _ssidList
        , adhocrepeater::SsidManager &  _manager
    )
    {
        auto    lock = std::unique_lock< std::mutex >( _manager.mutex );

        for( const auto & SSID : _manager.ssidSet ) {
            _ssidList.push_back( SSID.ssid );
        }

        return true;
    }
}

namespace adhocrepeater {
    SsidManager * newSsidManager(
        const SsidManagerEventHandlers &    _EVENT_HANDLERS
        , const MicroSeconds &              _KEEP_TIME
    )
    {
        auto    eventHandlersUnique = unique( clone( _EVENT_HANDLERS ) );
        if( eventHandlersUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:SSIDマネージャのイベントハンドラのクローン生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    thisUnique = unique(
            new( std::nothrow )SsidManager{
                std::move( eventHandlersUnique ),
                _KEEP_TIME,
            }
        );
        if( thisUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:SSIDマネージャの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        return thisUnique.release();
    }

    void free(
        SsidManager &   _this
    )
    {
        delete &_this;
    }

    bool update(
        SsidManager &           _this
        , const std::string &   _SSID
    )
    {
        const auto  CURRENT_TIME = Time::now();

        eraseTimeoutSsid(
            _this
            , CURRENT_TIME
        );

        auto    ssidPtr = findSsid(
            _this.ssidSet
            , _SSID
        );
        if( ssidPtr != nullptr ) {
            return ::update(
                *ssidPtr
                , CURRENT_TIME
            );
        } else {
            return insert(
                _this
                , _SSID
                , CURRENT_TIME
            );
        }
    }

    bool getSsidList(
        SsidManager &   _this
        , SsidList &    _ssidList
    )
    {
        const auto  CURRENT_TIME = Time::now();

        eraseTimeoutSsid(
            _this
            , CURRENT_TIME
        );

        auto    ssidList = SsidList();

        if( initSsidList(
            ssidList
            , _this
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSIDリストの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        _ssidList = std::move( ssidList );

        return true;
    }
}
