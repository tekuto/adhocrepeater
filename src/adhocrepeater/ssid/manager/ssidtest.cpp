﻿#include "adhocrepeater/util/test.h"
#include "adhocrepeater/ssid/manager/ssid.h"
#include "adhocrepeater/common/time.h"
#include "adhocrepeater/common/unique.h"

TEST(
    SsidTest
    , Update
)
{
    auto    ssid = adhocrepeater::Ssid{
        "TESTSSID",
        adhocrepeater::Time::now(),
    };

    const auto  CURRENT_TIME = adhocrepeater::Time::now();

    adhocrepeater::update(
        ssid
        , CURRENT_TIME
    );

    ASSERT_EQ(
        CURRENT_TIME.time_since_epoch().count()
        , ssid.updateTime.time_since_epoch().count()
    );
}

TEST(
    SsidTest
    , Equals
)
{
    const auto  SSID = adhocrepeater::Ssid{
        "TESTSSID",
        adhocrepeater::Time::now(),
    };

    ASSERT_TRUE( SSID == "TESTSSID" );
    ASSERT_FALSE( SSID == "TESTSSID2" );
}

TEST(
    SsidTest
    , LessThan
)
{
    const auto  CURRENT_TIME = adhocrepeater::Time::now();

    const auto  A = adhocrepeater::Ssid{
        "A",
        CURRENT_TIME,
    };
    const auto  B = adhocrepeater::Ssid{
        "B",
        CURRENT_TIME,
    };
    const auto  A2 = adhocrepeater::Ssid{
        "A",
        CURRENT_TIME,
    };

    ASSERT_TRUE( A < B );
    ASSERT_FALSE( B < A );
    ASSERT_FALSE( A < A2 );
}
