﻿#include "adhocrepeater/util/test.h"
#include "adhocrepeater/ssid/manager/inserteventhandler.h"
#include "adhocrepeater/ssid/manager/insertevent.h"
#include "adhocrepeater/ssid/manager/eventhandlers.h"
#include "adhocrepeater/ssid/manager/manager.h"
#include "adhocrepeater/common/time.h"
#include "adhocrepeater/common/unique.h"

#include <string>

TEST(
    SsidManagerInsertEventHandlerTest
    , New
)
{
    auto    eventHandlerUnique = adhocrepeater::unique(
        adhocrepeater::newSsidManagerInsertEventHandler(
            [](
                adhocrepeater::SsidManagerInsertEvent &
            ){}
        )
    );
    ASSERT_NE( nullptr, eventHandlerUnique.get() );

    const auto &    EVENT_HANDLER = *eventHandlerUnique;
    ASSERT_TRUE( static_cast< bool >( EVENT_HANDLER.proc ) );
}

TEST(
    SsidManagerInsertEventHandlerTest
    , Call
)
{
    auto    ssid = std::string();

    auto    eventHandlerUnique = adhocrepeater::unique(
        adhocrepeater::newSsidManagerInsertEventHandler(
            [
                &ssid
            ]
            (
                adhocrepeater::SsidManagerInsertEvent & _event
            )
            {
                ssid = adhocrepeater::getSsid( _event );
            }
        )
    );
    ASSERT_NE( nullptr, eventHandlerUnique.get() );
    auto &  eventHandler = *eventHandlerUnique;

    auto    eventHandlersUnique = adhocrepeater::unique( adhocrepeater::newSsidManagerEventHandlers() );
    ASSERT_NE( nullptr, eventHandlersUnique.get() );
    const auto &    EVENT_HANDLERS = *eventHandlersUnique;

    const auto  KEEP_TIME = adhocrepeater::MicroSeconds( 1000000 );

    auto    managerUnique = adhocrepeater::unique(
        adhocrepeater::newSsidManager(
            EVENT_HANDLERS
            , KEEP_TIME
        )
    );
    ASSERT_NE( nullptr, managerUnique.get() );
    auto &  manager = *managerUnique;

    auto    eventUnique = adhocrepeater::unique(
        adhocrepeater::newSsidManagerInsertEvent(
            manager
            , "TESTSSID"
        )
    );
    ASSERT_NE( nullptr, eventUnique.get() );
    auto &  event = *eventUnique;

    adhocrepeater::call(
        eventHandler
        , event
    );

    ASSERT_STREQ( "TESTSSID", ssid.c_str() );
}
