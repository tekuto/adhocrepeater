﻿#include "adhocrepeater/util/test.h"
#include "adhocrepeater/ssid/manager/insertevent.h"
#include "adhocrepeater/ssid/manager/eventhandlers.h"
#include "adhocrepeater/ssid/manager/manager.h"
#include "adhocrepeater/common/time.h"
#include "adhocrepeater/common/unique.h"

TEST(
    SsidManagerInsertEventTest
    , New
)
{
    auto    eventHandlersUnique = adhocrepeater::unique( adhocrepeater::newSsidManagerEventHandlers() );
    ASSERT_NE( nullptr, eventHandlersUnique.get() );
    const auto &    EVENT_HANDLERS = *eventHandlersUnique;

    const auto  KEEP_TIME = adhocrepeater::MicroSeconds( 1000000 );

    auto    managerUnique = adhocrepeater::unique(
        adhocrepeater::newSsidManager(
            EVENT_HANDLERS
            , KEEP_TIME
        )
    );
    ASSERT_NE( nullptr, managerUnique.get() );
    auto &  manager = *managerUnique;

    auto    eventUnique = adhocrepeater::unique(
        adhocrepeater::newSsidManagerInsertEvent(
            manager
            , "TESTSSID"
        )
    );
    ASSERT_NE( nullptr, eventUnique.get() );

    const auto &    EVENT = *eventUnique;
    ASSERT_EQ( &manager, &( EVENT.manager ) );
    ASSERT_STREQ( "TESTSSID", EVENT.ssid.c_str() );
}

TEST(
    SsidManagerInsertEventTest
    , GetManager
)
{
    auto    eventHandlersUnique = adhocrepeater::unique( adhocrepeater::newSsidManagerEventHandlers() );
    ASSERT_NE( nullptr, eventHandlersUnique.get() );
    const auto &    EVENT_HANDLERS = *eventHandlersUnique;

    const auto  KEEP_TIME = adhocrepeater::MicroSeconds( 1000000 );

    auto    managerUnique = adhocrepeater::unique(
        adhocrepeater::newSsidManager(
            EVENT_HANDLERS
            , KEEP_TIME
        )
    );
    ASSERT_NE( nullptr, managerUnique.get() );
    auto &  manager = *managerUnique;

    auto    eventUnique = adhocrepeater::unique(
        adhocrepeater::newSsidManagerInsertEvent(
            manager
            , "TESTSSID"
        )
    );
    ASSERT_NE( nullptr, eventUnique.get() );
    auto &  event = *eventUnique;

    ASSERT_EQ( &manager, &( adhocrepeater::getManager( event ) ) );
}

TEST(
    SsidManagerInsertEventTest
    , GetSsid
)
{
    auto    eventHandlersUnique = adhocrepeater::unique( adhocrepeater::newSsidManagerEventHandlers() );
    ASSERT_NE( nullptr, eventHandlersUnique.get() );
    const auto &    EVENT_HANDLERS = *eventHandlersUnique;

    const auto  KEEP_TIME = adhocrepeater::MicroSeconds( 1000000 );

    auto    managerUnique = adhocrepeater::unique(
        adhocrepeater::newSsidManager(
            EVENT_HANDLERS
            , KEEP_TIME
        )
    );
    ASSERT_NE( nullptr, managerUnique.get() );
    auto &  manager = *managerUnique;

    auto    eventUnique = adhocrepeater::unique(
        adhocrepeater::newSsidManagerInsertEvent(
            manager
            , "TESTSSID"
        )
    );
    ASSERT_NE( nullptr, eventUnique.get() );
    auto &  event = *eventUnique;

    ASSERT_STREQ( "TESTSSID", adhocrepeater::getSsid( event ).c_str() );
}
