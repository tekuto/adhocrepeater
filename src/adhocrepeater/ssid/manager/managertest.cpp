﻿#include "adhocrepeater/util/test.h"
#include "adhocrepeater/ssid/manager/manager.h"
#include "adhocrepeater/ssid/manager/eventhandlers.h"
#include "adhocrepeater/ssid/manager/inserteventhandler.h"
#include "adhocrepeater/ssid/manager/insertevent.h"
#include "adhocrepeater/ssid/manager/ssid.h"
#include "adhocrepeater/ssid/list.h"
#include "adhocrepeater/common/time.h"
#include "adhocrepeater/common/unique.h"

#include <string>
#include <algorithm>
#include <unistd.h>

TEST(
    SsidManagerTest
    , New
)
{
    auto    eventHandlersUnique = adhocrepeater::unique( adhocrepeater::newSsidManagerEventHandlers() );
    ASSERT_NE( nullptr, eventHandlersUnique.get() );
    const auto &    EVENT_HANDLERS = *eventHandlersUnique;

    const auto  KEEP_TIME = adhocrepeater::MicroSeconds( 10 );

    auto    managerUnique = adhocrepeater::unique(
        adhocrepeater::newSsidManager(
            EVENT_HANDLERS
            , KEEP_TIME
        )
    );
    ASSERT_NE( nullptr, managerUnique.get() );

    const auto &    MANAGER = *managerUnique;
    ASSERT_NE( nullptr, MANAGER.eventHandlersUnique.get() );
    ASSERT_EQ( 10, MANAGER.KEEP_TIME.count() );
    ASSERT_EQ( 0, MANAGER.ssidSet.size() );
}

adhocrepeater::SsidManager::SsidSet::iterator findSsid(
    const adhocrepeater::SsidManager::SsidSet & _SSID_SET
    , const std::string &                       _SSID_STRING
)
{
    return std::find_if(
        _SSID_SET.begin()
        , _SSID_SET.end()
        , [
            &_SSID_STRING
        ]
        (
            const adhocrepeater::Ssid & _SSID
        )
        {
            return _SSID.ssid == _SSID_STRING;
        }
    );
}

void isExistsSsid(
    const adhocrepeater::SsidManager::SsidSet & _SSID_SET
    , const std::string &                       _SSID
)
{
    ASSERT_NE(
        _SSID_SET.end()
        , findSsid(
            _SSID_SET
            , _SSID
        )
    );
}

TEST(
    SsidManagerTest
    , Update_Insert
)
{
    auto    eventHandlersUnique = adhocrepeater::unique( adhocrepeater::newSsidManagerEventHandlers() );
    ASSERT_NE( nullptr, eventHandlersUnique.get() );
    const auto &    EVENT_HANDLERS = *eventHandlersUnique;

    const auto  KEEP_TIME = adhocrepeater::MicroSeconds( 1000000 );

    auto    managerUnique = adhocrepeater::unique(
        adhocrepeater::newSsidManager(
            EVENT_HANDLERS
            , KEEP_TIME
        )
    );
    ASSERT_NE( nullptr, managerUnique.get() );
    auto &  manager = *managerUnique;

    ASSERT_TRUE(
        adhocrepeater::update(
            manager
            , "TESTSSID"
        )
    );

    const auto &    SSID_SET = manager.ssidSet;
    ASSERT_EQ( 1, SSID_SET.size() );

    ASSERT_NO_FATAL_FAILURE(
        isExistsSsid(
            SSID_SET
            , "TESTSSID"
        )
    );

    ASSERT_TRUE(
        adhocrepeater::update(
            manager
            , "TESTSSID2"
        )
    );

    ASSERT_EQ( 2, SSID_SET.size() );
}

TEST(
    SsidManagerTest
    , Update_Update
)
{
    auto    eventHandlersUnique = adhocrepeater::unique( adhocrepeater::newSsidManagerEventHandlers() );
    ASSERT_NE( nullptr, eventHandlersUnique.get() );
    const auto &    EVENT_HANDLERS = *eventHandlersUnique;

    const auto  KEEP_TIME = adhocrepeater::MicroSeconds( 1000000 );

    auto    managerUnique = adhocrepeater::unique(
        adhocrepeater::newSsidManager(
            EVENT_HANDLERS
            , KEEP_TIME
        )
    );
    ASSERT_NE( nullptr, managerUnique.get() );
    auto &  manager = *managerUnique;

    ASSERT_TRUE(
        adhocrepeater::update(
            manager
            , "TESTSSID"
        )
    );

    const auto &    SSID_SET = manager.ssidSet;
    const auto      IT = findSsid(
        SSID_SET
        , "TESTSSID"
    );
    ASSERT_NE( SSID_SET.end(), IT );
    const auto &    SSID = *IT;
    const auto      UPDATE_TIME = SSID.updateTime.time_since_epoch().count();

    ASSERT_TRUE(
        adhocrepeater::update(
            manager
            , "TESTSSID"
        )
    );
    ASSERT_EQ( 1, SSID_SET.size() );
    ASSERT_LT( UPDATE_TIME, SSID.updateTime.time_since_epoch().count() );
}

TEST(
    SsidManagerTest
    , Update_InsertEvent
)
{
    auto    ssid = std::string();

    auto    insertEventHandlerUnique = adhocrepeater::unique(
        adhocrepeater::newSsidManagerInsertEventHandler(
            [
                &ssid
            ]
            (
                adhocrepeater::SsidManagerInsertEvent & _event
            )
            {
                ssid = adhocrepeater::getSsid( _event );
            }
        )
    );
    ASSERT_NE( nullptr, insertEventHandlerUnique.get() );
    auto &  insertEventHandler = *insertEventHandlerUnique;

    auto    eventHandlersUnique = adhocrepeater::unique( adhocrepeater::newSsidManagerEventHandlers() );
    ASSERT_NE( nullptr, eventHandlersUnique.get() );
    auto &  eventHandlers = *eventHandlersUnique;

    ASSERT_TRUE(
        adhocrepeater::add(
            eventHandlers
            , insertEventHandler
        )
    );

    const auto  KEEP_TIME = adhocrepeater::MicroSeconds( 1000000 );

    auto    managerUnique = adhocrepeater::unique(
        adhocrepeater::newSsidManager(
            eventHandlers
            , KEEP_TIME
        )
    );
    ASSERT_NE( nullptr, managerUnique.get() );
    auto &  manager = *managerUnique;

    ASSERT_TRUE(
        adhocrepeater::update(
            manager
            , "TESTSSID"
        )
    );

    ASSERT_STREQ( "TESTSSID", ssid.c_str() );
}

TEST(
    SsidManagerTest
    , Update_ReInsertEvent
)
{
    auto    i = int( 0 );

    auto    insertEventHandlerUnique = adhocrepeater::unique(
        adhocrepeater::newSsidManagerInsertEventHandler(
            [
                &i
            ]
            (
                adhocrepeater::SsidManagerInsertEvent &
            )
            {
                i++;
            }
        )
    );
    ASSERT_NE( nullptr, insertEventHandlerUnique.get() );
    auto &  insertEventHandler = *insertEventHandlerUnique;

    auto    eventHandlersUnique = adhocrepeater::unique( adhocrepeater::newSsidManagerEventHandlers() );
    ASSERT_NE( nullptr, eventHandlersUnique.get() );
    auto &  eventHandlers = *eventHandlersUnique;

    ASSERT_TRUE(
        adhocrepeater::add(
            eventHandlers
            , insertEventHandler
        )
    );

    const auto  KEEP_TIME = adhocrepeater::MicroSeconds( 500000 );

    auto    managerUnique = adhocrepeater::unique(
        adhocrepeater::newSsidManager(
            eventHandlers
            , KEEP_TIME
        )
    );
    ASSERT_NE( nullptr, managerUnique.get() );
    auto &  manager = *managerUnique;

    ASSERT_TRUE(
        adhocrepeater::update(
            manager
            , "TESTSSID"
        )
    );
    ASSERT_TRUE(
        adhocrepeater::update(
            manager
            , "TESTSSID"
        )
    );

    ASSERT_EQ( 1, i );

    sleep( 1 );

    ASSERT_TRUE(
        adhocrepeater::update(
            manager
            , "TESTSSID"
        )
    );

    ASSERT_EQ( 2, i );
}

TEST(
    SsidManagerTest
    , GetSsidList
)
{
    auto    eventHandlersUnique = adhocrepeater::unique( adhocrepeater::newSsidManagerEventHandlers() );
    ASSERT_NE( nullptr, eventHandlersUnique.get() );
    const auto &    EVENT_HANDLERS = *eventHandlersUnique;

    const auto  KEEP_TIME = adhocrepeater::MicroSeconds( 1000000 );

    auto    managerUnique = adhocrepeater::unique(
        adhocrepeater::newSsidManager(
            EVENT_HANDLERS
            , KEEP_TIME
        )
    );
    ASSERT_NE( nullptr, managerUnique.get() );
    auto &  manager = *managerUnique;

    ASSERT_TRUE(
        adhocrepeater::update(
            manager
            , "TESTSSID"
        )
    );

    auto    ssidList = adhocrepeater::SsidList();

    ASSERT_TRUE(
        adhocrepeater::getSsidList(
            manager
            , ssidList
        )
    );

    ASSERT_EQ( 1, ssidList.size() );
    ASSERT_STREQ( "TESTSSID", ssidList[ 0 ].c_str() );
}
