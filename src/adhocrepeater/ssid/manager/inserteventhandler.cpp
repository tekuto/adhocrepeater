﻿#include "adhocrepeater/ssid/manager/inserteventhandler.h"
#include "adhocrepeater/common/unique.h"

#include <new>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace adhocrepeater {
    SsidManagerInsertEventHandler * newSsidManagerInsertEventHandler(
        const SsidManagerInsertEventHandlerProc &   _PROC
    )
    {
        auto    thisUnique = unique(
            new( std::nothrow )SsidManagerInsertEventHandler{
                _PROC,
            }
        );
        if( thisUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:SSIDマネージャの挿入イベントハンドラの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        return thisUnique.release();
    }

    void free(
        SsidManagerInsertEventHandler & _this
    )
    {
        delete &_this;
    }

    void call(
        SsidManagerInsertEventHandler & _this
        , SsidManagerInsertEvent &      _event
    )
    {
        _this.proc( _event );
    }
}
