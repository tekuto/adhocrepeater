﻿#include "adhocrepeater/ssid/manager/ssid.h"
#include "adhocrepeater/common/time.h"

namespace adhocrepeater {
    bool Ssid::operator==(
        const std::string & _SSID_STRING
    ) const
    {
        return this->ssid == _SSID_STRING;
    }

    bool Ssid::operator<(
        const Ssid &    _SSID
    ) const
    {
        return this->ssid < _SSID.ssid;
    }

    void update(
        Ssid &              _this
        , const TimePoint & _TIME
    )
    {
        _this.updateTime = _TIME;
    }
}
