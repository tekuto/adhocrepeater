﻿#include "adhocrepeater/util/test.h"
#include "adhocrepeater/ssid/manager/eventhandlers.h"
#include "adhocrepeater/ssid/manager/inserteventhandler.h"
#include "adhocrepeater/ssid/manager/insertevent.h"
#include "adhocrepeater/ssid/manager/manager.h"
#include "adhocrepeater/common/time.h"
#include "adhocrepeater/common/unique.h"

#include <string>

TEST(
    SsidManagerEventHandlersTest
    , New
)
{
    auto    eventHandlersUnique = adhocrepeater::unique( adhocrepeater::newSsidManagerEventHandlers() );
    ASSERT_NE( nullptr, eventHandlersUnique.get() );

    const auto &    EVENT_HANDLERS = *eventHandlersUnique;
    ASSERT_EQ( 0, EVENT_HANDLERS.insertEventHandlerPtrSet.size() );
}

TEST(
    SsidManagerEventHandlersTest
    , Clone
)
{
    auto    eventHandlersUnique = adhocrepeater::unique( adhocrepeater::newSsidManagerEventHandlers() );
    ASSERT_NE( nullptr, eventHandlersUnique.get() );
    auto &  eventHandlers = *eventHandlersUnique;

    auto    eventHandlerUnique = adhocrepeater::unique(
        adhocrepeater::newSsidManagerInsertEventHandler(
            []( adhocrepeater::SsidManagerInsertEvent & ){}
        )
    );
    ASSERT_NE( nullptr, eventHandlerUnique.get() );
    auto &  eventHandler = *eventHandlerUnique;

    ASSERT_TRUE(
        adhocrepeater::add(
            eventHandlers
            , eventHandler
        )
    );

    auto    cloneUnique = adhocrepeater::unique( adhocrepeater::clone( eventHandlers ) );
    ASSERT_NE( nullptr, cloneUnique.get() );
    const auto &    CLONE = *cloneUnique;

    const auto &    EVENT_HANDLER_PTR_SET = CLONE.insertEventHandlerPtrSet;
    ASSERT_EQ( 1, EVENT_HANDLER_PTR_SET.size() );
    ASSERT_EQ( &eventHandler, *( EVENT_HANDLER_PTR_SET.find( &eventHandler ) ) );
}

TEST(
    SsidManagerEventHandlersTest
    , Add
)
{
    auto    eventHandlersUnique = adhocrepeater::unique( adhocrepeater::newSsidManagerEventHandlers() );
    ASSERT_NE( nullptr, eventHandlersUnique.get() );
    auto &  eventHandlers = *eventHandlersUnique;

    auto    eventHandlerUnique = adhocrepeater::unique(
        adhocrepeater::newSsidManagerInsertEventHandler(
            []( adhocrepeater::SsidManagerInsertEvent & ){}
        )
    );
    ASSERT_NE( nullptr, eventHandlerUnique.get() );
    auto &  eventHandler = *eventHandlerUnique;

    ASSERT_TRUE(
        adhocrepeater::add(
            eventHandlers
            , eventHandler
        )
    );

    const auto &    EVENT_HANDLER_PTR_SET = eventHandlers.insertEventHandlerPtrSet;
    ASSERT_EQ( 1, EVENT_HANDLER_PTR_SET.size() );
    ASSERT_EQ( &eventHandler, *( EVENT_HANDLER_PTR_SET.find( &eventHandler ) ) );

    ASSERT_FALSE(
        adhocrepeater::add(
            eventHandlers
            , eventHandler
        )
    );
    ASSERT_EQ( 1, EVENT_HANDLER_PTR_SET.size() );
}

TEST(
    SsidManagerEventHandlersTest
    , Remove
)
{
    auto    eventHandlersUnique = adhocrepeater::unique( adhocrepeater::newSsidManagerEventHandlers() );
    ASSERT_NE( nullptr, eventHandlersUnique.get() );
    auto &  eventHandlers = *eventHandlersUnique;

    auto    eventHandlerUnique = adhocrepeater::unique(
        adhocrepeater::newSsidManagerInsertEventHandler(
            []( adhocrepeater::SsidManagerInsertEvent & ){}
        )
    );
    ASSERT_NE( nullptr, eventHandlerUnique.get() );
    auto &  eventHandler = *eventHandlerUnique;

    auto    eventHandler2Unique = adhocrepeater::unique(
        adhocrepeater::newSsidManagerInsertEventHandler(
            []( adhocrepeater::SsidManagerInsertEvent & ){}
        )
    );
    ASSERT_NE( nullptr, eventHandler2Unique.get() );
    auto &  eventHandler2 = *eventHandler2Unique;

    ASSERT_TRUE(
        adhocrepeater::add(
            eventHandlers
            , eventHandler
        )
    );

    ASSERT_TRUE(
        adhocrepeater::add(
            eventHandlers
            , eventHandler2
        )
    );

    adhocrepeater::remove(
        eventHandlers
        , eventHandler
    );

    const auto &    EVENT_HANDLER_PTR_SET = eventHandlers.insertEventHandlerPtrSet;
    ASSERT_EQ( 1, EVENT_HANDLER_PTR_SET.size() );
    ASSERT_EQ( &eventHandler2, *( EVENT_HANDLER_PTR_SET.find( &eventHandler2 ) ) );
}

TEST(
    SsidManagerEventHandlersTest
    , Call
)
{
    auto    eventHandlersUnique = adhocrepeater::unique( adhocrepeater::newSsidManagerEventHandlers() );
    ASSERT_NE( nullptr, eventHandlersUnique.get() );
    auto &  eventHandlers = *eventHandlersUnique;

    auto    ssid = std::string();

    auto    eventHandlerUnique = adhocrepeater::unique(
        adhocrepeater::newSsidManagerInsertEventHandler(
            [
                &ssid
            ]
            (
                adhocrepeater::SsidManagerInsertEvent & _event
            )
            {
                ssid = adhocrepeater::getSsid( _event );
            }
        )
    );
    ASSERT_NE( nullptr, eventHandlerUnique.get() );
    auto &  eventHandler = *eventHandlerUnique;

    auto    ssid2 = std::string();

    auto    eventHandler2Unique = adhocrepeater::unique(
        adhocrepeater::newSsidManagerInsertEventHandler(
            [
                &ssid2
            ]
            (
                adhocrepeater::SsidManagerInsertEvent & _event
            )
            {
                ssid2 = adhocrepeater::getSsid( _event );
            }
        )
    );
    ASSERT_NE( nullptr, eventHandler2Unique.get() );
    auto &  eventHandler2 = *eventHandler2Unique;

    ASSERT_TRUE(
        adhocrepeater::add(
            eventHandlers
            , eventHandler
        )
    );

    ASSERT_TRUE(
        adhocrepeater::add(
            eventHandlers
            , eventHandler2
        )
    );

    const auto  KEEP_TIME = adhocrepeater::MicroSeconds( 1000000 );

    auto    managerUnique = adhocrepeater::unique(
        adhocrepeater::newSsidManager(
            eventHandlers
            , KEEP_TIME
        )
    );
    ASSERT_NE( nullptr, managerUnique.get() );
    auto &  manager = *managerUnique;

    auto    eventUnique = adhocrepeater::unique(
        adhocrepeater::newSsidManagerInsertEvent(
            manager
            , "TESTSSID"
        )
    );
    ASSERT_NE( nullptr, eventUnique.get() );
    auto &  event = *eventUnique;

    adhocrepeater::call(
        eventHandlers
        , event
    );

    ASSERT_STREQ( "TESTSSID", ssid.c_str() );
    ASSERT_STREQ( "TESTSSID", ssid2.c_str() );
}
