﻿#include "adhocrepeater/ssid/manager/eventhandlers.h"
#include "adhocrepeater/ssid/manager/inserteventhandler.h"
#include "adhocrepeater/common/unique.h"

#include <new>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace adhocrepeater {
    SsidManagerEventHandlers * newSsidManagerEventHandlers(
    )
    {
        auto    thisUnique = unique( new( std::nothrow )SsidManagerEventHandlers );
        if( thisUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:SSIDマネージャのイベントハンドラ集合の生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        return thisUnique.release();
    }

    SsidManagerEventHandlers * clone(
        const SsidManagerEventHandlers &    _ORG
    )
    {
        auto    thisUnique = unique( new( std::nothrow )SsidManagerEventHandlers( _ORG ) );
        if( thisUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:SSIDマネージャのイベントハンドラ集合のクローン生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        return thisUnique.release();
    }

    void free(
        SsidManagerEventHandlers &  _this
    )
    {
        delete &_this;
    }

    bool add(
        SsidManagerEventHandlers &          _this
        , SsidManagerInsertEventHandler &   _eventHandler
    )
    {
        const auto  RESULT = _this.insertEventHandlerPtrSet.insert( &_eventHandler );
        if( RESULT.second == false ) {
#ifdef  DEBUG
            std::printf( "E:イベントハンドラがすでに追加済み\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    void remove(
        SsidManagerEventHandlers &          _this
        , SsidManagerInsertEventHandler &   _eventHandler
    )
    {
        _this.insertEventHandlerPtrSet.erase( &_eventHandler );
    }

    void call(
        SsidManagerEventHandlers &  _this
        , SsidManagerInsertEvent &  _event
    )
    {
        for( auto & eventHandlerPtr : _this.insertEventHandlerPtrSet ) {
            auto &  eventHandler = *eventHandlerPtr;

            adhocrepeater::call(
                eventHandler
                , _event
            );
        }
    }
}
