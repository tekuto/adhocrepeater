﻿#include "adhocrepeater/ssid/manager/insertevent.h"
#include "adhocrepeater/common/unique.h"

#include <new>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace adhocrepeater {
    SsidManagerInsertEvent * newSsidManagerInsertEvent(
        SsidManager &           _manager
        , const std::string &   _SSID
    )
    {
        auto    thisUnique = unique(
            new( std::nothrow )SsidManagerInsertEvent{
                _manager,
                _SSID,
            }
        );
        if( thisUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:SSIDマネージャの挿入イベントの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        return thisUnique.release();
    }

    void free(
        SsidManagerInsertEvent &    _this
    )
    {
        delete &_this;
    }

    SsidManager & getManager(
        SsidManagerInsertEvent &    _this
    )
    {
        return _this.manager;
    }

    const std::string & getSsid(
        const SsidManagerInsertEvent &  _THIS
    )
    {
        return _THIS.ssid;
    }
}
