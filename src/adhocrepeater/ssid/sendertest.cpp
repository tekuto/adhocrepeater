﻿#include "adhocrepeater/util/test.h"
#include "adhocrepeater/ssid/sender.h"
#include "adhocrepeater/packet/udp/receiver.h"
#include "adhocrepeater/packet/packet.h"
#include "adhocrepeater/config/config.h"
#include "adhocrepeater/common/thread.h"
#include "adhocrepeater/common/unique.h"

#include <string>

TEST(
    SsidSenderTest
    , New
)
{
    auto    configUnique = adhocrepeater::unique( adhocrepeater::newConfig( "test/testconf/testconf.conf" ) );
    ASSERT_NE( nullptr, configUnique.get() );
    const auto &    CONFIG = *configUnique;

    auto    ssidSenderUnique = adhocrepeater::unique( adhocrepeater::newSsidSender( CONFIG ) );
    ASSERT_NE( nullptr, ssidSenderUnique.get() );

    auto &  ssidSender = *ssidSenderUnique;
    ASSERT_NE( nullptr, ssidSender.packetManagerUnique.get() );
    ASSERT_NE( nullptr, ssidSender.sendThreadUnique.get() );

    adhocrepeater::end( *( ssidSender.sendThreadUnique ) );
}

TEST(
    SsidSenderTest
    , Send
)
{
    auto    configUnique = adhocrepeater::unique( adhocrepeater::newConfig( "test/testconf/testconf.conf" ) );
    ASSERT_NE( nullptr, configUnique.get() );
    const auto &    CONFIG = *configUnique;

    auto    ssidSenderUnique = adhocrepeater::unique( adhocrepeater::newSsidSender( CONFIG ) );
    ASSERT_NE( nullptr, ssidSenderUnique.get() );
    auto &  ssidSender = *ssidSenderUnique;

    auto    udpReceiverUnique = adhocrepeater::unique( adhocrepeater::newUdpReceiver( 12346 ) );
    ASSERT_NE( nullptr, udpReceiverUnique.get() );
    auto &  udpReceiver = *udpReceiverUnique;

    ASSERT_TRUE(
        adhocrepeater::send(
            ssidSender
            , "TESTSSID"
        )
    );

    ASSERT_TRUE(
        adhocrepeater::poll(
            udpReceiver
            , 1000
        )
    );

    auto    receivePacket = adhocrepeater::Packet();
    ASSERT_TRUE(
        adhocrepeater::receive(
            udpReceiver
            , receivePacket
        )
    );

    const auto  SIZE = receivePacket.size();
    ASSERT_EQ( 8, SIZE );

    const auto  DATA = std::string(
        receivePacket.data()
        , SIZE
    );

    ASSERT_STREQ( "TESTSSID", DATA.c_str() );

    adhocrepeater::end( *( ssidSender.sendThreadUnique ) );

    ASSERT_TRUE(
        adhocrepeater::send(
            ssidSender
            , "TESTSSID"
        )
    );
}
