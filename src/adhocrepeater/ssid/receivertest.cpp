﻿#include "adhocrepeater/util/test.h"
#include "adhocrepeater/ssid/receiver.h"
#include "adhocrepeater/ssid/list.h"
#include "adhocrepeater/ssid/manager/eventhandlers.h"
#include "adhocrepeater/ssid/manager/manager.h"
#include "adhocrepeater/packet/udp/sender.h"
#include "adhocrepeater/packet/packet.h"
#include "adhocrepeater/config/config.h"
#include "adhocrepeater/common/time.h"
#include "adhocrepeater/common/thread.h"
#include "adhocrepeater/common/unique.h"

#include <string>
#include <unistd.h>

TEST(
    SsidReceiverTest
    , New
)
{
    auto    configUnique = adhocrepeater::unique( adhocrepeater::newConfig( "test/testconf/testconf.conf" ) );
    ASSERT_NE( nullptr, configUnique.get() );
    const auto &    CONFIG = *configUnique;

    auto    eventHandlersUnique = adhocrepeater::unique( adhocrepeater::newSsidManagerEventHandlers() );
    ASSERT_NE( nullptr, eventHandlersUnique.get() );
    const auto &    EVENT_HANDLERS = *eventHandlersUnique;

    const auto  KEEP_TIME = adhocrepeater::MicroSeconds( 10000000 );

    auto    ssidManagerUnique = adhocrepeater::unique(
        adhocrepeater::newSsidManager(
            EVENT_HANDLERS
            , KEEP_TIME
        )
    );
    ASSERT_NE( nullptr, ssidManagerUnique.get() );
    auto &  ssidManager = *ssidManagerUnique;

    auto    ssidReceiverUnique = adhocrepeater::unique(
        adhocrepeater::newSsidReceiver(
            CONFIG
            , ssidManager
        )
    );
    ASSERT_NE( nullptr, ssidReceiverUnique.get() );

    auto &  ssidReceiver = *ssidReceiverUnique;
    ASSERT_NE( nullptr, ssidReceiver.packetManagerUnique.get() );
    ASSERT_NE( nullptr, ssidReceiver.receiveThreadUnique.get() );
    ASSERT_NE( nullptr, ssidReceiver.updateThreadUnique.get() );

    adhocrepeater::end( *( ssidReceiver.receiveThreadUnique ) );
    adhocrepeater::end( *( ssidReceiver.updateThreadUnique ) );
}

TEST(
    SsidReceiverTest
    , Receive
)
{
    auto    configUnique = adhocrepeater::unique( adhocrepeater::newConfig( "test/testconf/testconf.conf" ) );
    ASSERT_NE( nullptr, configUnique.get() );
    const auto &    CONFIG = *configUnique;

    auto    eventHandlersUnique = adhocrepeater::unique( adhocrepeater::newSsidManagerEventHandlers() );
    ASSERT_NE( nullptr, eventHandlersUnique.get() );
    const auto &    EVENT_HANDLERS = *eventHandlersUnique;

    const auto  KEEP_TIME = adhocrepeater::MicroSeconds( 10000000 );

    auto    ssidManagerUnique = adhocrepeater::unique(
        adhocrepeater::newSsidManager(
            EVENT_HANDLERS
            , KEEP_TIME
        )
    );
    ASSERT_NE( nullptr, ssidManagerUnique.get() );
    auto &  ssidManager = *ssidManagerUnique;

    auto    ssidReceiverUnique = adhocrepeater::unique(
        adhocrepeater::newSsidReceiver(
            CONFIG
            , ssidManager
        )
    );
    ASSERT_NE( nullptr, ssidReceiverUnique.get() );
    auto &  ssidReceiver = *ssidReceiverUnique;

    auto    udpSenderUnique = adhocrepeater::unique(
        adhocrepeater::newUdpSender(
            "127.0.0.1"
            , 12346
        )
    );
    ASSERT_NE( nullptr, udpSenderUnique.get() );
    auto &  udpSender = *udpSenderUnique;

    auto    sendPacket = adhocrepeater::Packet();
    ASSERT_TRUE(
        adhocrepeater::setData(
            sendPacket
            , "TESTSSID"
        )
    );

    sleep( 1 );

    ASSERT_TRUE(
        adhocrepeater::send(
            udpSender
            , sendPacket
        )
    );

    sleep( 1 );

    auto    ssidList = adhocrepeater::SsidList();
    ASSERT_TRUE(
        adhocrepeater::getSsidList(
            ssidManager
            , ssidList
        )
    );

    ASSERT_EQ( 1, ssidList.size() );
    ASSERT_STREQ( "TESTSSID", ssidList[ 0 ].c_str() );

    adhocrepeater::end( *( ssidReceiver.receiveThreadUnique ) );
    adhocrepeater::end( *( ssidReceiver.updateThreadUnique ) );

    ASSERT_TRUE(
        adhocrepeater::send(
            udpSender
            , sendPacket
        )
    );
}
