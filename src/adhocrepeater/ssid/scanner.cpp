﻿#include "adhocrepeater/ssid/scanner.h"
#include "adhocrepeater/ssid/manager/manager.h"
#include "adhocrepeater/iwlib/socket.h"
#include "adhocrepeater/iwlib/ssidscan.h"
#include "adhocrepeater/config/config.h"
#include "adhocrepeater/common/thread.h"
#include "adhocrepeater/common/time.h"
#include "adhocrepeater/common/unique.h"

#include <iwlib.h>
#include <new>
#include <utility>
#include <cstring>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    bool isTargetSsid(
        const adhocrepeater::ScannedSsid &  _SCANNED_SSID
        , const adhocrepeater::Config &     _CONFIG
        , adhocrepeater::IwSocket &         _socket
    )
    {
        const auto &    TARGET_SSID = _CONFIG.targetSsid;

        if( _SCANNED_SSID.mode != IW_MODE_ADHOC ) {
#ifdef  DEBUG
            std::printf( "I:アドホックモードのSSIDではない\n" );
#endif  // DEBUG

            return false;
        }

        if( std::strncmp(
            _SCANNED_SSID.ssid.c_str()
            , TARGET_SSID.prefix.c_str()
            , 4
        ) != 0 ) {
#ifdef  DEBUG
            std::printf( "I:SSIDの接頭辞がマッチしない\n" );
#endif  // DEBUG

            return false;
        }

        auto    channel = int();
        if( adhocrepeater::toChannel(
            channel
            , _socket
            , _SCANNED_SSID.iwFreq
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:チャンネルへの変換に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( channel != TARGET_SSID.channel ) {
#ifdef  DEBUG
            std::printf( "I:チャンネルがマッチしない\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool extractScanData(
        adhocrepeater::SsidScanData &   _scanData
        , const adhocrepeater::Config & _CONFIG
        , adhocrepeater::SsidManager &  _manager
        , adhocrepeater::IwSocket &     _socket
    )
    {
        auto    stream = stream_descr();
        adhocrepeater::initEventStream(
            stream
            , _scanData
        );

        while( 1 ) {
            auto    scannedSsid = adhocrepeater::ScannedSsid();
            if( adhocrepeater::getScannedSsid(
                scannedSsid
                , stream
                , _socket
            ) == false ) {
                break;
            }

            if( isTargetSsid(
                scannedSsid
                , _CONFIG
                , _socket
            ) == false ) {
#ifdef  DEBUG
                std::printf( "I:対象外のSSID\n" );
#endif  // DEBUG

                continue;
            }

            if( adhocrepeater::update(
                _manager
                , scannedSsid.ssid
            ) == false ) {
#ifdef  DEBUG
                std::printf( "E:SSIDマネージャの更新に失敗\n" );
#endif  // DEBUG

                return false;
            }
        }

        return true;
    }

    bool scanSsid(
        const adhocrepeater::Config &   _CONFIG
        , adhocrepeater::SsidManager &  _manager
        , adhocrepeater::IwSocket &     _socket
    )
    {
        const auto &    SCANNER = _CONFIG.scanner;

        if( adhocrepeater::initSsidScan( _socket ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSIDのスキャン開始に失敗\n" );
#endif  // DEBUG

            return false;
        }

        auto    scanData = adhocrepeater::SsidScanData();
        if( adhocrepeater::getSsidScanData(
            scanData
            , _socket
            , SCANNER.getScanDataTimeout
            , SCANNER.getScanDataInterval
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSIDスキャンデータの取得に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( extractScanData(
            scanData
            , _CONFIG
            , _manager
            , _socket
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSIDスキャンデータの展開に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool threadProc(
        adhocrepeater::Thread &         _thread
        , const adhocrepeater::Config & _CONFIG
        , adhocrepeater::SsidManager &  _manager
        , adhocrepeater::IwSocket &     _socket
        , bool &                        _loopEnd
    )
    {
        if( adhocrepeater::isRunning( _thread ) == false ) {
            _loopEnd = true;
            return true;
        }

        if( scanSsid(
            _CONFIG
            , _manager
            , _socket
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSIDのスキャンに失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    void threadProc(
        adhocrepeater::Thread &         _thread
        , const adhocrepeater::Config & _CONFIG
        , adhocrepeater::SsidManager &  _manager
    )
    {
        const auto &    INTERFACES = _CONFIG.interfaces;
        const auto &    SCANNER = _CONFIG.scanner;

        auto    socketUnique = adhocrepeater::unique( adhocrepeater::newIwSocket( INTERFACES.scanner ) );
        if( socketUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:SSIDスキャン用ソケットの生成に失敗\n" );
#endif  // DEBUG

            return;
        }
        auto &  socket = *socketUnique;

        adhocrepeater::intervalProc(
            SCANNER.scanInterval
            , [
                &_thread
                , &_CONFIG
                , &_manager
                , &socket
            ]
            (
                bool &  _loopEnd
            )
            {
                return threadProc(
                    _thread
                    , _CONFIG
                    , _manager
                    , socket
                    , _loopEnd
                );
            }
        );
    }
}

namespace adhocrepeater {
    SsidScanner * newSsidScanner(
        const Config &  _CONFIG
        , SsidManager & _manager
    )
    {
        auto    threadUnique = unique(
            newThread(
                [
                    &_CONFIG
                    , &_manager
                ]
                (
                    Thread &    _thread
                )
                {
                    threadProc(
                        _thread
                        , _CONFIG
                        , _manager
                    );
                }
            )
        );
        if( threadUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:スレッドの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    thisUnique = unique(
            new( std::nothrow )SsidScanner{
                std::move( threadUnique ),
            }
        );
        if( thisUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:SSIDスキャナの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        return thisUnique.release();
    }

    void free(
        SsidScanner &   _this
    )
    {
        delete &_this;
    }
}
