﻿#include "adhocrepeater/util/test.h"
#include "adhocrepeater/ssid/changer.h"
#include "adhocrepeater/ssid/sender.h"
#include "adhocrepeater/config/config.h"
#include "adhocrepeater/common/thread.h"
#include "adhocrepeater/common/unique.h"

#include <unistd.h>

TEST(
    SsidChangerTest
    , New
)
{
    auto    configUnique = adhocrepeater::unique( adhocrepeater::newConfig( "test/testconf/testconf.conf" ) );
    ASSERT_NE( nullptr, configUnique.get() );
    const auto &    CONFIG = *configUnique;

    auto    ssidSenderUnique = adhocrepeater::unique( adhocrepeater::newSsidSender( CONFIG ) );
    ASSERT_NE( nullptr, ssidSenderUnique.get() );
    auto &  ssidSender = *ssidSenderUnique;

    adhocrepeater::end( *( ssidSender.sendThreadUnique ) );

    auto    changerUnique = adhocrepeater::unique(
        adhocrepeater::newSsidChanger(
            CONFIG
            , ssidSender
        )
    );
    ASSERT_NE( nullptr, changerUnique.get() );

    auto &  changer = *changerUnique;
    ASSERT_STREQ( "", changer.nextSsid.c_str() );
    ASSERT_FALSE( changer.existsNextSsid );
    ASSERT_FALSE( changer.connected );
    ASSERT_NE( nullptr, changer.insertEventHandlerUnique.get() );
    ASSERT_NE( nullptr, changer.ssidManagerUnique.get() );
    ASSERT_NE( nullptr, changer.threadUnique.get() );

    adhocrepeater::end( *( changer.threadUnique ) );
}

TEST(
    SsidChangerTest
    , Change
)
{
    auto    configUnique = adhocrepeater::unique( adhocrepeater::newConfig( "test/testconf/testconf.conf" ) );
    ASSERT_NE( nullptr, configUnique.get() );
    const auto &    CONFIG = *configUnique;

    auto    ssidSenderUnique = adhocrepeater::unique( adhocrepeater::newSsidSender( CONFIG ) );
    ASSERT_NE( nullptr, ssidSenderUnique.get() );
    auto &  ssidSender = *ssidSenderUnique;

    adhocrepeater::end( *( ssidSender.sendThreadUnique ) );

    auto    changerUnique = adhocrepeater::unique(
        adhocrepeater::newSsidChanger(
            CONFIG
            , ssidSender
        )
    );
    ASSERT_NE( nullptr, changerUnique.get() );
    auto &  changer = *changerUnique;

    adhocrepeater::end( *( changer.threadUnique ) );

    adhocrepeater::change(
        changer
        , "TESTSSID"
    );

    ASSERT_STREQ( "TESTSSID", changer.nextSsid.c_str() );

    adhocrepeater::change(
        changer
        , "TESTSSID2"
    );

    ASSERT_STREQ( "TESTSSID", changer.nextSsid.c_str() );
}

TEST(
    SsidChangerTest
    , Change_Force
)
{
    auto    configUnique = adhocrepeater::unique( adhocrepeater::newConfig( "test/testconf/testconf.conf" ) );
    ASSERT_NE( nullptr, configUnique.get() );
    const auto &    CONFIG = *configUnique;

    auto    ssidSenderUnique = adhocrepeater::unique( adhocrepeater::newSsidSender( CONFIG ) );
    ASSERT_NE( nullptr, ssidSenderUnique.get() );
    auto &  ssidSender = *ssidSenderUnique;

    adhocrepeater::end( *( ssidSender.sendThreadUnique ) );

    auto    changerUnique = adhocrepeater::unique(
        adhocrepeater::newSsidChanger(
            CONFIG
            , ssidSender
        )
    );
    ASSERT_NE( nullptr, changerUnique.get() );
    auto &  changer = *changerUnique;

    adhocrepeater::end( *( changer.threadUnique ) );

    changer.currentSsid = "TESTSSIDABCDEF1";
    changer.connected = true;
    changer.existsNextSsid = true;

    adhocrepeater::change(
        changer
        , "TESTSSIDABCDEF2"
    );

    ASSERT_STREQ( "TESTSSIDABCDEF2", changer.nextSsid.c_str() );
    ASSERT_FALSE( changer.connected );
    ASSERT_TRUE( changer.existsNextSsid );
}

TEST(
    SsidChangerTest
    , Disconnect
)
{
    auto    configUnique = adhocrepeater::unique( adhocrepeater::newConfig( "test/testconf/testconf.conf" ) );
    ASSERT_NE( nullptr, configUnique.get() );
    const auto &    CONFIG = *configUnique;

    auto    ssidSenderUnique = adhocrepeater::unique( adhocrepeater::newSsidSender( CONFIG ) );
    ASSERT_NE( nullptr, ssidSenderUnique.get() );
    auto &  ssidSender = *ssidSenderUnique;

    adhocrepeater::end( *( ssidSender.sendThreadUnique ) );

    auto    changerUnique = adhocrepeater::unique(
        adhocrepeater::newSsidChanger(
            CONFIG
            , ssidSender
        )
    );
    ASSERT_NE( nullptr, changerUnique.get() );
    auto &  changer = *changerUnique;

    adhocrepeater::change(
        changer
        , "TESTSSID"
    );

    adhocrepeater::disconnect( changer );

    ASSERT_TRUE( changer.existsNextSsid );

    sleep( 1 );

    adhocrepeater::end( *( changer.threadUnique ) );

    adhocrepeater::disconnect( changer );

    adhocrepeater::change(
        changer
        , "TESTSSID"
    );
}

TEST(
    SsidChangerTest
    , GetSsidManager
)
{
    auto    configUnique = adhocrepeater::unique( adhocrepeater::newConfig( "test/testconf/testconf.conf" ) );
    ASSERT_NE( nullptr, configUnique.get() );
    const auto &    CONFIG = *configUnique;

    auto    ssidSenderUnique = adhocrepeater::unique( adhocrepeater::newSsidSender( CONFIG ) );
    ASSERT_NE( nullptr, ssidSenderUnique.get() );
    auto &  ssidSender = *ssidSenderUnique;

    adhocrepeater::end( *( ssidSender.sendThreadUnique ) );

    auto    changerUnique = adhocrepeater::unique(
        adhocrepeater::newSsidChanger(
            CONFIG
            , ssidSender
        )
    );
    ASSERT_NE( nullptr, changerUnique.get() );
    auto &  changer = *changerUnique;

    adhocrepeater::end( *( changer.threadUnique ) );

    ASSERT_EQ( changer.ssidManagerUnique.get(), &( adhocrepeater::getSsidManager( changer ) ) );
}
