﻿#include "adhocrepeater/ssid/sender.h"
#include "adhocrepeater/packet/manager.h"
#include "adhocrepeater/packet/packet.h"
#include "adhocrepeater/packet/udp/sender.h"
#include "adhocrepeater/config/config.h"
#include "adhocrepeater/common/thread.h"
#include "adhocrepeater/common/unique.h"

#include <new>
#include <utility>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    void sendProc(
        adhocrepeater::PacketManager &  _manager
        , adhocrepeater::UdpSender &    _sender
        , adhocrepeater::PacketBuffer & _buffer
    )
    {
        adhocrepeater::getBuffer(
            _manager
            , _buffer
        );

        for( const auto & PACKET : _buffer ) {
            if( adhocrepeater::send(
                _sender
                , PACKET
            ) == false ) {
#ifdef  DEBUG
                std::printf( "E:UDP送信に失敗\n" );
#endif  // DEBUG

                return;
            }
        }
    }

    void sendProc(
        adhocrepeater::Thread &             _thread
        , const adhocrepeater::Config &     _CONFIG
        , adhocrepeater::PacketManager &    _manager
    )
    {
        const auto &    PEER = _CONFIG.peer;

        auto    senderUnique = adhocrepeater::unique(
            adhocrepeater::newUdpSender(
                PEER.address
                , PEER.ssidPort
            )
        );
        if( senderUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:UDP送信の生成に失敗\n" );
#endif  // DEBUG

            return;
        }
        auto &  sender = *senderUnique;

        auto    buffer = adhocrepeater::PacketBuffer();
        while( adhocrepeater::isRunning( _thread ) == true ) {
            sendProc(
                _manager
                , sender
                , buffer
            );
        }
    }
}

namespace adhocrepeater {
    SsidSender * newSsidSender(
        const Config &  _CONFIG
    )
    {
        auto    packetManagerUnique = unique( newPacketManager() );
        if( packetManagerUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:パケットマネージャの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }
        auto &  packetManager = *packetManagerUnique;

        auto    sendThreadUnique = unique(
            newThread(
                [
                    &_CONFIG
                    , &packetManager
                ]
                (
                    Thread &    _thread
                )
                {
                    sendProc(
                        _thread
                        , _CONFIG
                        , packetManager
                    );
                }
            )
        );
        if( sendThreadUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:SSID送信スレッドの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    thisUnique = unique(
            new( std::nothrow )SsidSender{
                std::move( packetManagerUnique ),
                std::move( sendThreadUnique ),
            }
        );
        if( thisUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:SSID送信の生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        return thisUnique.release();
    }

    void free(
        SsidSender &    _this
    )
    {
        delete &_this;
    }

    bool send(
        SsidSender &            _this
        , const std::string &   _SSID
    )
    {
        auto    packet = adhocrepeater::Packet();
        if( setData(
            packet
            , _SSID
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:パケットデータ設定に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( add(
            *( _this.packetManagerUnique )
            , std::move( packet )
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:パケットの追加に失敗\n" );
#endif  // DEBUG

            return false;
        }

#ifdef  DEBUG
        std::printf(
            "I:SSIDを送信: %s\n"
            , _SSID.c_str()
        );
#endif  // DEBUG

        return true;
    }
}
