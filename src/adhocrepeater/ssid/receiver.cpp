﻿#include "adhocrepeater/ssid/receiver.h"
#include "adhocrepeater/ssid/manager/manager.h"
#include "adhocrepeater/packet/manager.h"
#include "adhocrepeater/packet/udp/receiver.h"
#include "adhocrepeater/config/config.h"
#include "adhocrepeater/common/thread.h"
#include "adhocrepeater/common/time.h"
#include "adhocrepeater/common/unique.h"

#include <new>
#include <utility>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    bool pollAndReceive(
        const adhocrepeater::Config &   _CONFIG
        , adhocrepeater::UdpReceiver &  _receiver
        , adhocrepeater::Packet &       _packet
    )
    {
        const auto &    RECEIVER = _CONFIG.receiver;

        if( poll(
            _receiver
            , RECEIVER.pollingTimeoutMSeconds
        ) == false ) {
#ifdef  DEBUG
            std::printf( "I:ポーリングに失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( adhocrepeater::receive(
            _receiver
            , _packet
        ) == false ) {
#ifdef  DEDUB
            std::printf( "E:UDP受信に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool receive(
        const adhocrepeater::Config &   _CONFIG
        , adhocrepeater::UdpReceiver &  _receiver
        , adhocrepeater::Packet &       _packet
    )
    {
        const auto &    RECEIVER = _CONFIG.receiver;

        return adhocrepeater::timeoutProc(
            RECEIVER.receiveUdpTimeout
            , [
                &_CONFIG
                , &_receiver
                , &_packet
            ]
            (
                bool &  _loopEnd
            )
            {
                if( pollAndReceive(
                    _CONFIG
                    , _receiver
                    , _packet
                ) == true ) {
                    _loopEnd = true;

                    return true;
                }

                return false;
            }
        );
    }

    void receiveProc(
        const adhocrepeater::Config &       _CONFIG
        , adhocrepeater::PacketManager &    _manager
        , adhocrepeater::UdpReceiver &      _receiver
    )
    {
        auto    packet = adhocrepeater::Packet();
        if( ::receive(
            _CONFIG
            , _receiver
            , packet
        ) == false ) {
#ifdef  DEBUG
            std::printf( "I:データ受信に失敗\n" );
#endif  // DEBUG

            return;
        }

        if( adhocrepeater::add(
            _manager
            , std::move( packet )
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:パケットの追加に失敗\n" );
#endif  // DEBUG

            return;
        }
    }

    void receiveProc(
        adhocrepeater::Thread &             _thread
        , const adhocrepeater::Config &     _CONFIG
        , adhocrepeater::PacketManager &    _packetManager
    )
    {
        const auto &    PORTS = _CONFIG.ports;

        auto    receiverUnique = adhocrepeater::unique( adhocrepeater::newUdpReceiver( PORTS.ssid ) );
        if( receiverUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:SSID受信の生成に失敗\n" );
#endif  // DEBUG

            return;
        }
        auto &  receiver = *receiverUnique;

        while( adhocrepeater::isRunning( _thread ) == true ) {
            receiveProc(
                _CONFIG
                , _packetManager
                , receiver
            );
        }
    }

    void updateProc(
        adhocrepeater::SsidManager &        _ssidManager
        , adhocrepeater::PacketManager &    _packetManager
        , adhocrepeater::PacketBuffer &     _buffer
    )
    {
        adhocrepeater::getBuffer(
            _packetManager
            , _buffer
        );

        for( const auto & PACKET : _buffer ) {
            const auto  SSID = std::string(
                PACKET.data()
                , PACKET.size()
            );
#ifdef  DEBUG
            std::printf(
                "I:SSIDを受信: %s\n"
                , SSID.c_str()
            );
#endif  // DEBUG

            if( adhocrepeater::update(
                _ssidManager
                , SSID
            ) == false ) {
#ifdef  DEBUG
                std::printf( "E:SSIDマネージャの更新に失敗\n" );
#endif  // DEBUG

                continue;
            }
        }
    }

    void updateProc(
        adhocrepeater::Thread &             _thread
        , adhocrepeater::SsidManager &      _ssidManager
        , adhocrepeater::PacketManager &    _packetManager
    )
    {
        auto    buffer = adhocrepeater::PacketBuffer();
        while( adhocrepeater::isRunning( _thread ) == true ) {
            updateProc(
                _ssidManager
                , _packetManager
                , buffer
            );
        }
    }
}

namespace adhocrepeater {
    SsidReceiver * newSsidReceiver(
        const Config &  _CONFIG
        , SsidManager & _ssidManager
    )
    {
        auto    packetManagerUnique = unique( newPacketManager() );
        if( packetManagerUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:パケットマネージャの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }
        auto &  packetManager = *packetManagerUnique;

        auto    receiveThreadUnique = unique(
            newThread(
                [
                    &_CONFIG
                    , &packetManager
                ]
                (
                    Thread &    _thread
                )
                {
                    receiveProc(
                        _thread
                        , _CONFIG
                        , packetManager
                    );
                }
            )
        );
        if( receiveThreadUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:SSID受信スレッドの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    updateThreadUnique = unique(
            newThread(
                [
                    &_CONFIG
                    , &_ssidManager
                    , &packetManager
                ]
                (
                    Thread &    _thread
                )
                {
                    updateProc(
                        _thread
                        , _ssidManager
                        , packetManager
                    );
                }
            )
        );
        if( updateThreadUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:SSID更新スレッドの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    thisUnique = unique(
            new( std::nothrow )SsidReceiver{
                std::move( packetManagerUnique ),
                std::move( receiveThreadUnique ),
                std::move( updateThreadUnique ),
            }
        );
        if( thisUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:SSID受信の生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        return thisUnique.release();
    }

    void free(
        SsidReceiver &  _this
    )
    {
        delete &_this;
    }
}
