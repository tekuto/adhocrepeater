﻿#include "adhocrepeater/util/test.h"
#include "adhocrepeater/ssid/scanner.h"
#include "adhocrepeater/ssid/manager/eventhandlers.h"
#include "adhocrepeater/ssid/manager/manager.h"
#include "adhocrepeater/config/config.h"
#include "adhocrepeater/common/thread.h"
#include "adhocrepeater/common/time.h"
#include "adhocrepeater/common/unique.h"

TEST(
    SsidScannerTest
    , New
)
{
    auto    configUnique = adhocrepeater::unique( adhocrepeater::newConfig( "test/testconf/testconf.conf" ) );
    ASSERT_NE( nullptr, configUnique.get() );
    const auto &    CONFIG = *configUnique;

    auto    eventHandlersUnique = adhocrepeater::unique( adhocrepeater::newSsidManagerEventHandlers() );
    ASSERT_NE( nullptr, eventHandlersUnique.get() );
    const auto &    EVENT_HANDLERS = *eventHandlersUnique;

    const auto  KEEP_TIME = adhocrepeater::MicroSeconds( 1000000 );

    auto    managerUnique = adhocrepeater::unique(
        adhocrepeater::newSsidManager(
            EVENT_HANDLERS
            , KEEP_TIME
        )
    );
    ASSERT_NE( nullptr, managerUnique.get() );
    auto &  manager = *managerUnique;

    auto    scannerUnique = adhocrepeater::unique(
        adhocrepeater::newSsidScanner(
            CONFIG
            , manager
        )
    );
    ASSERT_NE( nullptr, scannerUnique.get() );

    auto &  scanner = *scannerUnique;
    ASSERT_NE( nullptr, scanner.threadUnique.get() );

    adhocrepeater::end( *( scanner.threadUnique ) );
}
