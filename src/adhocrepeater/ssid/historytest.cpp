﻿#include "adhocrepeater/util/test.h"
#include "adhocrepeater/ssid/history.h"
#include "adhocrepeater/common/unique.h"

TEST(
    SsidHistoryTest
    , New
)
{
    auto    ssidHistoryUnique = adhocrepeater::unique( adhocrepeater::newSsidHistory( 10 ) );
    ASSERT_NE( nullptr, ssidHistoryUnique.get() );

    const auto &    SSID_HISTORY = *ssidHistoryUnique;
    ASSERT_EQ( 10, SSID_HISTORY.MAX_SIZE );
    ASSERT_EQ( 0, SSID_HISTORY.history.size() );
}

TEST(
    SsidHistoryTest
    , GetNextSsid
)
{
    auto    ssidHistoryUnique = adhocrepeater::unique( adhocrepeater::newSsidHistory( 2 ) );
    ASSERT_NE( nullptr, ssidHistoryUnique.get() );
    auto &  ssidHistory = *ssidHistoryUnique;

    const auto  SSID_LIST = adhocrepeater::SsidList(
        {
            "TESTSSID1",
            "TESTSSID2",
            "TESTSSID3",
        }
    );

    auto    ssid = std::string();

    ASSERT_TRUE(
        adhocrepeater::getNextSsid(
            ssid
            , ssidHistory
            , SSID_LIST
        )
    );
    ASSERT_STREQ( "TESTSSID1", ssid.c_str() );

    ASSERT_TRUE(
        adhocrepeater::add(
            ssidHistory
            , "TESTSSID1"
        )
    );

    ASSERT_TRUE(
        adhocrepeater::getNextSsid(
            ssid
            , ssidHistory
            , SSID_LIST
        )
    );
    ASSERT_STREQ( "TESTSSID2", ssid.c_str() );

    ASSERT_TRUE(
        adhocrepeater::add(
            ssidHistory
            , "TESTSSID2"
        )
    );

    ASSERT_TRUE(
        adhocrepeater::getNextSsid(
            ssid
            , ssidHistory
            , SSID_LIST
        )
    );
    ASSERT_STREQ( "TESTSSID3", ssid.c_str() );

    ASSERT_TRUE(
        adhocrepeater::add(
            ssidHistory
            , "TESTSSID3"
        )
    );

    const auto  SSID_LIST2 = adhocrepeater::SsidList(
        {
            "TESTSSID2",
            "TESTSSID3",
        }
    );
    ASSERT_TRUE(
        adhocrepeater::getNextSsid(
            ssid
            , ssidHistory
            , SSID_LIST2
        )
    );
    ASSERT_STREQ( "TESTSSID2", ssid.c_str() );
}

TEST(
    SsidHistoryTest
    , Add
)
{
    auto    ssidHistoryUnique = adhocrepeater::unique( adhocrepeater::newSsidHistory( 10 ) );
    ASSERT_NE( nullptr, ssidHistoryUnique.get() );
    auto &  ssidHistory = *ssidHistoryUnique;

    const auto &    HISTORY = ssidHistory.history;

    ASSERT_TRUE(
        adhocrepeater::add(
            ssidHistory
            , "TESTSSID1"
        )
    );

    ASSERT_EQ( 1, HISTORY.size() );
    ASSERT_STREQ( "TESTSSID1", HISTORY[ 0 ].c_str() );

    ASSERT_TRUE(
        adhocrepeater::add(
            ssidHistory
            , "TESTSSID2"
        )
    );

    ASSERT_EQ( 2, HISTORY.size() );
    ASSERT_STREQ( "TESTSSID1", HISTORY[ 0 ].c_str() );
    ASSERT_STREQ( "TESTSSID2", HISTORY[ 1 ].c_str() );
}

TEST(
    SsidHistoryTest
    , Add_Full
)
{
    auto    ssidHistoryUnique = adhocrepeater::unique( adhocrepeater::newSsidHistory( 2 ) );
    ASSERT_NE( nullptr, ssidHistoryUnique.get() );
    auto &  ssidHistory = *ssidHistoryUnique;

    ASSERT_TRUE(
        adhocrepeater::add(
            ssidHistory
            , "TESTSSID1"
        )
    );
    ASSERT_TRUE(
        adhocrepeater::add(
            ssidHistory
            , "TESTSSID2"
        )
    );
    ASSERT_TRUE(
        adhocrepeater::add(
            ssidHistory
            , "TESTSSID3"
        )
    );

    const auto &    HISTORY = ssidHistory.history;

    ASSERT_EQ( 2, HISTORY.size() );
    ASSERT_STREQ( "TESTSSID2", HISTORY[ 0 ].c_str() );
    ASSERT_STREQ( "TESTSSID3", HISTORY[ 1 ].c_str() );
}

TEST(
    SsidHistoryTest
    , Add_Exists
)
{
    auto    ssidHistoryUnique = adhocrepeater::unique( adhocrepeater::newSsidHistory( 10 ) );
    ASSERT_NE( nullptr, ssidHistoryUnique.get() );
    auto &  ssidHistory = *ssidHistoryUnique;

    ASSERT_TRUE(
        adhocrepeater::add(
            ssidHistory
            , "TESTSSID1"
        )
    );
    ASSERT_TRUE(
        adhocrepeater::add(
            ssidHistory
            , "TESTSSID2"
        )
    );
    ASSERT_TRUE(
        adhocrepeater::add(
            ssidHistory
            , "TESTSSID3"
        )
    );
    ASSERT_TRUE(
        adhocrepeater::add(
            ssidHistory
            , "TESTSSID2"
        )
    );

    const auto &    HISTORY = ssidHistory.history;

    ASSERT_EQ( 3, HISTORY.size() );
    ASSERT_STREQ( "TESTSSID1", HISTORY[ 0 ].c_str() );
    ASSERT_STREQ( "TESTSSID3", HISTORY[ 1 ].c_str() );
    ASSERT_STREQ( "TESTSSID2", HISTORY[ 2 ].c_str() );
}
