﻿#include "adhocrepeater/ssid/history.h"
#include "adhocrepeater/ssid/list.h"
#include "adhocrepeater/common/unique.h"

#include <new>
#include <algorithm>
#include <string>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    bool findSsid(
        std::string &                       _ssid
        , const adhocrepeater::SsidList &   _HISTORY
        , const adhocrepeater::SsidList &   _SSID_LIST
    )
    {
        if( _SSID_LIST.size() <= 0 ) {
#ifdef  DEBUG
            std::printf( "I:SSIDリストにSSIDがない\n" );
#endif  // DEBUG

            return false;
        }

        const auto  BEGIN = _HISTORY.begin();
        const auto  END = _HISTORY.end();

        auto    it = END;
        for( const auto & SSID : _SSID_LIST ) {
            const auto  IT = std::find_if(
                BEGIN
                , END
                , [
                    &SSID
                ]
                (
                    const std::string & _SSID
                )
                {
                    return _SSID == SSID;
                }
            );
            if( IT == END ) {
                _ssid = SSID;

                return true;
            }

            if( IT < it ) {
                it = IT;
            }
        }

        _ssid = *it;

        return true;
    }

    void eraseSameSsid(
        adhocrepeater::SsidList &   _history
        , const std::string &       _SSID
    )
    {
        const auto  END = _history.end();

        auto    it = std::find_if(
            _history.begin()
            , END
            , [
                &_SSID
            ]
            (
                const std::string & _HISTORY_SSID
            )
            {
                return _HISTORY_SSID == _SSID;
            }
        );
        if( it != END ) {
            _history.erase( it );
        }
    }
}

namespace adhocrepeater {
    SsidHistory * newSsidHistory(
        size_t  _maxSize
    )
    {
        auto    thisUnique = unique(
            new( std::nothrow )SsidHistory{
                _maxSize,
            }
        );
        if( thisUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:SSID履歴の生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        return thisUnique.release();
    }

    void free(
        SsidHistory &   _this
    )
    {
        delete &_this;
    }

    bool getNextSsid(
        std::string &           _nextSsid
        , const SsidHistory &   _THIS
        , const SsidList &      _SSID_LIST
    )
    {
        const auto &    HISTORY = _THIS.history;

        if( findSsid(
            _nextSsid
            , HISTORY
            , _SSID_LIST
        ) == false ) {
#ifdef  DEBUG
            std::printf( "I:SSIDの検索に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool add(
        SsidHistory &           _this
        , const std::string &   _SSID
    )
    {
        auto &  history = _this.history;

        eraseSameSsid(
            history
            , _SSID
        );

        history.push_back( _SSID );

        if( history.size() > _this.MAX_SIZE ) {
            history.erase( history.begin() );
        }

        return true;
    }
}
